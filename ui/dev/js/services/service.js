_revlegApp.service("formNameService", [function() {
    return {
        formName: null,
        setFormName: function(value) {
            this.formName = value;
        }
    }
}]);

_revlegApp.service("authService", ["ipCookie",function(ipCookie) {
    
    this.checkLogin = function() {

    	if(ipCookie('userId') != undefined)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    };

    this.getFullName = function() {

    	return ipCookie('fullName');
    };
    
    this.getDisplayName = function()
    {
        return ipCookie('displayName');
    };

    this.getProfilePic = function()
    {
        return ipCookie('profilePic');
    };
    
    this.isProfileComplete = function() {
        
        return ipCookie('isProfileComplete');
    };
    
    this.deleteSession = function() {

        ipCookie.remove('userId');
        ipCookie.remove('emailId');
        ipCookie.remove('displayName');
        ipCookie.remove('isProfileComplete');
        ipCookie.remove('profilePic');
        ipCookie.remove('userScore');
        ipCookie.remove('fullName');

    };
    
    this.profileCompleted = function()
    {
        ipCookie('isProfileComplete',1,{ expires: 365 });
    };

    this.checkProfileComplete = function()
    {
      return ipCookie('isProfileComplete');
    };

    this.changeProfilePic = function(url) {

       ipCookie.remove('profilePic');
       ipCookie('profilePic',url,{ expires: 365 });

    };
    return this;

}]);

_revlegApp.service('sessionService', ["ipCookie",function (ipCookie) {

  this.create = function (basicInfoObj) {

       ipCookie('userId',basicInfoObj.userId,{ expires: 365 });
       ipCookie('emailId',basicInfoObj.emailId,{ expires: 365 });
       ipCookie('fullName',basicInfoObj.fullName,{ expires: 365 });
       ipCookie('displayName',basicInfoObj.displayName,{ expires: 365 });
       ipCookie('profilePic',basicInfoObj.profilePic,{ expires: 365 });
       ipCookie('userScore',basicInfoObj.userScore,{ expires: 365 });
      
       if(basicInfoObj.profileCompleted == "0")
       {
        ipCookie('isProfileComplete',0,{ expires: 365 });
       }
      else {

       ipCookie('isProfileComplete',1,{ expires: 365 });

       }
  };
  this.destroy = function () {

      ipCookie.remove('userId');
      ipCookie.remove('emailId');
      ipCookie.remove('displayName');
      ipCookie.remove('isProfileComplete');
      ipCookie.remove('profilePic');
      ipCookie.remove('userScore');
  };
  return this;

}]);

_revlegApp.service('searcherService',[function(){

  this.searchedValue = "";
  this.resultSet = null;
  this.offset = 0;
  
  this.saveSearchResult = function(input,result,offset)
  {
    this.searchedValue = input;
    this.resultSet = result;
    this.currentOffset = offset;
  };

  this.getSearchValue = function()
  {
    return this.searchedValue;
  };

  this.getResultSet = function()
  {
    return this.resultSet;
  };

  this.getOffset = function()
  {
    return this.offset;
  };

  return this;

}]);


_revlegApp.service('intendedURLService',[function(){

  this.storedURL = null;
  
  this.saveURL = function(input)
  {
    this.storedURL = input;
  };

  this.getURL = function()
  {
    return this.storedURL;
  };


  return this;

}]);


_revlegApp.service('locationService',[function(){

    this.getLocation = function(response)
    {
        var formattedAddressArray = response.results[0].formatted_address.split(",");
        var countryRaw = formattedAddressArray[formattedAddressArray.length - 1];
        var stateRaw = formattedAddressArray[formattedAddressArray.length - 2];
        var cityRaw = formattedAddressArray[formattedAddressArray.length - 3];
        
        var obj = {};
        obj.country = countryRaw.replace(/[0-9]/g, '');
        obj.state = stateRaw.replace(/[0-9]/g, '');
        obj.city = cityRaw.replace(/[0-9]/g, '');
        
        return obj;
        
    }
    
    return this;
    
}]);

_revlegApp.service('socketService',function(){
    
    this.socketObject = null;

    this.saveSocketObject = function(object)
    {
        this.socketObject = object;
    };
    
    this.getSocketObject = function()
    {
        return this.socketObject;
    }
    
    return this;
    
});

_revlegApp.service('anchorSmoothScroll', [function(){
    
    this.scrollTo = function(eID) {

        // This scrolling function 
        // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript
        
        var startY = currentYPosition();
        var stopY = elmYPosition(eID);
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY); return;
        }
        var speed = Math.round(distance / 100);
        if (speed >= 20) speed = 20;
        var step = Math.round(distance / 25);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for ( var i=startY; i<stopY; i+=step ) {
                setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
                leapY += step; if (leapY > stopY) leapY = stopY; timer++;
            } return;
        }
        for ( var i=startY; i>stopY; i-=step ) {
            setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
            leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
        }
        
        function currentYPosition() {
            // Firefox, Chrome, Opera, Safari
            if (self.pageYOffset) return self.pageYOffset;
            // Internet Explorer 6 - standards mode
            if (document.documentElement && document.documentElement.scrollTop)
                return document.documentElement.scrollTop;
            // Internet Explorer 6, 7 and 8
            if (document.body.scrollTop) return document.body.scrollTop;
            return 0;
        }
        
        function elmYPosition(eID) {
            var elm = document.getElementById(eID);
            var y = elm.offsetTop;
            var node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop;
                y-=90;
            } return y;
        }

    };
    
}]);
