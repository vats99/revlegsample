_revlegApp.controller('applicationController',["$scope" , "authService" , "$rootScope" , "$location" , "intendedURLService" , "$route" , "$routeParams", "logoutFactory", "$analytics" , "$modal" , "socketService" , 

   function ($scope,authService,$rootScope,$location,intendedURLService,$route,$routeParams,logoutFactory,$analytics,$modal,socketService) { 
                  
       $scope.logOut = function()
       {
           
           $analytics.eventTrack('_openreferus-click', {  category: 'open refer model', label: 'null' });  
           
            var url  = appConfig.API_HOST + appConstants.LOGOUT;

            logoutFactory.logout(url)
            .success(function(response){

                if(response.status == "success")
                {
                       
                    
                        authService.deleteSession();
                        $location.path('/search');
                    
                        console.log("connection disconnet");
                        var socketConnection = socketService.getSocketObject();
                        socketConnection.disconnect();

                    
                }
            })
            .error(function(){

                alert("some error");
            });
           

       }
       
       
       $scope.$route = $route;
       $scope.$location = $location;
       $scope.$routeParams = $routeParams;
       
       $rootScope.$on('event-signin-success',function(){
        
           var url = intendedURLService.getURL();
           
           if(url != null)
           {
               
               $location.path(url);
               $route.reload();
               intendedURLService.saveURL(null);
               
           }
       
       });
       
}]);

_revlegApp.controller('menuController',["$scope" , "authService" , "$rootScope" , "$location" , "$modal" ,

   function ($scope,authService,$rootScope,$location,$modal) { 


        $scope.isMenuVisible = !(authService.checkLogin());
            
        $scope.$watch(
        function(){ return  authService.checkLogin() },

        function(newVal) {
          $scope.isMenuVisible = newVal;
        }
       );  

}]);


_revlegApp.controller('userController',["$scope" , "authService" , "$rootScope" , "$location" , "$modal" , "$analytics" ,

   function ($scope,authService,$rootScope,$location,$modal,$analytics) { 


        $scope.user = {};
        $scope.user.fullName = authService.getFullName();
        $scope.user.profilePic = authService.getProfilePic();

       $scope.$watch(
           function(){ return  authService.getFullName() },

           function(newVal) {
               $scope.user.fullName = newVal;
           }
       );

       $scope.$watch(
           function(){ return  authService.getProfilePic() },

           function(newVal) {
               $scope.user.profilePic = newVal;
           }
       );


        // need to check from the cookie if he needs to fill or
        // if he has exhausted all the referals
        $scope.isReferVisible = true;
        
        // opening the referral modal 
        $scope.openRefer = function () {
            
        $analytics.eventTrack('_openreferus-click', {  category: 'open refer model', label: 'null' });  
            
//         var modalInstance = $modal.open({
//          templateUrl: 'views/referModal.html',
//          controller: 'referModalController',
//         });
            
                 
         var modalInstance = $modal.open({
          size : 'lg',     
          templateUrl: 'views/referModalNew.html',
          controller: 'referModalController',
         });       
                
        };
       
       //for opening the suggestions modal window
       $scope.openFeedback = function () {

        $analytics.eventTrack('_feedback-click', {  category: 'open feedback model', label: 'null' });     
           
         var modalInstance = $modal.open({
          templateUrl: 'views/suggestionsModal.html',
          controller: 'suggestionsController',
         });
             
        };

}]);


_revlegApp.controller('suggestionsController', ['$scope' , '$modalInstance', 'suggestionFactory' , function ($scope, $modalInstance , suggestionFactory) {

  $scope.feedback = {};
  $scope.feedback.text = "";
     
  $scope.ok = function () {
    
      
      var url  = appConfig.API_HOST + appConstants.SUGGESTIONS;
      var formData = new FormData();
      
      formData.append('feedback',$scope.feedback.text);
      
      suggestionFactory.send(formData,url)
        .success(function(response){
            
            $modalInstance.dismiss('cancel');
        })
        .error(function(){
            
            alert("some error");
        });
      
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
         
    
}]);

_revlegApp.controller('referModalController', ['$scope' , '$modalInstance', 'invitationFactory' , '$analytics' , function ($scope, $modalInstance , invitationFactory , $analytics) {

 
  $scope.cancel = function () {
      
    $analytics.eventTrack('_feedback_cancel-click', {  category: 'open feedback model', label: 'null' });
      
    $modalInstance.dismiss('cancel');
  };
    
  $scope.isForm = "false";
    
  $scope.$watch(function() {
          return $scope.isForm;
      }, function(newValue, oldValue) {
         
    if(newValue == "true")
    {
        $modalInstance.close();

    }
  });      
  
    
}]);


_revlegApp.controller('sideMenuController',["$scope" , "authService" , "$rootScope" , "$location" , "$modal" ,

   function ($scope,authService,$rootScope,$location,$modal) { 


        $scope.isSideMenuVisible = !(authService.checkLogin());
            
        $scope.$watch(
        function(){ return  authService.checkLogin() },

        function(newVal) {
          $scope.isSideMenuVisible = newVal;
        }
       );  

}]);

_revlegApp.controller('navbarController',["$scope" , "authService" , "$rootScope" , "$location" , "$modal" , "$analytics" , "$routeParams","$route" ,

   function ($scope,authService,$rootScope,$location,$modal,$analytics,$routeParams,$route) { 

       
       $scope.loggedOut = !authService.checkLogin();
       
       $scope.$watch(
        function(){ return  authService.checkLogin() },

        function(newVal) {
          $scope.loggedOut = !newVal;
        }
       );
       
       
       $scope.signUp = function () {

         var modalInstance = $modal.open({
          templateUrl: 'views/signUpModal.html',
          controller: 'signUpController',
         });
             
        }; 
       
        $scope.signIn = function () {
            
            
             
            

        $analytics.eventTrack('_signin-click', {  category: 'signinclicked', label: 'null' });      
            
         var modalInstance = $modal.open({
          templateUrl: 'views/signIn.html',
          controller: 'signInController',
         });
             
        };
       
       $scope.requestForInvitation = function () {

       $analytics.eventTrack('_requestforinvitation-click', {  category: 'request for invitation', label: 'null' });             
           
         var modalInstance = $modal.open({
          templateUrl: 'views/requestForInvitation.html',
          controller: 'reqForInvController',
         });
             
        };
       
       $rootScope.$on('event-signIn',function(){
        
       $analytics.eventTrack('_signin-inbox-mail-flow', {  category: 'inbox mail flow', label: 'null' });
           
             var modalInstance = $modal.open({
              templateUrl: 'views/signIn.html',
              controller: 'signInController',
             });                          
       });

}]);


_revlegApp.controller('suggestorController',["$scope","$location","$http","searcherFactory","searcherService","$rootScope","$routeParams", "$modal", "$analytics" , "$interval" , 
    function($scope,$location,$http,searcherFactory,searcherService,$rootScope,$routeParams,$modal,$analytics,$interval){
        
    
    /** adding the **/    
//     $("img.youtube").YouTubePopup({ hideTitleBar: true });
                    $("a.youtube").YouTubePopup({ hideTitleBar: true });
                      $('[data-toggle="tooltip"]').tooltip();        
               
    var query = $routeParams.q;
    var page = $routeParams.page;
    $scope.asyncSelected = query;    
    $scope.isResultSetDisplay = false;
    var resultSet = [];    
        
    var placeholder = [];
    placeholder[0] = "education from Indian Institute of Technology";
    placeholder[1] = "passionate about technology";
    placeholder[2] = "living in Karnataka";
         
    placeholder[3] = "education from Indian Institute of Information Technology";    
    placeholder[4] = "passionate about gadgets";
    placeholder[5] = "living in  Uttar pradesh";    
             
    placeholder[6] = "passionate about Android"; 
    placeholder[7] = "living in Rajasthan"; 
        
    var index = 0;
        
    $interval(function() {
       $scope.mainSearchPlaceHolder = placeholder[index++ % placeholder.length];
    }, 2500);  
        
        
        
//    $scope.mainSearchPlaceHolder = "Start by typing : working at ... , education from ... , passionate about ..., living in ... "; 
       
        
    if(page == undefined)
    {
        page = 1;
    }
        
    if(query != undefined)
    {
        NProgress.start();    
        $scope.mainHeading = false;
        $rootScope.$broadcast("removeImage");
        $scope.isResultSetDisplay = true;
        
        // calling search api to get the result set //
        
        var searchInput = query;
        var searcherUrl = appConfig.API_HOST + appConstants.SEARCHER+ "?q=" + searchInput+"&page="+page;
        
        searcherFactory.search(searcherUrl)
        .success(function(response) {
            
            NProgress.done();  
            
            /** search event being registered **/  
            //_gaq.push(['_trackEvent','resultSet', query , response.data.response.results]);  
            
            
            var offset = 0;
            resultSet = response.data.response.results;
            $scope.resultSet = resultSetChurner(response.data.response.results);
            
            // for pagination //            
            if($scope.resultSet != null) {

                  var totalresults = response.data.response.totalNumberOfResults;
                  $scope.itemsPerPage = appConstants.MAX_POST_PER_PAGE;

                  if(totalresults >= appConstants.MAX_POST_PER_PAGE) {

                    $scope.isPagination = true;
                    $scope.totalItems = totalresults;
                    $scope.currentPage = page;

                  }
                  else {

                    $scope.isPagination = false;
                    // no need pagination in this case //

                  }

              }
              else {

                $scope.isPagination = false;

              }
            
            // end of paginaton //
        
            
        })
        .error(function(){
            alert("some error");
        });
        
    }
    else 
    {
        $scope.mainHeading = true;
        $rootScope.$broadcast("addImage");
        
    }
        
            
        
  	var strip = function(html)
	{
	   var tmp = document.createElement("DIV");
	   tmp.innerHTML = html;
	   return tmp.textContent || tmp.innerText || "";
	}
  	$scope.setSearchInput = function(selected) {
  		$scope.asyncSelected = strip(selected.text);
        
        /** on select search **/
        
        if($scope.asyncSelected == "")
       {
       		alert("Enter something to search !!!!");
       }
       else{

       	   var searchInput = $scope.asyncSelected;
		   var searcherUrl = appConfig.API_HOST + appConstants.SEARCHER+ "?q=" + searchInput;
           
           $analytics.eventTrack('_search', {  category: 'keywords searched', label: searchInput });  
           
           // re routing it with query params attached;
//           $location.search('q',searchInput).path("/search");
             $location.search({'q':searchInput,'page':null}).path("/search");
       }   
        
        
  	};

    var capitaliseFirstLetter = function(string)
    {
        
        if(string.indexOf(".") == -1 && string.length >3 ){
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
        else {
            return string.toUpperCase();
        }
    };    
        
    $scope.getSuggestions = function(val) {
    return $http.get(appConfig.API_HOST + appConstants.SUGGESTOR, {
      params: {
         q: val,
        // sensor: false
      }
    }).then(function(res){
        
     /** search event being registered **/  
//     _gaq.push(['_searchEvent', val , res.data.data.response]);    
        
      var suggestion = [];
      resultSet = res.data.data.response;
      for( var i=0;i<resultSet.length;i++)
		{
			var suggestionObj=resultSet[i];
			var suggestionString='';
				
			for (var key in suggestionObj) {

			  if(key!='stats'){
			  	var obj = suggestionObj[key];
			  	var start = 'once';
			  	for (var lowerKey in obj) {
			  	//$.each(obj,function(ObjKey, ObjValue){

			  		if(lowerKey.indexOf("_text")>=0)
			  		{
			  			var columnArray = lowerKey.split("_");
			  			if(start=='once'){
			  				
			  				suggestionString+="<b>"+columnArray[0] +"</b> : "+capitaliseFirstLetter(obj[lowerKey])+' ';
			  				start='end';
			  			}
			  			else{
			  				
			  				suggestionString+=' , '+"<b>"+columnArray[0] +"</b> : "+capitaliseFirstLetter(obj[lowerKey]);
			  			}
			  			
			  		}

			  	}
			  	
			    suggestionString+='<b> and </b>';
			}

			

			}

			suggestionString=suggestionString.slice(0,-8);
			var obj = {};
			obj.text = suggestionString;
			obj.data = resultSet[i];
			suggestion[i] = obj;
		}
      return suggestion;
    });
    };

    $scope.searcher = function()
    {	
        
       if($scope.asyncSelected == "")
       {
       		alert("Enter something to search !!!!");
       }
       else{

       	   var searchInput = $scope.asyncSelected;
		   var searcherUrl = appConfig.API_HOST + appConstants.SEARCHER+ "?q=" + searchInput;
           
           $analytics.eventTrack('_search', {  category: 'keywords searched', label: searchInput });  
           
           // re routing it with query params attached;
           $location.search('q',searchInput).path("/search");
       }    	
    };
        
        
//    element.bind("keydown", function () {
//    if(event.which === 13) {
//
//        event.preventDefault();  
//        if($scope.asyncSelected == "")
//       {
//       		alert("Enter something to search !!!!");
//       }
//       else{
//
//       	   var searchInput = $scope.asyncSelected;
//		   var searcherUrl = appConfig.API_HOST + appConstants.SEARCHER+ "?q=" + searchInput;
//           
//           $analytics.eventTrack('_search', {  category: 'keywords searched', label: searchInput });  
//           
//           // re routing it with query params attached;
//           $location.search('q',searchInput).path("/search");
//       }   
//    }
//    });
        
    var resultSetChurner = function(input) {

    	var res = {};
	    res.data = input;
	    var resultSet = {};
        var suggestion = [];
	    for( var i=0;i<res.data.length;i++)
		{
			var numberOfUser = res.data[i].stats.netUserId.length;
			var suggestionObj=res.data[i];
			var suggestionString='';
				
			for (var key in suggestionObj) {

			  if(key!='stats'){
			  	var obj = suggestionObj[key];
			  	var start = 'once';
			  	for (var lowerKey in obj) {
			  	//$.each(obj,function(ObjKey, ObjValue){

			  		if(lowerKey.indexOf("_text")>=0)
			  		{
			  			var columnArray = lowerKey.split("_");
			  			if(columnArray[0] == 'country')
			  			{
			  				columnArray[0] = "" + columnArray[0]+" ";
			  			}

			  			if(start=='once'){
			  				
			  				suggestionString+=" "+columnArray[0] +" : "+capitaliseFirstLetter(obj[lowerKey])+' ';
			  				start='end';
			  			}
			  			else{
			  				
			  				suggestionString+=' , '+" "+columnArray[0] +" : "+capitaliseFirstLetter(obj[lowerKey]);
			  			}
			  			
			  		}

			  	}
			  	
			    suggestionString+=' and ';
			}

			

			}

			suggestionString=suggestionString.slice(0,-4);
			var obj = {};
			obj.text = suggestionString;
			obj.data = res.data[i];
			suggestion[i] = obj;
			var obj2 = {};
			obj2.title = obj.text;
			obj2.descp = 'some description';
			obj2.totalPeopleCount = numberOfUser;
			resultSet[i] = obj2;
		}

		return resultSet;

    };
        
        
    $scope.pageChanged = function()
    {
  	   var changePagedInto = $scope.currentPage;
//       var searchInput = $scope.asyncSelected;
       var searchInput = query;
       var searcherUrl = appConfig.API_HOST + appConstants.SEARCHER+ "?q=" + searchInput;

       // re routing it with query params attached;
       $location.search({'q':searchInput,'page':changePagedInto}).path("/search");
  	   
    };
        
    $scope.sendMessage = function (index) {

    $analytics.eventTrack('_message-create-click', {  category: 'message-create', label: 'null' });       
        
     var modalInstance = $modal.open({
      templateUrl: 'views/surveyMessageModal.html',
      controller: 'createMessageController',
      size : 'lg',
      resolve: {
        audienceList: function() {
            return resultSet[index].stats.netUserId;
        }
      }
     });

    };
                    
}]);
       
_revlegApp.controller('signInController', ['$scope' , '$modalInstance', 'intendedURLService', '$location', '$analytics', 'Facebook' , 'ipCookie', 'sessionService' , 'formFactory' , '$rootScope' , '$timeout' , function ($scope, $modalInstance , intendedURLService , $location , $analytics , Facebook , ipCookie , sessionService , formFactory , $rootScope , $timeout) {
    
    
 $rootScope.ref_userId = "";
 $rootScope.ref_refCode = "";    
    
 $scope.login = function() {
     
     $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: 'white', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: 1, 
                color: '#fff' 
            } }); 
     
      $('.blockMsg').html("<h1 style='color:green;'>Please wait</h1>");
     
      // From now on you can use the Facebook service just as Facebook api says
      Facebook.login(function(response) {
        // Do something with response.
          
        if(response.status === 'connected') {
  
            console.log(response);
            Facebook.api('/me', function(response) {
                
                $scope.user = response;
                console.log(response);
                
                if(response.first_name != undefined)
                {
                var first_name = response.first_name;
                }
                
                if(response.last_name != undefined)
                {
                var last_name = response.last_name;
                }
                
                if(response.name != undefined)
                {    
                var name = response.name;
                } 
                if(response.email != undefined)
                {
                    var emailId = response.email;
                    var userId = response.id;

                    var formData = new FormData();

                    formData.append('emailId',emailId);
                    formData.append('fullName',name);
                    formData.append('userType','3');
                    formData.append('referCode',$rootScope.ref_refCode);
                    formData.append('referedBy',$rootScope.ref_userId);
                    
                    var loginSubmitUrl = appConfig.API_HOST + appConstants.FB_LOGIN;
                    formFactory.submit(formData, loginSubmitUrl)
                    .success(function(response) {
                        $scope.loader = "true";

                       if(response.status == 'success')
                       {
                            /** to be changed from username to displayname**/
                            sessionService.create(response.data.userData.basicInfo);
                            $rootScope.$broadcast('event-signin-success');

                            if(ipCookie('isProfileComplete') == false)
                            {
                                $location.path("/profile");
                            }

                            $.unblockUI(); 
                            $modalInstance.close();

                       }
                       else  {

                            $('.blockMsg').html("<h1 style='color:red;'>"+response.data.message+"</h1>");
                            $timeout(function(){
                                $.unblockUI();
                            },2000); 
                       }
                    }).error(function(){

                            
                            $('.blockMsg').html("<h1 style='color:red;'>Some error from backend</h1>");
                            $timeout(function(){
                                $.unblockUI();
                            },2000); 
                        

                    });
                    
                }
                else {
                
                    $('.blockMsg').html("<h3 style='color:red;'>Unable to get your emailId through facebook</h3><br><span style='color:black;'>Check your facebook account settings or login using RevLeg Credentials.</span>");
                    $timeout(function(){
                        $.unblockUI();
                    },5000); 
                    
                }
                
            });
            
        } else {
            
            $('.blockMsg').html("<h1 style='color:red;'>Facebook Login Cancelled</h1>");
            $timeout(function(){
                $.unblockUI();
            },2000); 
        }
      },{scope:'email'});
 };

        
  $scope.isForm = "false";
    
  $scope.$watch(function() {
          return $scope.isForm;
      }, function(newValue, oldValue) {
         
            if(newValue == "true")
            {
                $modalInstance.close();
                
            }
  });   
        
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
    
      if(intendedURLService.getURL()!= null)
      {
          $location.path("/search");
      }
      
  };
    
}]); 


_revlegApp.controller('modalInfoController',['$scope' , '$modalInstance',function($scope,$modalInstance){

    $scope.cancel =  function () {
        $modalInstance.dismiss('cancel');
    };
    
}]);

_revlegApp.controller('reqForInvController', ['$scope' , '$modalInstance', 'invitationFactory', 'formFactory' , 'SweetAlert' , function ($scope, $modalInstance , invitationFactory , formFactory , SweetAlert) {

  $scope.singleRequest = {name : "" , email : ""};
       
  $scope.submit = function () {
    
          var finalData  = new Array();


          var obj = {};
          obj.emailId = $scope.singleRequest.email;
          obj.name = $scope.singleRequest.name;

          //hardcoding the email//
          //obj.emailId = "praveen.menon.88@gmail.com";

          finalData.push(obj);


          var url  = appConfig.API_HOST + appConstants.INVITATION;
          var formData = new FormData();

          formData.append('referList',JSON.stringify(finalData));

          formFactory.submit(formData,url)
            .success(function(response){
                
                if(response.status == "success")
                {
                
                SweetAlert.swal("GREAT!", "Check your mail for the invitation link", "success")    
                    
                $modalInstance.dismiss('cancel');
                }
                else {
                
                SweetAlert.swal("Oops...", "Something went wrong!", "error");    
                    
                }
            })
            .error(function(){

                alert("some error");
            });
      
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
    
}]); 




_revlegApp.controller('createMessageController', ['$scope' , '$modalInstance', "authService", "messageFactory" , "audienceList", function ($scope, $modalInstance , authService , messageFactory , audienceList) {

 $scope.message = { messageTitle : "",messageBody : ""};
    
 var messageBodyLimit = 300;
 var messageTitleLimit = 100;
    
 $scope.isMessageSubmit = true;
 $scope.isCancelClicked = "no";    
     
 $scope.messageBodyAlert = "The total characters left " + messageBodyLimit ;
 $scope.messageTitleAlert = "The total characters left " + messageTitleLimit;
    

 $scope.audienceList = audienceList;    
    
 $scope.title = function()
 {
   if($scope.message.messageTitle.length >= messageTitleLimit )
   {
      $scope.messageTitleAlert = "Sorry you have exceeded the limit";
      $scope.message.messageTitle = $scope.message.messageTitle.substring(0,$scope.message.messageTitle.length - 1);
   }
   else {
   
     $scope.messageTitleAlert = "The total characters left " + (messageTitleLimit-$scope.message.messageTitle.length);
   }
 };
 
 
 $scope.body = function()
 {
   if($scope.message.messageBody.length >= messageBodyLimit )
   {
      $scope.messageBodyAlert = "Sorry you have exceeded the limit";
      $scope.message.messageBody = $scope.message.messageBody.substring(0,$scope.message.messageBody.length - 1);
   }
   else {
   
     $scope.messageBodyAlert = "The total characters left " + (messageBodyLimit-$scope.message.messageBody.length);
   }
 };
    
    
 if(!authService.checkLogin())
 {

    // when not logged in
    $scope.isCreateMessage = false;
 }
 else {
 
    // when logged in //
    $scope.isCreateMessage = true; 
     
 }    
    
    
//  $scope.send = function () {
//    
//      var title = $scope.message.messageTitle;
//      var body = $scope.message.messageBody;
//      var audeinceListString = audienceList.toString();
//      
//      var isTitleFine = false;
//      var isBodyFine = false;
//      
//      if(title.length > 0 && title.length < messageTitleLimit)
//      {
//         isTitleFine = true;
//      }
//      else {
//        
//          isTitleFine = false;
//      }
//      
//      
//      if(body.length > 0 && body.length < messageBodyLimit)
//      {
//         isBodyFine = true;
//      }
//      else {
//        
//          isBodyFine = false;
//      }
//      
//      
//      
//      if(isBodyFine == true && isTitleFine == true)
//      {
//          var formData = new FormData();
//          formData.append('messageSubject',title);
//          formData.append('messageBody',body);
//          formData.append('audienceBlock',audeinceListString);
//          
//          // sending the message //
//          var messageCreateUrl = appConfig.API_HOST + appConstants.MESSAGE_CREATE;
//          messageFactory.send(formData, messageCreateUrl)
//          .success(function(response) {
//              $scope.loader = "true";
//
//               if(response.status == 'success')
//               {
//                    $modalInstance.dismiss('cancel');
//               }
//               else{
//
//                    $scope.loader = "true";
//               }
//
//            }).error(function(){
//
//                    $scope.loader = "true";
//                    $scope.form = "true"; 
//
//                });    
//          
//          
//      }
//      else {
//         
//          $scope.isMessageSubmit = false;
//          
//          if (isTitleFine == false)
//          {
//              $scope.afterMessageSubmitAlert = "Title length is not correct";
//          }
//          if (isBodyFine == false)
//          {
//              $scope.afterMessageSubmitAlert = "Description length not correct";
//          }
//        
//      }
//      
//      //messageFactory.send();
//      
//      
//      
//  };

  $scope.cancel = function () {
     $scope.isCancelClicked = "yes";   
    $modalInstance.dismiss('cancel');
  };
    
  $scope.isForm = "false";
    
  $scope.$watch(function() {
          return $scope.isForm;
      }, function(newValue, oldValue) {
         
            if(newValue == "true")
            {
                $modalInstance.close();
                
            }
  });    
    
    
}]); 


_revlegApp.controller('signUpController', ['$scope', '$modalInstance' , 'Facebook' , 'formFactory' , 'ipCookie', '$rootScope', 'sessionService','$location', '$timeout' , function ($scope,$modalInstance,Facebook,formFactory,ipCookie,$rootScope,sessionService,$location,$timeout) {

    
  $scope.isMessageDisplay = true;
  $scope.isForm = "false";
    
  $rootScope.ref_userId = "";
  $rootScope.ref_refCode = "";     
    
  $scope.$watch(function() {
          return $scope.isForm;
      }, function(newValue, oldValue) {
         
    if(newValue == "true")
    {
        $modalInstance.close();

    }
  });     
    
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };    
    
  
  $scope.signUpFB = function() {
      
     $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: 'white', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: 1, 
                color: '#fff' 
            } }); 
     
      $('.blockMsg').html("<h1 style='color:green;'>Please wait</h1>");
     
      // From now on you can use the Facebook service just as Facebook api says
      Facebook.login(function(response) {
      // Do something with response.
        if(response.status === 'connected') {
  
            console.log(response);
            Facebook.api('/me', function(response) {
                
                $scope.user = response;
                console.log(response);
                
                if(response.first_name != undefined)
                {
                var first_name = response.first_name;
                }
                
                if(response.last_name != undefined)
                {
                var last_name = response.last_name;
                }
                
                if(response.name != undefined)
                {    
                var name = response.name;
                } 
                if(response.email != undefined)
                {
                    var emailId = response.email;
                    var userId = response.id;

                    var formData = new FormData();

                    formData.append('emailId',emailId);
                    formData.append('fullName',name);
                    formData.append('userType','3');
                    formData.append('referCode',$rootScope.ref_refCode);
                    formData.append('referedBy',$rootScope.ref_userId);
                    
                    
                    var loginSubmitUrl = appConfig.API_HOST + appConstants.FB_LOGIN;
                    formFactory.submit(formData, loginSubmitUrl)
                    .success(function(response) {
                        $scope.loader = "true";

                       if(response.status == 'success')
                       {
                            /** to be changed from username to displayname**/
                            sessionService.create(response.data.userData.basicInfo);
                            $rootScope.$broadcast('event-signin-success');

                            if(ipCookie('isProfileComplete') == false)
                            {
                                $location.path("/profile");
                            }

                            $.unblockUI(); 
                            $modalInstance.close();

                       }
                       else  {

                            $('.blockMsg').html("<h1 style='color:red;'>"+response.data.message+"</h1>");
                            $timeout(function(){
                                $.unblockUI();
                            },2000); 
                       }
                    }).error(function(){

                            
                            $('.blockMsg').html("<h1 style='color:red;'>Some error from backend</h1>");
                            $timeout(function(){
                                $.unblockUI();
                            },2000); 
                        

                    });
                    
                }
                else {
                
                    $('.blockMsg').html("<h3 style='color:red;'>Unable to get your emailId through facebook</h3><br><span style='color:black;'>Check your facebook account settings or Sign up using RevLeg singup form.</span>");
                    $timeout(function(){
                        $.unblockUI();
                    },5000); 
                }
                
            });
            
        } else {
            
            $('.blockMsg').html("<h1 style='color:red;'>Facebook Login Cancelled</h1>");
            $timeout(function(){
                $.unblockUI();
            },2000); 
        }
      },{scope:'email'});
      
  };    
    
    
}]); 

_revlegApp.controller('howItWorksController', ['$scope', '$location', '$anchorScroll' , function ($scope , $location, $anchorScroll) {

    
    $scope.gotoAnchor = function(id) {
      // set the location.hash to the id of
      // the element you wish to scroll to.
      $location.hash(id);

      // call $anchorScroll()
      $anchorScroll();
    };
    
    
}]);



_revlegApp.controller('profileController', ['$scope'  , 'templatesFactory' , "$http", 'ipCookie', '$modal', '$routeParams' , "$location", "growl" , "getLocationList" , "$rootScope"  , "authService" , 
function ($scope,templatesFactory,$http,ipCookie,$modal,$routeParams,$location,growl,getLocationList,$rootScope,authService) {

        /** for initiating tooltip **/
        $('[data-toggle="tooltip"]').tooltip();
    
    
        var viewIsCompleteCount = 0; 
        /** checking for disabling the div **/
        //$('#collegeForm').disable();
    
        /** For autocomplete for all the typeaheads **/

        $scope.profileInput = {};
        $scope.forAdmin = "no";
    
        var capitaliseFirstLetterForArray = function(inputArray)
        {

            for( var i = 0; i < inputArray.length ; i++)
            {
                var string = inputArray[i]; 
                if(string.indexOf(".") == -1 && string.length >3 ){
                    inputArray[i] =  string.charAt(0).toUpperCase() + string.slice(1);
                }
                else {
                    inputArray[i] =  string.toUpperCase();
                }            
            }

            return inputArray;
        }; 
    
        var capitaliseFirstLetter = function(string)
        {

            if(string.indexOf(".") == -1 && string.length >3 ){
                return string.charAt(0).toUpperCase() + string.slice(1);
            }
            else {
                return string.toUpperCase();
            }
        };  
    
        var ucFirstAllWords = function(string)
        {
            var pieces = string.split(" ");
            for ( var i = 0; i < pieces.length; i++ )
            {
                var j = pieces[i].charAt(0).toUpperCase();
                pieces[i] = j + pieces[i].substr(1);
            }
            return pieces.join(" ");
        }
        
        $scope.getSingleValueProfileSuggestions = function(val,field) {

        /** old way of calling **/   
//        var autocompleteURL = appConfig.API_HOST + appConstants.PROFILE_SUGGESTION + "/"+field + "/" + val;    
//        return $http.get(autocompleteURL, {
//        }).then(function(res){
//          return res.data.data.suggestion;
//        });
        
            
        if(field == 'college')
        {
            var autocompleteURL = appConfig.API_HOST + appConstants.GET_COLLEGE_SUGGESTIONS + "/" + val;    
        }
        
        if(field == 'school')
        {
            var autocompleteURL = appConfig.API_HOST + appConstants.GET_SCHOOL_SUGGESTIONS + "/" + val;    
        }   
            
        if(field == 'coaching')
        {
            var autocompleteURL = appConfig.API_HOST + appConstants.GET_COACHING_SUGGESTIONS + "/" + val;    
        }  
                        
        if(field == 'branch')
        {
            var autocompleteURL = appConfig.API_HOST + appConstants.GET_COLLEGE_BRANCH_SUGGESTIONS + "/" + val;    
        }       
            
        return $http.get(autocompleteURL, {
            }).then(function(res){

               if(res.data.data.suggestion.length > 7)
               {
                   return res.data.data.suggestion.slice(0,7);
               }
               else {

                   return res.data.data.suggestion;
               }


            });    
        };
    
    
        $scope.getSingleValueCitySuggestions = function(val) {

        var autocompleteURL = appConfig.API_HOST + appConstants.LOCATION_LIST + "/india" + "/other"+ "/" + val ;
        return $http.get(autocompleteURL, {
        }).then(function(res){
                if(res.data.data.suggestion.length > 7)
                {
                    return res.data.data.suggestion.slice(0,7);
                }
                else {

                    return res.data.data.suggestion;
                }
        });
        };

        $scope.getSingleValueArea2Suggestions = function(val) {

        var autocompleteURL = appConfig.API_HOST + appConstants.GET_AREA_SUGGESTION + "/" + $('#inputState2')[0].value + "/" + $('#inputCity2')[0].value + "/" + val ;
        return $http.get(autocompleteURL, {
        }).then(function(res){
                if(res.data.data.suggestion.length > 7)
                {
                    return res.data.data.suggestion.slice(0,7);
                }
                else {

                    return res.data.data.suggestion;
                }
            });
        };

        $scope.getSingleValueArea3Suggestions = function(val) {

        var autocompleteURL = appConfig.API_HOST + appConstants.GET_AREA_SUGGESTION + "/karnataka/bengaluru"+ "/" + val ;
        return $http.get(autocompleteURL, {
        }).then(function(res){
                if(res.data.data.suggestion.length > 7)
                {
                    return res.data.data.suggestion.slice(0,7);
                }
                else {

                    return res.data.data.suggestion;
                }
            });
        };


        $scope.getSingleValueAreaSuggestions = function(val) {

        var autocompleteURL = appConfig.API_HOST + appConstants.LOCATION_LIST + "/india" + "/karnataka/bengaluru"+ "/" + val ;
        return $http.get(autocompleteURL, {
        }).then(function(res){
                if(res.data.data.suggestion.length > 7)
                {
                    return res.data.data.suggestion.slice(0,7);
                }
                else {

                    return res.data.data.suggestion;
                }
        });
        };
        /** end **/   
    
        $scope.loadSuggestions = function(val,field) {

        return $http.get(appConfig.API_HOST + appConstants.PROFILE_SUGGESTION, {
              params: {
                 field: field,
                 input: val
              }
            }).then(function(res){
                if(res.data.data.suggestion.length > 7)
                {
                    return res.data.data.suggestion.slice(0,7);
                }
                else {

                    return res.data.data.suggestion;
                }
            });
         };
        
        /** end **/
    
       
        if($routeParams.authCode == undefined)
        {    
            var getTemplateUrl = appConfig.API_HOST + appConstants.TEMPLATES_ALL;    
            //var getTemplateUrl = 'http://demo5963010.mockable.io/getprofile'; 
            
        }
        else {
            
            var getTemplateUrl = appConfig.API_HOST + appConstants.ADMIN_GET_SINGLE_PROFILE + "/" + $routeParams.authCode + "/" + $routeParams.userId;
            
            //var getTemplateUrl = 'http://demo5963010.mockable.io/getprofile'; 
            $scope.forAdmin = "yes";
            $scope.userIdForEdit = $routeParams.userId;
        }
    
        templatesFactory.getAll(getTemplateUrl)
        .success(function(response) {
            
            
            var templatesResponse = response;
            
            if($scope.forAdmin == "no") {
                if(!ipCookie('isProfileComplete'))
                {    
                var modalInstance = $modal.open({
                  templateUrl: 'views/profileInfo.html',
                  controller: 'modalInfoController', 
                 });
                }
                $scope.isAlertDisplay = false;

                if(!ipCookie('isProfileComplete'))
                {
                     $scope.isAlertDisplay = true;
                }
            }
            
            var templatesArray = response.data.data.templates;
            var headerArray = response.data.data.header;
            
            $scope.profile = {};
            //var profileObj = angular.fromJson($cookieStore.get('profile'));
            
            if($scope.forAdmin == "no") {
                $scope.profile.fullName = ipCookie('fullName');
                $scope.profile.displayName = ipCookie('displayName');
                $scope.profile.emailId = ipCookie('emailId');
                $scope.profile.userScore = ipCookie('userScore');
                
            }
            else {
                $scope.profile.fullName = response.data.data.fullName;
                $scope.profile.displayName = response.data.data.displayName;
                $scope.profile.emailId = response.data.data.emailId;
            }
            
            $scope.location = {};
            $scope.college = {};
            $scope.company = {};
            $scope.passion = {};
            $scope.school = {};
            $scope.coaching = {};
            
            $scope.location.isComplete = response.data.data.header["1"];
            $scope.college.isComplete = response.data.data.header["3"];
            $scope.company.isComplete = response.data.data.header["4"];
            $scope.passion.isComplete = response.data.data.header["5"];
            $scope.school.isComplete = response.data.data.header["2"];
            $scope.coaching.isComplete = response.data.data.header["6"];
            
            $scope.location.switchStatus = ($scope.location.isComplete);
            $scope.college.switchStatus = ($scope.college.isComplete);
            $scope.company.switchStatus = ($scope.company.isComplete);
            $scope.passion.switchStatus = ($scope.passion.isComplete);
            $scope.school.switchStatus = ($scope.school.isComplete);
            $scope.coaching.switchStatus = ($scope.coaching.isComplete);
            
            
            /** company ui **/ 
            if($scope.company.switchStatus == true) {
               
                 $('#companyForm').block({ message: null, css : {cursor:"not-drop"} }); 
                 
            }
            $scope.$watch("company.switchStatus", function(newValue, oldValue) {
                 
                if(newValue == true)
                {
                   $('#companyForm').block({ message: null, css : {cursor:"not-drop"} }); 
                }
                else {
                    
                   $('#companyForm').unblock(); 
                }
            });
            
            
            /** school ui **/
            if($scope.school.switchStatus == true) {
               
                 $('#schoolForm').block({ message: null, css : {cursor:"not-drop"} }); 
            }
            
            $scope.$watch("school.switchStatus", function(newValue, oldValue) {
                
                if(newValue == true)
                {
                   $('#schoolForm').block({ message: null, css : {cursor:"not-drop"} }); 
                }
                else {
                    
                   $('#schoolForm').unblock(); 
                }
            });
            
            
            $scope.schoolCity = 'Bengaluru';
            $scope.coachingCity = 'Bengaluru';

            /** college ui **/
            if($scope.college.switchStatus == true) {
               
                 $('#collegeForm').block({ message: null, css : {cursor:"not-drop"} }); 
            }
            
            $scope.$watch("college.switchStatus", function(newValue, oldValue) {
                
                if(newValue == true)
                {
                   $('#collegeForm').block({ message: null, css : {cursor:"not-drop"} }); 
                }
                else {
                    
                   $('#collegeForm').unblock(); 
                }
            });
            
            
            /** location ui **/
            if($scope.location.switchStatus == true) {
               
                 $('#locationForm').block({ message: null, css : {cursor:'not-drop',backgroundColor:'#fff'} }); 
            }
            
            
            $scope.$watch("location.switchStatus", function(newValue, oldValue) {
                
                if(newValue == true)
                {
                   $('#locationForm').block({ message: null, css : {cursor:'not-drop',backgroundColor:'#fff'} }); 
                }
                else {
                    
                   $('#locationForm').unblock(); 
                }
            });
            
            
            /** passion ui **/       
            if($scope.passion.switchStatus == true) {
               
                 $('#passionForm').block({ message: null, css : {cursor:"not-drop"} }); 
            }
            
            
            $scope.$watch("passion.switchStatus", function(newValue, oldValue) {
                
                if(newValue == true)
                {
                   $('#passionForm').block({ message: null, css : {cursor:"not-drop"} }); 
                }
                else {
                    
                   $('#passionForm').unblock(); 
                }
            });
            
            /** coaching ui **/
            if($scope.coaching.switchStatus == true) {
               
                 $('#coachingForm').block({ message: null, css : {cursor:"not-drop"} }); 
            }
            $scope.$watch("coaching.switchStatus", function(newValue, oldValue) {
                
                if(newValue == true)
                {
                   $('#coachingForm').block({ message: null, css : {cursor:"not-drop"} }); 
                }
                else {
                    
                   $('#coachingForm').unblock(); 
                }
            });
            
            if(response.data.data.templates['1']!=undefined)
            {
            var locationObj = response.data.data.templates['1'];
            viewIsCompleteCount +=1;    
            }
            
            if(response.data.data.templates['3']!=undefined)
            {
            var collegeObj = response.data.data.templates['3'];
            viewIsCompleteCount +=1;      
            }
            
            if(response.data.data.templates['4']!=undefined)
            {
            var companyObj = response.data.data.templates['4'];
            viewIsCompleteCount +=1;      
            }
            
            if(response.data.data.templates['5']!=undefined)
            {
            var passionObj = response.data.data.templates['5'];
            }
            
            if(response.data.data.templates['2']!=undefined)
            {
            var schoolObj = response.data.data.templates['2'];
            viewIsCompleteCount +=1;    
            }
            
            if(response.data.data.templates['6']!=undefined)
            {
            var coachingObj = response.data.data.templates['6'];
            viewIsCompleteCount +=1;    
            }
            
//            $rootScope.viewIsCompleteCount = viewIsCompleteCount;
            
//            $rootScope.$watch("viewIsCompleteCount", function(newValue, oldValue) {
//                
//                if(!authService.isProfileComplete())
//                {
//                
//                if(newValue == 1 && oldValue!=1 )
//                {
//                 
//                    growl.addSuccessMessage('<b>Congrats: &nbsp;</b> One Complete one more to Go !!!');
//                }
//                /** when both the profiles are complete **/
//                if(newValue == 2 && oldValue!=2 )
//                {
//                    growl.addSuccessMessage('<b>Congrats: &nbsp;</b> Your Basic Profile is complete  <br> Now you can navigate !!!');
//                     authService.profileCompleted();
//                }
//                }
//            });
            
            /** for location **/    
            $scope.countryList = ["India"];
            $scope.country = "India";

            // for state //
            var getStateListUrl = appConfig.API_HOST + appConstants.LOCATION_LIST + "/india" + "/null" + "/null" ;
            getLocationList.get(getStateListUrl)
            .success(function(response) {

                if(response.status == "success")
                {

                    $scope.stateList = response.data.suggestion;
                    
                    if($scope.location.isComplete == 1 && templatesResponse.data.data.templates['1']!=undefined)
                    {

//                    $scope.state = ucFirstAllWords(locationObj.state[0]);

                        $scope.state = locationObj.state[0];
                    var getCityListUrl = appConfig.API_HOST + appConstants.LOCATION_LIST + "/india" + "/" + $scope.state + "/null" ;
                    getLocationList.get(getCityListUrl)
                    .success(function(response) {

                        if(response.status == "success")
                        {

                            $scope.cityList = response.data.suggestion;
                            
                             if($scope.location.isComplete == 1 && templatesResponse.data.data.templates['1']!=undefined)
                             {
//                             $scope.city = ucFirstAllWords(locationObj.city[0]);
                                 $scope.city = locationObj.city[0];
                             }
                                  
                        }

                    })
                    .error(function(){
                        alert("some error");
                    });     
                        
                    }
                    else {
                    
                    $scope.state = "";    
                    
                    }
                }

            })
            .error(function(){
                alert("some error");
            });   
            
            //for area //

            if($scope.location.isComplete == 1 && templatesResponse.data.data.templates['1']!=undefined)
            {

              if(locationObj.area != undefined)
              {
                      $scope.area = locationObj.area[0];
              }
            
            }

            /** on year change **/
            $scope.onYearChange = function(newValue)
            {

                    $('#inputYear').val(newValue).change();
                    $('#collegeViewForm').bootstrapValidator('revalidateField', 'year');
            };
            $scope.onYearClick = function()
            {

                   if($('#inputYear').val() == null)
                    {

                    }
                    else {

                        $('#collegeViewForm').bootstrapValidator('revalidateField', 'year');
                    }
            };
            
            /** on year change **/
            $scope.onPreparingForChange = function(newValue)
            {

                    $('#inputPreparingFor').val(newValue).change();
                    $('#collegeViewForm').bootstrapValidator('revalidateField', 'preparingFor');
            };
            
            $scope.onPreparingForClick = function()
            {

                   if($('#inputPreparingFor').val() == null)
                    {

                    }
                    else {

                        $('#collegeViewForm').bootstrapValidator('revalidateField', 'preparingFor'); 
                    }
            };
            
            
            /** on class change **/
            $scope.onClassChange = function(newValue)
            {

                    $('#inputClass').val(newValue).change();
                    $('#schoolViewForm').bootstrapValidator('revalidateField', 'class');
            };
            $scope.onClassClick = function()
            {

                   if($('#inputCLass').val() == null)
                    {

                    }
                    else {

                        $('#schoolViewForm').bootstrapValidator('revalidateField', 'class'); 
                    }
            };
            
            /** on year change **/
            $scope.onSchoolPreparingForChange = function(newValue)
            {

                    $('#inputSchoolPreparingFor').val(newValue).change();
                    $('#schoolViewForm').bootstrapValidator('revalidateField', 'schoolPreparingFor');
            };
            
            $scope.onSchoolPreparingForClick = function()
            {

                   if($('#inputSchoolPreparingFor').val() == null)
                    {

                    }
                    else {

                        $('#schoolViewForm').bootstrapValidator('revalidateField', 'schoolPreparingFor'); 
                    }
            };
            /** end of populating listing **/
            
            /** checking for change in the state value **/
            var firstTime = "yes";
            $scope.onStateChange = function(newValue) {

                    // HACK : using jquery to upgrade the value //
                    // revalidating the field //
                    if($('#inputCity2').val() != null)
                    {
                        var cityVal = null; 
                        $scope.city = null;
                        $('#inputCity2').val(cityVal).change();
                        $('#locationViewForm').bootstrapValidator('revalidateField', 'city'); 
                    }



                    // end //
                    // revalidating the field //

                    $('#inputState2').val(newValue).change();
                    $('#locationViewForm').bootstrapValidator('revalidateField', 'state');

                    //$scope.cityList = new Array();
                    var getCityListUrl = appConfig.API_HOST + appConstants.LOCATION_LIST + "/india" + "/" + newValue + "/null" ;
                    getLocationList.get(getCityListUrl)
                    .success(function(response) {

                        if(response.status == "success")
                        {

                            $scope.cityList = response.data.suggestion;
                            $scope.city = null;
                            $('#inputCity2').val($scope.city).change();

                            if(firstTime == "yes")
                            {
                               firstTime = "no";
                            }
                            else {

                                $('#locationViewForm').bootstrapValidator('revalidateField', 'city'); 
                            }

                        }

                    })
                    .error(function(){
                        alert("some error");
                    });    
            };



           $scope.onCityChange = function(newValue) {

                    // revalidating the field //
                    $('#inputCity2').val(newValue).change();
                    $('#locationViewForm').bootstrapValidator('revalidateField', 'city'); 
            };

            $scope.onCityClick = function()
            {

                   if($('#inputState2').val() == null)
                    {

                    }
                    else {

                        $('#locationViewForm').bootstrapValidator('revalidateField', 'state'); 
                    }
            };
            
            // location Typeahead //
            
            
            $scope.location.loader = true;
            $scope.location.form = true;
            $scope.location.messageDisplay =true;
            
            $scope.college.loader = true;
            $scope.college.form = true;
            $scope.college.messageDisplay =true;
            
            $scope.school.loader = true;
            $scope.school.form = true;
            $scope.school.messageDisplay =true;
            
            $scope.company.loader = true;
            $scope.company.form = true;
            $scope.company.messageDisplay =true;
            
            $scope.passion.loader = true;
            $scope.passion.form = true;
            $scope.passion.messageDisplay =true;
            
            $scope.coaching.loader = true;
            $scope.coaching.form = true;
            $scope.coaching.messageDisplay =true;

            
            
            
            
            
            if($scope.college.isComplete == 1 && response.data.data.templates['3']!=undefined)
            {
            /** for college **/
            $scope.collegeName = collegeObj.college.toString();
            $scope.branch = collegeObj.branch.toString();
            
            if(collegeObj.year != undefined)
            {
                $scope.year =  collegeObj.year.toString();
            }
                
            if(collegeObj.city != undefined)
            {
                $scope.collegeCity =  collegeObj.city.toString();   
            }
            
            if(collegeObj.preparingFor != undefined)
            {
                
//                var incomingValue = ucFirstAllWords(collegeObj.preparingFor[0].toString());
//                var foundInList = false;
//                
//                for(var counter= 0 ; counter < $scope.preparingForList.length ; counter++)
//                {
//                    if($scope.preparingForList[counter].value == incomingValue )
//                    {
//                        foundInList = true;
//                    }
//                }
//                
//                if(!foundInList)
//                {
//                   var obj = {} ;
//                   obj['value'] = incomingValue;
//                   obj['name'] = incomingValue;
//                   $scope.preparingForList[$scope.preparingForList.length] = obj;    
//                    
//                }
                
                
                if(ucFirstAllWords(collegeObj.preparingFor[0].toString()) == '')
                {
                   $scope.preparingFor = 'other';
                }
                else {
                   
                   $scope.preparingFor = ucFirstAllWords(collegeObj.preparingFor[0].toString());
                }   
                
                
            }    
                
            }
            
            
            
            /** for currently since only for 12th class **/
            $scope.class = '12th';
            //$scope.schoolOtherValue = '';
            
            if($scope.school.isComplete == 1 && response.data.data.templates['2']!=undefined)
            {
            /** for school **/
            $scope.schoolName = schoolObj.school.toString();
            
            if(schoolObj.class != undefined)
            {
                $scope.class =  schoolObj.class.toString();
            }
                
            if(schoolObj.city != undefined)
            {
                $scope.schoolCity =  schoolObj.city.toString();   
            }
                
            if(schoolObj.area != undefined)
            {
                $scope.schoolArea =  schoolObj.area.toString();   
            }   
            
            if(schoolObj.preparingFor != undefined)
            {
                
//                var incomingValue = ucFirstAllWords(schoolObj.preparingFor[0].toString());
//                var foundInList = false;
//                
//                for(var counter= 0 ; counter < $scope.schoolPreparingForList.length ; counter++)
//                {
//                    if($scope.schoolPreparingForList[counter].value == incomingValue )
//                    {
//                        foundInList = true;
//                    }
//                }
//                
//                if(!foundInList)
//                {
//                   var obj = {} ;
//                   obj['value'] = incomingValue;
//                   obj['name'] = incomingValue;
//                   $scope.schoolPreparingForList[$scope.schoolPreparingForList.length] = obj;    
//                    
//                }
                
                
                if(ucFirstAllWords(schoolObj.preparingFor[0].toString()) == '')
                {
                    $scope.schoolPreparingFor = 'other';
                }
                else {
                
                    $scope.schoolPreparingFor =  ucFirstAllWords(schoolObj.preparingFor[0].toString());
                }
                
            }    
                
            }
           
            
            if($scope.coaching.isComplete == 1 && response.data.data.templates['6']!=undefined)
            {
            /** for coaching **/
            $scope.coachingName = coachingObj.coaching.toString();
            
                
            if(coachingObj.city != undefined)
            {
                $scope.coachingCity =  coachingObj.city.toString();   
            }
                
            if(coachingObj.area != undefined)
            {
                $scope.coachingArea =  coachingObj.area.toString();   
            }   
            
                
            }
            
            /** test code for dropdown for project**/
            var projectNames = new Bloodhound({
              datumTokenizer: function (datum) {
                    return Bloodhound.tokenizers.whitespace(datum.value);
               },
              queryTokenizer: Bloodhound.tokenizers.whitespace,
              remote: {
                url: appConfig.API_HOST + appConstants.PROFILE_SUGGESTION + "/project/%QUERY" ,
                filter: function(parsedResponse ) {
                  return $.map(parsedResponse.data.suggestion , function(singleValue) {
                    return { value: singleValue }; });
                }
              }
            });
            projectNames.initialize();

            $('#projectList').tagsinput({
              typeaheadjs: {
                name: 'singleValue',  
                displayKey: 'value',
                valueKey: 'value',
                source: projectNames.ttAdapter()
              },
              freeInput: true,
              allowDuplicates: false,
              trimValue: true
            });    
                        
           
            /** test code for dropdown for skills**/
            var skillNames = new Bloodhound({
              datumTokenizer: function (datum) {
                    return Bloodhound.tokenizers.whitespace(datum.value);
               },
              queryTokenizer: Bloodhound.tokenizers.whitespace,
              remote: {
                url: appConfig.API_HOST + appConstants.PROFILE_SUGGESTION + "/skill/%QUERY" ,
                filter: function(parsedResponse ) {
                  return $.map(parsedResponse.data.suggestion , function(singleValue) {
                    return { value: singleValue }; });
                }
              }
            });
            skillNames.initialize();

            $('#technicalSkillList').tagsinput({
              typeaheadjs: {
                name: 'singleValue',  
                displayKey: 'value',
                valueKey: 'value',
                source: skillNames.ttAdapter()
                  },
              freeInput: true,
              allowDuplicates: false,
              trimValue: true
            });
            
            
            /** test code for dropdown for passionList**/
            var passionNames = new Bloodhound({
              datumTokenizer: function (datum) {
                    return Bloodhound.tokenizers.whitespace(datum.value);
               },
              queryTokenizer: Bloodhound.tokenizers.whitespace,
              remote: {
                url: appConfig.API_HOST + appConstants.PROFILE_SUGGESTION + "/passion/%QUERY" ,
                filter: function(parsedResponse ) {
                  return $.map(parsedResponse.data.suggestion , function(singleValue) {
                    return { value: singleValue }; });
                }
              }
            });
            passionNames.initialize();

            $('#passionList').tagsinput({
              typeaheadjs: {
                name: 'singleValue',  
                displayKey: 'value',
                valueKey: 'value',
                source: passionNames.ttAdapter()
              },
              freeInput: true,
              allowDuplicates: false,
              trimValue: true
            });
     
            /** test code for dropdown for passionList**/
            var examNames = new Bloodhound({
              datumTokenizer: function (datum) {
                    return Bloodhound.tokenizers.whitespace(datum.value);
               },
              queryTokenizer: Bloodhound.tokenizers.whitespace,
              remote: {
                url: appConfig.API_HOST + appConstants.GET_EXAM_SUGGESTIONS + "/%QUERY" ,
                filter: function(parsedResponse ) {
                  return $.map(parsedResponse.data.suggestion , function(singleValue) {
                    return { value: singleValue }; });
                }
              }
            });
            examNames.initialize();

            $('#examList').tagsinput({
              typeaheadjs: {
                name: 'singleValue',  
                displayKey: 'value',
                valueKey: 'value',
                source: examNames.ttAdapter()
              },
              freeInput: true,
              allowDuplicates: false,
              trimValue: true
            });
            
            if($scope.company.isComplete == 1 && response.data.data.templates['4']!=undefined)
            {
            /** for company **/
            $scope.companyName = companyObj.company.toString();
            $scope.position = companyObj.position.toString();

            $scope.technicalSkillList = companyObj.skill.toString();
            //$scope.projectList = companyObj.project.toString();
            
            /** for multiple values **/
            var inputProjectList = companyObj.project.toString();
            $scope.projectList = inputProjectList; 
            
           // to show the list of values //
           var projectList = $scope.projectList;
           var projectArray = projectList.split(",");
           for(var i = 0 ; i < projectArray.length ; i++)
           {
               $('#projectList').tagsinput('add', projectArray[i]);
           }
           /** the end **/




           // to show the list of values //
           var skillList = $scope.technicalSkillList ;
           var skillArray = skillList.split(",");
           for(var i = 0 ; i < skillArray.length ; i++)
           {
               $('#technicalSkillList').tagsinput('add', skillArray[i]);
           }
           /** the end **/     

            }

            if($scope.passion.isComplete == 1 && response.data.data.templates['5']!=undefined)
            {    
                /** for passion **/
                $scope.passionList = passionObj.passion.toString();

               // to show the list of values //
               var passionList = $scope.passionList ;
               var passionArray = passionList.split(",");
               for(var i = 0 ; i < passionArray.length ; i++)
               {
                   $('#passionList').tagsinput('add', passionArray[i]);
               }
               /** the end **/     
            } 
            
            if($scope.coaching.isComplete == 1 && response.data.data.templates['6']!=undefined)
            {    
                /** for exam List **/
                $scope.examList = coachingObj.preparingFor.toString();

                var examArray = coachingObj.preparingFor;
                for(var i = 0 ; i < examArray.length ; i++)
                {
                   $('#examList').tagsinput('add', examArray[i]);
                }
                /** the end **/     
            }     
            
                                    
        })
        .error(function(){
            alert("some error");
        });
    
        $(".twitter-typeahead").css('display', 'inline');
    
    
        $scope.year = '';
        $scope.preparingFor = '';
        $scope.collegeCity = '';
        $scope.schoolPreparingFor = '';
        $scope.schoolClass = 
    
        /** especially for admin **/
        $scope.years = [
            {name:'1st', value:'1st'},
            {name:'2nd', value:'2nd'},
            {name:'3rd', value:'3rd'},
            {name:'4th', value:'4th'},
            {name:'5th', value:'5th'},
            {name:'6th', value:'6th'}   
        ];
    
       $scope.preparingForList = [
            {name:'Masters', value:'Masters'},
            {name:'Public Services', value:'Public Services'},
            {name:'Military', value:'Military'},
            {name:'entrepreneurship', value:'entrepreneurship'},
            {name:'Double Graduation', value:'Double Graduation'},
            {name:'Short Term Courses', value:'Short Term Courses'},
            {name:'other', value:'other'}
        ];
    
        $scope.classes = [
            {name:'12th', value:'12th'},
        ];
    
        $scope.schoolPreparingForList = [
            {name:'Engineering', value:'Engineering'},
            {name:'Pure Science', value:'Pure Science'},
            {name:'Research', value:'Research'},
            {name:'Medical', value:'Medical'},
            {name:'Marine', value:'Marine'},
            {name:'Defense', value:'Defense'},
            {name:'Fashion & Design', value:'Fashion & Design'},
            {name:'Humanities & Social Science', value:'Humanities & Social Science'},
            {name:'Law ', value:'Law '},
            {name:'University Admission', value:'University Admission'},
            {name:'other', value:'other'}
        ];
     
    
}]);

_revlegApp.controller("accordianController",["$scope",function($scope) {
  $scope.oneAtATime = true;
  $scope.status = {
    isFirstOpen: true,
    isFirstDisabled: false
  };
}]);


_revlegApp.controller("referLoadingController",[ "$scope" , "referralFactory" , "$routeParams", "$location" , "$rootScope" , "locationFactory" , "locationService" , "getLocationList" , "$timeout" , "Facebook" , "formFactory" , "sessionService" , "ipCookie" , function($scope , referralFactory , $routeParams , $location , $rootScope , locationFactory , locationService , getLocationList , $timeout , Facebook , formFactory , sessionService ,ipCookie) {
 
    
    $scope.referLoading = {};
    
    $scope.referLoading.isLoadingScreen = true;
    $scope.referLoading.isSignupScreen = false;
    
     $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: 'white', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: 1, 
                color: '#fff' 
            } }); 
     
    $('.blockMsg').html("<h3 style='color:green;'>Verifying your Request <br><img  src='/img/Preloader_2/Preloader_2.gif'></h3>");
            
    if($routeParams.userId != undefined && $routeParams.refCode!=undefined )
    {
        var referUrl = appConfig.API_HOST + appConstants.REFERRAL_CHECK +
        "/"+$routeParams.userId+"/"+$routeParams.refCode;
        
        referralFactory.checkReferCode(referUrl)
        .success(function(response) {
            
            if(response.status == "success")
            {    
                 //$location.path("/signUp");
                 $scope.referLoading.isLoadingScreen = false;
                 $scope.referLoading.isSignupScreen = true;
                 $rootScope.ref_userId = $routeParams.userId;
                 $rootScope.ref_refCode = $routeParams.refCode;
                 $timeout(function(){
                    $.unblockUI(); 
                 },1800);
                                
            }
            
            
            else {
            
                 $('.blockMsg').html("<h2 style='color:red;'>Incorrect URL, please check referral link</h2>");        
            }
            
            
        })
        .error(function(){
                
             $('.blockMsg').html("<h2 style='color:red;'>Some error ocurred</h2>");   
             $timeout(function(){
                $.unblockUI(); 
              },1800);
        });
    }
    else
    {
     
       $location.path("/search"); 
       
    }    
              
     $scope.signUpFB = function() {
      
     $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: 'white', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: 1, 
                color: '#fff' 
            } }); 
     
      $('.blockMsg').html("<h1 style='color:green;'>Please wait...</h1>");
     
      // From now on you can use the Facebook service just as Facebook api says
      Facebook.login(function(response) {
      // Do something with response.
        if(response.status === 'connected') {
  
            console.log(response);
            Facebook.api('/me', function(response) {
                
                $scope.user = response;
                console.log(response);
                
                if(response.first_name != undefined)
                {
                var first_name = response.first_name;
                }
                
                if(response.last_name != undefined)
                {
                var last_name = response.last_name;
                }
                
                if(response.name != undefined)
                {    
                var name = response.name;
                } 
                if(response.email != undefined)
                {
                    var emailId = response.email;
                    var userId = response.id;

                    var formData = new FormData();

                    formData.append('emailId',emailId);
                    formData.append('fullName',name);
                    formData.append('userType','3');
                    formData.append('referCode',$rootScope.ref_refCode);
                    formData.append('referedBy',$rootScope.ref_userId);
                    
                    var loginSubmitUrl = appConfig.API_HOST + appConstants.FB_LOGIN;
                    formFactory.submit(formData, loginSubmitUrl)
                    .success(function(response) {
                        $scope.loader = "true";

                       if(response.status == 'success')
                       {
                            /** to be changed from username to displayname**/
                            sessionService.create(response.data.userData.basicInfo);
                            $rootScope.$broadcast('event-signin-success');

                            if(ipCookie('isProfileComplete') == false)
                            {
                                $location.path("/profile");
                            }

                            $.unblockUI(); 

                       }
                       else  {

                            $('.blockMsg').html("<h1 style='color:red;'>"+response.data.message+"</h1>");
                            $timeout(function(){
                                $.unblockUI();
                            },2000); 
                       }
                    }).error(function(){

                            
                            $('.blockMsg').html("<h1 style='color:red;'>Some error from backend</h1>");
                            $timeout(function(){
                                $.unblockUI();
                            },2000); 
                        

                    });
                    
                }
                else {
                
                    $('.blockMsg').html("<h3 style='color:red;'>Unable to get your emailId through facebook</h3><br><span style='color:black;'>Check your facebook account settings or signup using RevLeg signup form.</span>");
                    $timeout(function(){
                        $.unblockUI();
                    },5000); 
                }
                
            });
            
        } else {
            
            $('.blockMsg').html("<h1 style='color:red;'>Facebook Login Cancelled</h1>");
            $timeout(function(){
                $.unblockUI();
            },2000); 
        }
      },{scope:'email'});
      
  };    
    
    
}]);


_revlegApp.controller("emailverificationController",[ "$scope" , "emailverificationFactory" , "$routeParams", "$location" , "$rootScope" , function($scope , referralFactory , $routeParams , $location , $rootScope) {
 
    
    $scope.emailVerify = {};
    $scope.emailVerify.isLoader = true;
    $scope.emailVerify.isMessage = false;
    
        
    if($routeParams.userId != undefined && $routeParams.authCode!=undefined)
    {
        var referUrl = appConfig.API_HOST + appConstants.EMAIL_VERIFY + 
        "/"+$routeParams.userId+"/"+$routeParams.authCode;
        
        referralFactory.verifyEmailId(referUrl)
        .success(function(response) {
            
            if(response.status == "success")
            {
               //alert("fine"); 
               $rootScope.$emit('event-signIn');        
               $location.path("/search");
               
            }
            
            
            else {
            
                 $scope.emailVerify.isLoader = false;
                 $scope.emailVerify.isMessage = true;
                 $scope.emailVerify.message = response.data.message;
                
                
            }
            
            
        })
        .error(function(){
            alert("some error");
        });
    }
    else
    {
     
            $location.path("/search"); 
       
    }    
        
    
    
}]);


/** file uplad controller **/

_revlegApp.controller('fileUploadController',['$scope', '$upload', 'authService' , function($scope, $upload , authService) {

  $scope.fileUpload= {};
  $scope.fileUpload.profilePic = authService.getProfilePic();

  $scope.$watch(
        function(){ return  authService.getProfilePic() },

        function(newVal) {
            $scope.fileUpload.profilePic = newVal;
        }
    );


  $scope.onFileSelect = function($files) {
    //$files: an array of files selected, each file has name, size, and type.
    for (var i = 0; i < $files.length; i++) {
      var file = $files[i];
      $scope.upload = $upload.upload({
        url: appConfig.API_HOST + appConstants.PROFILE_IMAGE_UPLOAD,
        data: {myObj: $scope.myModelObj},
        file: file,
      }).progress(function(evt) {
        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
      }).success(function(data, status, headers, config) {
        // file is uploaded successfully
        authService.changeProfilePic(data.data.imageUrl);
      });
    }
  };
}]);


_revlegApp.controller('forgetPasswordController',['$scope','forgetPasswordFactory' , function($scope,forgetPasswordFactory) {

          $scope.forgetPassword = {};
          $scope.forgetPassword.form = true;
          $scope.forgetPassword.messageDisplay = false;
          $scope.forgetPassword.loader = false;
    
//        $scope.forgetPassword.send = function()
//        {
//               
//            var url = appConfig.API_HOST + appConstants.FORGET_PASSWORD;
//            var formData = new FormData();
//            formData.append('emailId',$scope.forgetPassword.emailId);                                      
//            forgetPasswordFactory(url,formData)
//                .success(function(response){
//                                                  
//                })
//                .error();                                  
//                                                  
//        }                                        
    
}]);



_revlegApp.controller("sentMessageController",[ "$scope" , "messageFactory" , "commentFactory" , "$routeParams", "authService" , function($scope , messageFactory , commentFactory , $routeParams , authService) {
    
    
    $scope.user = {};
    
    $scope.user.profilePic = authService.getProfilePic();
    
    $scope.$watch(
        function(){ return  authService.getProfilePic() },

        function(newVal) {
            $scope.user.profilePic = newVal;
        }
    );
    
     $scope.isAlertForInbox = false;
     var messageRecvUrl = appConfig.API_HOST + appConstants.MESSAGE_SENT;
        
     messageFactory.recv(messageRecvUrl)
        .success(function(response) {
            var offset = 0;
            $scope.messageList = response.data.messages;
            
            if(response.data.status == "fail")
            {
                $scope.isAlertForInbox = true;
                $scope.inboxAlertMessage = "Some issue in backend we couldnt recv your messages";
            }
            
            else if($scope.messageList.length == 0)
            {
                $scope.isAlertForInbox = true;
                $scope.inboxAlertMessage = "You have no messages.";
            }    
            
            else {
            
               $scope.isAlertForInbox = false;
            }
            
            
        })
        .error(function(){
            alert("some error");
        });    
       
    
        
    
    
}]);


_revlegApp.controller("getAllController",[ "$scope" , "getAllProfileFactory" , "$routeParams", "authService" , function($scope , getAllProfileFactory , $routeParams , authService) {
    
    
     var profileListRecvUrl = appConfig.API_HOST + appConstants.GET_ALL + "/" + $routeParams.authCode;
        
     getAllProfileFactory.get(profileListRecvUrl)
        .success(function(response) {
            var offset = 0;
//            $scope.profileList = response.data.data;
            
            
            var profileList  = [];
            var unformattedProfileList = response.data.data;
            
            console.log('the get all controller');
            
            for( var i = 1; i < unformattedProfileList.length ; i++)
            {
                console.log('the index '+ i);
                var singleProfile = {};
                singleProfile.userId = unformattedProfileList[i].userId;
                singleProfile.emailId = unformattedProfileList[i].emailId;
                singleProfile.profileEditLink = appConfig.APP_HOST + "#!/adminprofileedit/2June1989!/" + singleProfile.userId;
                
                var templateArray = unformattedProfileList[i].returnMe.templates;
                
                
                if(unformattedProfileList[i].returnMe.templates != undefined)
                {
                    // the get all controller //
                    if(templateArray[1] != undefined)
                    {
                    var locationObj = templateArray[1];
                    var locationString = locationObj['country'][0] + " , " + locationObj['state'][0] + " , " + locationObj['city'][0];
                    singleProfile.location = locationString;
                    }
                    else {
                    singleProfile.location = "";
                    }

                    if(templateArray[3] != undefined){
                    var educationObj = templateArray[3];

                    if(educationObj['university'] != undefined) 
                    {
                        var eudcationString =  educationObj['university'][0] + " , " + educationObj['college'][0] + " , " + educationObj['branch'][0];
                    }
                    else {

                        var eudcationString =  educationObj['college'][0] + " , " + educationObj['branch'][0];
                    } 

                    singleProfile.education = eudcationString;
                    }
                    else {
                    singleProfile.education = "";
                    }

                    if(templateArray[4] != undefined){
                    var companyObj = templateArray[4];
                    var companyString = companyObj['company'].join() + " ; " + companyObj['position'].join() + " ; " + companyObj['project'].join() + " ; " + companyObj['skill'].join() ;
                    singleProfile.company = companyString;
                    }
                    else {
                    singleProfile.company = "";

                    }

                    if(templateArray[5] != undefined){
                    var pasisonObj = templateArray[5];
                    var passionString = pasisonObj['passion'].join();
                    singleProfile.passion = passionString;
                    }
                    else {

                    singleProfile.passion = "";    
                    }
                
                
                
                
                profileList[offset] = singleProfile;
                offset++;
                    
                }
                else {
                
                }
                
                
            }
            
            $scope.profileList = profileList;
            
            
        })
        .error(function(){
            alert("some error");
        });    
}]);


_revlegApp.controller('carouselController', ["$scope" , function ($scope) {
  $scope.myInterval = 7000;
  var slides = $scope.slides = [];
  $scope.addSlide = function() {
    var newWidth = 600 + slides.length + 1;
    slides.push({
      image: 'http://placekitten.com/' + newWidth + '/300',
      text: ['More','Extra','Lots of','Surplus'][slides.length % 4] + ' ' +
        ['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
    });
  };
  for (var i=0; i<4; i++) {
    $scope.addSlide();
  }
}]);

_revlegApp.controller("inboxMCQController",[ "$scope" , "messageFactory" , "commentFactory" , "$routeParams", "authService" , "SweetAlert" , "anchorSmoothScroll" , "$rootScope" , "$timeout", "$document" , "formFactory" , function($scope , messageFactory , commentFactory , $routeParams , authService , SweetAlert , anchorSmoothScroll , $rootScope , $timeout, $document, formFactory) {
 
    
    $scope.user = {};
    $scope.optionValue = {};
    $scope.inputSuggestion = [];
    $scope.isSuggestionSubmitErrorMessage = true;
    $scope.isAlertForInbox = false;
    
    $scope.$watch(
        function(){ return  authService.getProfilePic() },

        function(newVal) {
            $scope.user.profilePic = newVal;
        }
    );
    
    
    $scope.loadingScreenMessge = true;
    
    var getSingleMessage = function()
    {
        /** need to work for a single msg incomplete **/
        var messageRecvUrl = appConfig.API_HOST + appConstants.SINGLE_MSG + $routeParams.messageId;
        messageFactory.recv(messageRecvUrl)
        .success(function(response) {
            
            var offset = 0;
            $scope.messageList = response.data.message;
            $rootScope.lastMessageId = $scope.messageList[$scope.messageList.length -1].messageId;
            
            if(response.data.status == "fail")
            {
                $scope.isAlertForInbox = true;
                $scope.inboxAlertMessage = "Some issue in backend we couldnt receive your messages";
            }
            
            else if($scope.messageList.length == 0)
            {
                $scope.isAlertForInbox = true;
                $scope.inboxAlertMessage = "No Message Available";
            }    
            
            else {
            
            $scope.loadingScreenMessge = false;    
            $scope.isAlertForInbox = false;
             
            // for single mssage start //
                
            /** for mcq msg **/
            for (var i = 0 ; i < $scope.messageList.length ; i++ )
            {
                var currentMessageId = $scope.messageList[i].messageId;
                $scope.messageList[i].anyAction = "yes";
                $scope.messageList[i].messageStatus = "open";
                $scope.messageList[i].buttonDisabled = false;
                
                var routeCode = $scope.messageList[i].url ;
                if(routeCode == 1)
                {
                    // everything is fine //
                }
                else {
                
                $scope.isAlertForInbox = false;
                $scope.inboxAlertMessage = "No Message Available";
                
                }
                
                // to handle the loggin and loggout to show commenting or not 
                $scope.messageList[0].isCommentingAllowed = !(authService.checkLogin());
                $scope.$watch(
                function(){ return  authService.checkLogin() },
                function(newVal) {
                  $scope.messageList[0].isCommentingAllowed = newVal;
                }
                );  
 
                var dateAndTime = ($scope.messageList[i].createdOn).split(" ");
                var dateArray = dateAndTime[0].split("-");
                var dateString = dateArray[0]+dateArray[1]+dateArray[2];
                //$scope.messageList[i].date = dateAndTime[0];
                $scope.messageList[i].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
                $scope.messageList[i].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3);     
                
                if( $scope.messageList[i].messageType == "0")
                {
                    $scope.messageList[i].anyAction = "no"; 
                    
                }
                else if($scope.messageList[i].isClosed == "yes")
                {
                    $scope.messageList[i].anyAction = "no";
                    $scope.messageList[i].messageStatus = "closed";
                    
                    for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
                    {
                        var currentComment = $scope.messageList[i].comment[c];
                        $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);
                        
                        if(!authService.checkLogin())
                        {
                            //when not loggedin
                            $scope.messageList[i].anyAction = "yes";
                            $scope.messageList[i].comment[c].isLikeByUser = "no";
                        }
                        else 
                        {
                            // when logged in 
                            if(currentComment.isLikeByUser == "yes")
                            {
                                $scope.optionValue[currentMessageId] = currentComment.commentId;
                            }
                        }
                        
                    }
                    
                }
                else 
                {
                    for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
                    {
                        var currentComment = $scope.messageList[i].comment[c];
                        $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);
                        
                        if(!authService.checkLogin())
                        {
                            //when not loggedin //
                            $scope.messageList[i].anyAction = "yes";
                            $scope.messageList[i].comment[c].isLikeByUser = "no";
                        }
                        else 
                        {
                            if(currentComment.isLikeByUser == "yes")
                            {
                                $scope.optionValue[currentMessageId] = currentComment.commentId;
                                $scope.messageList[i].anyAction = "no";
                                $scope.messageList[i].messageStatus = "submitted";
                            }
                        }
                    }
                }
                if($scope.messageList[i].suggestion == undefined)
                {
                      
                    $scope.messageList[i].suggestion = []; 
                    $scope.messageList[i].suggestionLength = 0;
                } 
                else {
                 
                    var suggestionList = $scope.messageList[i].suggestion;
                    for(var c = 0 ; c < suggestionList.length ; c++ )
                    {
                        var dateAndTime = ($scope.messageList[i].suggestion[c].createdOn).split(" ");
                        var dateArray = dateAndTime[0].split("-");
                        var dateString = dateArray[0]+dateArray[1]+dateArray[2];
                        $scope.messageList[i].suggestion[c].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
                        $scope.messageList[i].suggestion[c].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3); 
                    }
                    $scope.messageList[i].suggestionLength = $scope.messageList[i].suggestion.length;
                }    
                    
            }
            
            if(response.data.status == "fail")
            {
                $scope.isAlertForInbox = true;
                $scope.inboxAlertMessage = "Some issue in backend we couldnt recv your messages";
            }
            
            if($scope.messageList.length == 0)
            {
                $scope.isAlertForInbox = true;
                $scope.inboxAlertMessage = "You have no messages.";
            }    
                  
                
                
                
            }
            
            
        })
        .error(function(){
            alert("some error");
        });
    }
    
    if($routeParams.messageId != undefined)
    {
        
        getSingleMessage();
        
//        $scope.$watch(
//        function(){ return  authService.checkLogin() },
//        function(newVal) {
//          getSingleMessage();
//        }
//        );  
        
        
    }
    else
    {
     
     $scope.isAlertForInbox = false;
     var messageRecvUrl = appConfig.API_HOST + appConstants.MESSAGE_RECV;
        
     messageFactory.recv(messageRecvUrl)
        .success(function(response) {
            var offset = 0;
             
            $scope.loadingScreenMessge = false; 
            $scope.messageList = response.data.messages;
            $rootScope.lastMessageId = $scope.messageList[$scope.messageList.length -1].messageId;
           
            for (var i = 0 ; i < $scope.messageList.length ; i++ )
            {
                var currentMessageId = $scope.messageList[i].messageId;
                $scope.messageList[i].anyAction = "yes";
                $scope.messageList[i].messageStatus = "open";
                $scope.messageList[i].isCommentingAllowed = true;
                $scope.messageList[i].buttonDisabled = false;
                
                var dateAndTime = ($scope.messageList[i].createdOn).split(" ");
                var dateArray = dateAndTime[0].split("-");
                var dateString = dateArray[0]+dateArray[1]+dateArray[2];
                //$scope.messageList[i].date = dateAndTime[0];
                $scope.messageList[i].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
                $scope.messageList[i].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3);                 
                
                if( $scope.messageList[i].messageType == "0")
                {
                    $scope.messageList[i].anyAction = "no"; 
                    
                }
                else if($scope.messageList[i].isClosed == "yes")
                {
                    $scope.messageList[i].anyAction = "no";
                    $scope.messageList[i].messageStatus = "closed";
                    
                    for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
                    {
                        var currentComment = $scope.messageList[i].comment[c];
                        $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);
                        
                        if(currentComment.isLikeByUser == "yes")
                        {
                            $scope.optionValue[currentMessageId] = currentComment.commentId;
                        }
                    }
                    
                }
                else 
                {
                    for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
                    {
                        var currentComment = $scope.messageList[i].comment[c];
                        $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);
                        
                        if(currentComment.isLikeByUser == "yes")
                        {
                            $scope.optionValue[currentMessageId] = currentComment.commentId;
                            $scope.messageList[i].anyAction = "no";
                            $scope.messageList[i].messageStatus = "submitted";
                        }
                    }
                }
                                
                 if($scope.messageList[i].suggestion == undefined)
                 {
                      
                    $scope.messageList[i].suggestion = []; 
                    $scope.messageList[i].suggestionLength = 0;
                 } 
                 else {
                 
                    var suggestionList = $scope.messageList[i].suggestion;
                    for(var c = 0 ; c < suggestionList.length ; c++ )
                    {
                        var dateAndTime = ($scope.messageList[i].suggestion[c].createdOn).split(" ");
                        var dateArray = dateAndTime[0].split("-");
                        var dateString = dateArray[0]+dateArray[1]+dateArray[2];
                        $scope.messageList[i].suggestion[c].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
                        $scope.messageList[i].suggestion[c].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3); 
                    }  
                    $scope.messageList[i].suggestionLength = $scope.messageList[i].suggestion.length;
                 }
                    
                
            }
            
            if(response.data.status == "fail")
            {
                $scope.isAlertForInbox = true;
                $scope.inboxAlertMessage = "Some issue in backend we couldnt recv your messages";
            }
            
            else if($scope.messageList.length == 0)
            {
                $scope.isAlertForInbox = true;
                $scope.inboxAlertMessage = "You have no messages.";
            }    
            
            else {
            
               $scope.isAlertForInbox = false;
            }
            
            
        })
        .error(function(){
            alert("some error");
        });    
       
    }
        
    
    $scope.upVote = function(messageId)
    {
        
            // ** Submitting Options ** //
            var upVote_url = appConfig.API_HOST + appConstants.COMMENT_CREATE + messageId + "/likes";
            var formData = new FormData();            
            // increasing the number of comments here //
            for( var l = 0; l < $scope.messageList.length ; l++)
            {
                if($scope.messageList[l].messageId == messageId)
                {
                    var messageTotalAudience = $scope.messageList[l].totalReach;
                    $scope.messageList[l].totalLike+=1;
                    $scope.messageList[l].isLikeByUser = 'yes';
                }
            }
            
            formFactory.submit(formData, upVote_url)
            .success(function(response) {

                if(response.status == "success")
                {
                    // do nothing //

                }
                else{

                    // on failure returning back to orginal //
                    SweetAlert.swal("Oops...", "Something went wrong!", "error");
                    
                    for( var l = 0; l < $scope.messageList.length ; l++)
                    {
                        if($scope.messageList[l].messageId == messageId)
                        {
                            var messageTotalAudience = $scope.messageList[l].totalReach;
                            $scope.messageList[l].totalLike-=1;
                            $scope.messageList[l].isLikeByUser = 'no';
                        }
                    }
                    
                }

            }).error(function(){

                    // on failure returning back to orginal //
                    SweetAlert.swal("Oops...", "Something went wrong!", "error");
                    
                    for( var l = 0; l < $scope.messageList.length ; l++)
                    {
                        if($scope.messageList[l].messageId == messageId)
                        {
                            var messageTotalAudience = $scope.messageList[l].totalReach;
                            $scope.messageList[l].totalLike-=1;
                            $scope.messageList[l].isLikeByUser = 'no';
                        }
                    }
            });
        
    };
    
    $scope.downVote = function(messageId)
    {
            // ** Submitting Options ** //
            var downVote_url = appConfig.API_HOST + appConstants.COMMENT_CREATE + messageId + "/dislikes";
            var formData = new FormData();            
            // increasing the number of comments here //
            for( var l = 0; l < $scope.messageList.length ; l++)
            {
                if($scope.messageList[l].messageId == messageId)
                {
                    var messageTotalAudience = $scope.messageList[l].totalReach;
                    $scope.messageList[l].totalDisLike+=1;
                    $scope.messageList[l].isDisLikeByUser = 'yes';
                }
            }
            formFactory.submit(formData, downVote_url)
            .success(function(response) {

                if(response.status == "success")
                {
                    // do nothing //

                }
                else{

                    // on failure returning back to orginal //
                    SweetAlert.swal("Oops...", "Something went wrong!", "error");
                    
                    for( var l = 0; l < $scope.messageList.length ; l++)
                    {
                        if($scope.messageList[l].messageId == messageId)
                        {
                            var messageTotalAudience = $scope.messageList[l].totalReach;
                            $scope.messageList[l].totalDisLike-=1;
                            $scope.messageList[l].isDisLikeByUser = 'no';
                        }
                    }
                    
                }

            }).error(function(){

                    // on failure returning back to orginal //
                    SweetAlert.swal("Oops...", "Something went wrong!", "error");
                    
                    for( var l = 0; l < $scope.messageList.length ; l++)
                    {
                        if($scope.messageList[l].messageId == messageId)
                        {
                            var messageTotalAudience = $scope.messageList[l].totalReach;
                            $scope.messageList[l].totalDisLike-=1;
                            $scope.messageList[l].isDisLikeByUser = 'no';
                        }
                    }
            });
    };
    
    $scope.commentReturnValue = function(messageId)
    {
        if(messageId == $rootScope.scrollIntoViewMessageId)
        {
            return false;
        }
        else {
            return true;
        }
    };
//    
//    $scope.$on('onRepeatLast',function(){
//        $scope.showComment[$rootScope.scrollIntoViewMessageId] = false;
//        console.log("on last repeat");
//    });
    
    $scope.submitOpinion = function(messageId,index)
    {
        if(!authService.checkLogin())
        {
            SweetAlert.swal("Oops...", "You need to login first", "error");
        }
        else 
        {
        
        var choosenCommentId = $scope.optionValue[messageId];
        if(choosenCommentId == undefined)
        {
            SweetAlert.swal("Oops...", "You need to choose one option", "error");
        }
        else {
            
            $scope.messageList[index].buttonDisabled = true;
            
            // ** Submitting Options ** //
            var comment_url = appConfig.API_HOST + appConstants.COMMENT_CREATE + messageId + "/" + "comments/" + choosenCommentId + "/likes" ;
            var formData = new FormData();
            formData.append('commentId',choosenCommentId);
            /** for recv this key has to be no and for sender it has to be yes**/
            formData.append('closeMessage','no');
            
            
            // increasing the number of comments here //
            for( var l = 0; l < $scope.messageList.length ; l++)
            {
                if($scope.messageList[l].messageId == messageId)
                {
                    var messageTotalAudience = $scope.messageList[l].totalReach;

                    for(var c = 0 ; c < $scope.messageList[l].comment.length ; c++)
                    {
                        if($scope.messageList[l].comment[c].commentId  == choosenCommentId)
                        {    

                        var commentTotalAudeinceNew = $scope.messageList[l].comment[c].totalLike + 1; 
                        var percentVote = parseFloat ( (commentTotalAudeinceNew / messageTotalAudience ) * 100 );
                        percentVote.toFixed(2);    
                            
                        var orginalPrecentLike = $scope.messageList[l].comment[c].percentLike;
                        var originalTotalLike = $scope.messageList[l].comment[c].totalLike;  
                        $scope.messageList[l].comment[c].percentLike = percentVote.toFixed(2);  
                        $scope.messageList[l].comment[c].totalLike  += 1; 
                        $scope.messageList[l].anyAction = "no";
                        $scope.messageList[l].messageStatus = "submitted"; 
                        $scope.messageList[l].comment[c].isLikeByUser = 'yes';    

                        }
                    }

                }
            }
            
            commentFactory.send(formData, comment_url)
            .success(function(response) {

                if(response.status == "success")
                {


                }
                else{

                    // on failure returning back to orginal //
                    SweetAlert.swal("Oops...", "Something went wrong!", "error");
                    
                    $scope.messageList[index].buttonDisabled = false;
                    
                    for( var l = 0; l < $scope.messageList.length ; l++)
                    {
                        if($scope.messageList[l].messageId == messageId)
                        {
                            var messageTotalAudience = $scope.messageList[l].totalReach;                            
                            
                            for(var c = 0 ; c < $scope.messageList[l].comment.length ; c++)
                            {
                                if($scope.messageList[l].comment[c].commentId  == choosenCommentId)
                                {    
                                                                    
                                $scope.messageList[l].comment[c].percentLike =  orginalPrecentLike;
                                $scope.messageList[l].comment[c].totalLike  = originalTotalLike; 
                                $scope.messageList[l].anyAction = "yes";
                                $scope.messageList[l].messageStatus = "open"; 
                                $scope.messageList[l].comment[c].isLikeByUser = 'no';        
                                }
                            }
                            
                           $scope.optionValue[messageId] = "";    
                            
                        }
                    }
                    
                }

            }).error(function(){

                    // on failure returning back to orginal //
                    SweetAlert.swal("Oops...", "Something went wrong!", "error");
                    
                    $scope.messageList[index].buttonDisabled = false;
                
                    for( var l = 0; l < $scope.messageList.length ; l++)
                    {
                        if($scope.messageList[l].messageId == messageId)
                        {
                            var messageTotalAudience = $scope.messageList[l].totalReach;

                            for(var c = 0 ; c < $scope.messageList[l].comment.length ; c++)
                            {
                                if($scope.messageList[l].comment[c].commentId  == choosenCommentId)
                                {  
                                    
                                $scope.messageList[l].comment[c].percentLike =  orginalPrecentLike;
                                $scope.messageList[l].comment[c].totalLike = originalTotalLike;     
                                $scope.messageList[l].anyAction = "yes";
                                $scope.messageList[l].messageStatus = "open"; 
                                $scope.messageList[l].comment[c].isLikeByUser = 'no';    
                                    
                                }
                            }
                            
                           $scope.optionValue[messageId] = "";
                            
                        }
                    }

            });
                    
        }
        }
    }
   
        
    
    
}]);


_revlegApp.controller("outboxMCQController",[ "$scope" , "messageFactory" , "commentFactory" , "$routeParams", "authService" , "SweetAlert" , "$rootScope" , function($scope , messageFactory , commentFactory , $routeParams , authService , SweetAlert , $rootScope) {
 
    
    $scope.user = {};
    $scope.optionValue = {};
    $scope.inputSuggestion = [];
    $scope.isSuggestionSubmitErrorMessage = true;
    $scope.isAlertForOutBox = false;
    
    $scope.$watch(
        function(){ return  authService.getProfilePic() },

        function(newVal) {
            $scope.user.profilePic = newVal;
        }
    );
     
        if($routeParams.messageId != undefined)
        {
        
        /** need to work for a single msg incomplete **/
        
        var messageRecvUrl = appConfig.API_HOST + appConstants.SINGLE_MSG + $routeParams.messageId;
        
        messageFactory.recv(messageRecvUrl)
        .success(function(response) {
            
            var offset = 0;
            $scope.messageList = response.data.message;
            $rootScope.lastMessageId = $scope.messageList[$scope.messageList.length -1].messageId;
            
            if(response.data.status == "fail")
            {
                $scope.isAlertForOutBox = true;
                $scope.outBoxAlertMessage = "Some issue in backend we couldnt receive your messages";
            }
            
            else if($scope.messageList.length == 0)
            {
                $scope.isAlertForOutBox = true;
                $scope.outBoxAlertMessage = "No Message Available";
            }    
            
            else {
            
            $scope.isAlertForInbox = false;
            for (var i = 0 ; i < $scope.messageList.length ; i++ )
            {
                $scope.messageList[i].buttonDisabled = false;
                
                var currentMessageId = $scope.messageList[i].messageId;
                $scope.messageList[i].anyAction = "yes";
                var poolURL =  appConfig.APP_HOST+"#!/pool/"+currentMessageId;
                
                var fbContentShare = "https://www.facebook.com/sharer/sharer.php?u="+escape(poolURL);
                $scope.messageList[i].FBPoolURL = fbContentShare; 
                
                var twitterContentShare = "https://twitter.com/home?status="+escape(poolURL);  
                $scope.messageList[i].twitterPoolURL = twitterContentShare;
                
                var googlePlusContentShare = "https://plus.google.com/share?url="+escape(poolURL); 
                $scope.messageList[i].googlePlusPollURL = googlePlusContentShare;
                
                var linkedinContentShare = "https://www.linkedin.com/shareArticle?mini=true&url="+poolURL+"&title="+$scope.messageList[i].messageSubject+"&summary="+$scope.messageList[i].messageBody+"&source=http://revleg.com";              
                $scope.messageList[i].linkedinPollURL = linkedinContentShare;
                
                var routeCode = $scope.messageList[i].url ;
                if(routeCode == 0)
                {
                    // everything is fine //
                }
                else {
                
                $scope.isAlertForOutBox = true;
                $scope.outBoxAlertMessage = "You have no message of this ID";
                
                }
                
                var dateAndTime = ($scope.messageList[i].createdOn).split(" ");
                var dateArray = dateAndTime[0].split("-");
                var dateString = dateArray[0]+dateArray[1]+dateArray[2];
                //$scope.messageList[i].date = dateAndTime[0];
                $scope.messageList[i].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
                $scope.messageList[i].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3); 
                
                if( $scope.messageList[i].messageType == "0")
                {
                    $scope.messageList[i].anyAction = "no"; 
                    $scope.messageList[i].isDisplaySubmit = "no";
                }
                else  
                {    
                    $scope.messageList[i].anyAction = "no";
                    $scope.messageList[i].participated = 0;
                    $scope.messageList[i].left = 0;
                    
                    
                    if($scope.messageList[i].isClosed == "yes")
                    {
                         $scope.messageList[i].isDisplaySubmit = "no";
                    }
                    else {
                        
                         $scope.messageList[i].isDisplaySubmit = "yes";
                    }
                    
                    for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
                    {
                        var currentComment = $scope.messageList[i].comment[c];
                        $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);
                        $scope.messageList[i].participated += $scope.messageList[i].comment[c].totalLike;
                        if(currentComment.isLikeByOwner == "yes")
                        {
                            $scope.optionValue[currentMessageId] = currentComment.commentId;
                        }
                    }
                    
                    $scope.messageList[i].left = $scope.messageList[i].totalReach  - $scope.messageList[i].participated
                    
 
                }
                if($scope.messageList[i].suggestion == undefined)
                {
                      
                    $scope.messageList[i].suggestion = []; 
                    $scope.messageList[i].suggestionLength = 0;
                } 
                else {
                 
                    var suggestionList = $scope.messageList[i].suggestion;
                    for(var c = 0 ; c < suggestionList.length ; c++ )
                    {
                        var dateAndTime = ($scope.messageList[i].suggestion[c].createdOn).split(" ");
                        var dateArray = dateAndTime[0].split("-");
                        var dateString = dateArray[0]+dateArray[1]+dateArray[2];
                        $scope.messageList[i].suggestion[c].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
                        $scope.messageList[i].suggestion[c].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3); 
                    } 
                    
                    $scope.messageList[i].suggestionLength = $scope.messageList[i].suggestion.length;
                }  
                
            } 
                
            }
            
        })
        .error(function(){
            alert("some error");
        });
    }
    else
    {
    
    
    /** start **/
    
     $scope.isAlertForInbox = false;
     var messageRecvUrl = appConfig.API_HOST + appConstants.MESSAGE_SENT;
        
     messageFactory.recv(messageRecvUrl)
        .success(function(response) {
            var offset = 0;
            $scope.messageList = response.data.messages;
            $rootScope.lastMessageId = $scope.messageList[$scope.messageList.length -1].messageId;
            
            for (var i = 0 ; i < $scope.messageList.length ; i++ )
            {
                
                $scope.messageList[i].buttonDisabled = false;
                
                var currentMessageId = $scope.messageList[i].messageId;
                var poolURL =  appConfig.APP_HOST+"#!/pool/"+currentMessageId;
                
                var fbContentShare = "https://www.facebook.com/sharer/sharer.php?u="+escape(poolURL);
                $scope.messageList[i].FBPoolURL = fbContentShare; 
                
                var twitterContentShare = "https://twitter.com/home?status="+escape(poolURL);  
                $scope.messageList[i].twitterPoolURL = twitterContentShare;
                
                var googlePlusContentShare = "https://plus.google.com/share?url="+escape(poolURL); 
                $scope.messageList[i].googlePlusPollURL = googlePlusContentShare;
                
                var linkedinContentShare = "https://www.linkedin.com/shareArticle?mini=true&url="+poolURL+"&title="+$scope.messageList[i].messageSubject+"&summary="+$scope.messageList[i].messageBody+"&source=http://revleg.com";              
                $scope.messageList[i].linkedinPollURL = linkedinContentShare;
                
                 
                $scope.messageList[i].anyAction = "yes";
                var dateAndTime = ($scope.messageList[i].createdOn).split(" ");
                var dateArray = dateAndTime[0].split("-");
                var dateString = dateArray[0]+dateArray[1]+dateArray[2];
                //$scope.messageList[i].date = dateAndTime[0];
                $scope.messageList[i].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
                $scope.messageList[i].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3); 
                
                if( $scope.messageList[i].messageType == "0")
                {
                    $scope.messageList[i].anyAction = "no"; 
                    $scope.messageList[i].isDisplaySubmit = "no";
                }
                else  
                {    
                    $scope.messageList[i].anyAction = "no";
                    $scope.messageList[i].participated = 0;
                    $scope.messageList[i].left = 0;
                    
                    
                    if($scope.messageList[i].isClosed == "yes")
                    {
                         $scope.messageList[i].isDisplaySubmit = "no";
                    }
                    else {
                        
                         $scope.messageList[i].isDisplaySubmit = "yes";
                    }
                    
                    for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
                    {
                        var currentComment = $scope.messageList[i].comment[c];
                        $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);
                        $scope.messageList[i].participated += $scope.messageList[i].comment[c].totalLike;
                        if(currentComment.isLikeByOwner == "yes")
                        {
                            $scope.optionValue[currentMessageId] = currentComment.commentId;
                        }
                    }
                    
                    $scope.messageList[i].left = $scope.messageList[i].totalReach  - $scope.messageList[i].participated
                    
 
                }
                if($scope.messageList[i].suggestion == undefined)
                 {
                      
                    $scope.messageList[i].suggestion = []; 
                    $scope.messageList[i].suggestionLength = 0;
                 } 
                 else {
                 
                    var suggestionList = $scope.messageList[i].suggestion;
                    for(var c = 0 ; c < suggestionList.length ; c++ )
                    {
                        var dateAndTime = ($scope.messageList[i].suggestion[c].createdOn).split(" ");
                        var dateArray = dateAndTime[0].split("-");
                        var dateString = dateArray[0]+dateArray[1]+dateArray[2];
                        $scope.messageList[i].suggestion[c].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
                        $scope.messageList[i].suggestion[c].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3);  
                    }  
                     
                    $scope.messageList[i].suggestionLength = $scope.messageList[i].suggestion.length;
                 }
                
            }
            
            if(response.data.status == "fail")
            {
                $scope.isAlertForInbox = true;
                $scope.inboxAlertMessage = "Some issue in backend we couldnt recv your messages";
            }
            
            else if($scope.messageList.length == 0)
            {
                $scope.isAlertForInbox = true;
                $scope.inboxAlertMessage = "You have no messages.";
            }    
            
            else {
            
               $scope.isAlertForInbox = false;
            }
            
            
        })
        .error(function(){
            alert("some error");
        }); 
    }    
        
    /** end **/

    $scope.suggestionReset = function(messageId)
    {
        $scope.inputSuggestion[messageId] = "";
        $scope.isSuggestionSubmitErrorMessage = true;
        $scope.suggestionSubmitErrorMessage = "";
    }
        
    $scope.submitOpinion = function(messageId,index)
    {
        var choosenCommentId = $scope.optionValue[messageId];
        
        
        if(choosenCommentId == undefined || choosenCommentId == "")
        {
            SweetAlert.swal("Oops...", "You need to choose one option to close the answer", "error");
        }
        else {
            
            
         $scope.messageList[index].buttonDisabled = true;    
            
            
        /** warning message before submitting the question **/ 
            
        SweetAlert.swal({   
            title: "Are you sure?",   
            text: "No voting will happen after this",   
            type: "warning",   showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, I accept the answer !",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, 
            function(isConfirm){
                
                if (isConfirm) {
                // ** Submitting Options ** //
                var comment_url = appConfig.API_HOST + appConstants.COMMENT_CREATE + messageId + "/" + "comments/" + choosenCommentId + "/likes" ;
                var formData = new FormData();
                formData.append('commentId',choosenCommentId);
                /** for recv this key has to be no and for sender it has to be yes**/
                formData.append('closeMessage','yes');
                commentFactory.send(formData, comment_url)
                .success(function(response) {
    
                    if(response.status == "success")
                    {
                        SweetAlert.swal("Accepted!", "Your audience can now view your answer", "success");
                        
                        for( var l = 0; l < $scope.messageList.length ; l++)
                        {
                            if($scope.messageList[l].messageId == messageId)
                            {

                                for(var c = 0 ; c < $scope.messageList[l].comment.length ; c++)
                                {
                                    if($scope.messageList[l].comment[c].commentId  == choosenCommentId)
                                    {    
                                    $scope.messageList[l].isClosed = "yes"; 
                                    $scope.messageList[l].comment[c].isLikeByOwner = 'yes';
                                    $scope.messageList[l].isDisplaySubmit = "no";   
                                    }
                                }

                            }
                        }

                  }
                  else{

                    // returning back as it was //
                    SweetAlert.swal("Sorry", "Some issue in backend", "error"); 
                      
                    $scope.messageList[index].buttonDisabled = false;   
                      
                    for( var l = 0; l < $scope.messageList.length ; l++)
                    {
                        if($scope.messageList[l].messageId == messageId)
                        {
                            
                            for(var c = 0 ; c < $scope.messageList[l].comment.length ; c++)
                            {
                                if($scope.messageList[l].comment[c].commentId  == choosenCommentId)
                                {    
                                    $scope.optionValue[messageId] = "";
                                }
                            }
                            
                        }
                    }
                    
                                                        
                   }

                }).error(function(){

                    // returning back as it was //
                    SweetAlert.swal("Sorry", "Some issue in backend", "error"); 
                    
                    $scope.messageList[index].buttonDisabled = false; 
                    
                    for( var l = 0; l < $scope.messageList.length ; l++)
                    {
                        if($scope.messageList[l].messageId == messageId)
                        {
                            
                            for(var c = 0 ; c < $scope.messageList[l].comment.length ; c++)
                            {
                                if($scope.messageList[l].comment[c].commentId  == choosenCommentId)
                                {    
                                    $scope.optionValue[messageId] = "";
                                }
                            }
                            
                        }
                    }

                });
                    
                    
                    
                } 
                else 
                {   
                    // returning back as it was //
                    $scope.messageList[index].buttonDisabled = false; 
                    
                    for( var l = 0; l < $scope.messageList.length ; l++)
                    {
                        if($scope.messageList[l].messageId == messageId)
                        {
                            
                            for(var c = 0 ; c < $scope.messageList[l].comment.length ; c++)
                            {
                                if($scope.messageList[l].comment[c].commentId  == choosenCommentId)
                                {    
                                    $scope.optionValue[messageId] = "";
                                }
                            }
                            
                        }
                    }
                    
                    SweetAlert.swal("Cancelled", "Voting has not stopped :)", "error");   
                } 
            });    
            
            
           
        }
                
        
    }
    
    $scope.commentReturnValue = function(messageId)
    {
        if(messageId == $rootScope.scrollIntoViewMessageId)
        {
            return false;
        }
        else {
            return true;
        }
    };
   
        
    
    
}]);

//_revlegApp.controller("adminProfileEditController",[ "$scope" , "getAllProfileFactory" , "$routeParams", "authService" , function($scope , getAllProfileFactory , $routeParams , authService) {
//    
//    
//     var profileListRecvUrl = appConfig.API_HOST + appConstants.GET_SINGLE_PROFILE + "/" + $routeParams.authCode + "/" + $routeParams.userId;
//        
//     getAllProfileFactory.get(profileListRecvUrl)
//        .success(function(response) {
//            var offset = 0;
//            
//            
//            
//        })
//        .error(function(){
//            alert("some error");
//        });    
//}]);


_revlegApp.controller("openQuestionsController",[ "$scope" , "messageFactory" , "commentFactory" , "$routeParams", "authService" , "SweetAlert"  , "formFactory" , function($scope , messageFactory , commentFactory , $routeParams , authService , SweetAlert , formFactory) {
 
    
    $scope.user = {};
    $scope.optionValue = {};
    $scope.isAlertForOpenQuestion = false;
    
    $scope.$watch(
        function(){ return  authService.getProfilePic() },

        function(newVal) {
            $scope.user.profilePic = newVal;
        }
    );
        
    
    if($routeParams.messageId != undefined)
    {
        
        /** need to work for a single msg incomplete **/
        
        var messageRecvUrl = appConfig.API_HOST + appConstants.SINGLE_MSG + $routeParams.messageId;
        
        messageFactory.recv(messageRecvUrl)
        .success(function(response) {
            
            var offset = 0;
            $scope.messageList = response.data.message;
            
            
            for (var i = 0 ; i < $scope.messageList.length ; i++ )
            {
                var currentMessageId = $scope.messageList[i].messageId;
                $scope.messageList[i].anyAction = "yes";
                $scope.messageList[i].messageStatus = "open";
                $scope.messageList[i].buttonDisabled = false;
                
                var routeCode = $scope.messageList[i].url ;
                if(routeCode == 2)
                {
                    // everything is fine //
                }
                else {
                
                $scope.isAlertForOpenQuestion = true;
                $scope.openQuestionAlertMessage = "No Message Avaliable";
                
                }
                
                var dateAndTime = ($scope.messageList[i].createdOn).split(" ");
                var dateArray = dateAndTime[0].split("-");
                var dateString = dateArray[0]+dateArray[1]+dateArray[2];
                //$scope.messageList[i].date = dateAndTime[0];
                $scope.messageList[i].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
                $scope.messageList[i].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3); 
                
                if( $scope.messageList[i].messageType == "0")
                {
                    $scope.messageList[i].anyAction = "no"; 
                    
                }
                else if($scope.messageList[i].isClosed == "yes")
                {
                    $scope.messageList[i].anyAction = "no";
                    $scope.messageList[i].messageStatus = "closed";
                    
                    for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
                    {
                        var currentComment = $scope.messageList[i].comment[c];
                        $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);
                        
                        if(currentComment.isLikeByUser == "yes")
                        {
                            $scope.optionValue[currentMessageId] = currentComment.commentId;
                        }
                    }
                    
                }
                else 
                {
                    for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
                    {
                        var currentComment = $scope.messageList[i].comment[c];
                        $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);
                        
                        if(currentComment.isLikeByUser == "yes")
                        {
                            $scope.optionValue[currentMessageId] = currentComment.commentId;
                            $scope.messageList[i].anyAction = "no";
                            $scope.messageList[i].messageStatus = "submitted";
                        }
                    }
                }
                if($scope.messageList[i].suggestion == undefined)
                 {
                      
                    $scope.messageList[i].suggestion = []; 
                    $scope.messageList[i].suggestionLength = 0;
                 } 
                 else {
                     
                    var suggestionList = $scope.messageList[i].suggestion;
                    for(var c = 0 ; c < suggestionList.length ; c++ )
                    {
                        var dateAndTime = ($scope.messageList[i].suggestion[c].createdOn).split(" ");
                        var dateArray = dateAndTime[0].split("-");
                        var dateString = dateArray[0]+dateArray[1]+dateArray[2];
                        $scope.messageList[i].suggestion[c].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
                        $scope.messageList[i].suggestion[c].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3); 
                    }
                     
                    $scope.messageList[i].suggestionLength = $scope.messageList[i].suggestion.length;
                 }
                
            }
            
            if(response.data.status == "fail")
            {
                $scope.isAlertForInbox = true;
                $scope.inboxAlertMessage = "Some issue in backend we couldnt recv your messages";
            }
            
            if($scope.messageList.length == 0)
            {
                $scope.isAlertForInbox = true;
                $scope.inboxAlertMessage = "You have no messages.";
            }    
            
        });
    }    
    else {
    
            
        // to get all the messages //
//        $scope.isAlertForInbox = false;
//        var messageRecvUrl = appConfig.API_HOST + appConstants.MESSAGE_GLOBAL + "&page=1&item=10";
//
//        messageFactory.recv(messageRecvUrl)
//            .success(function(response) {
//            var offset = 0;
//            $scope.messageList = response.data.messages;
//            
//            
//            for (var i = 0 ; i < $scope.messageList.length ; i++ )
//            {
//                var currentMessageId = $scope.messageList[i].messageId;
//                $scope.messageList[i].anyAction = "yes";
//                $scope.messageList[i].messageStatus = "open";
//                
//                var dateAndTime = ($scope.messageList[i].createdOn).split(" ");
//                var dateArray = dateAndTime[0].split("-");
//                var dateString = dateArray[0]+dateArray[1]+dateArray[2];
//                //$scope.messageList[i].date = dateAndTime[0];
//                $scope.messageList[i].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
//                $scope.messageList[i].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3); 
//                
//                if( $scope.messageList[i].messageType == "0")
//                {
//                    $scope.messageList[i].anyAction = "no"; 
//                    
//                }
//                else if($scope.messageList[i].isClosed == "yes")
//                {
//                    $scope.messageList[i].anyAction = "no";
//                    $scope.messageList[i].messageStatus = "closed";
//                    
//                    for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
//                    {
//                        var currentComment = $scope.messageList[i].comment[c];
//                        $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);
//                        
//                        if(currentComment.isLikeByUser == "yes")
//                        {
//                            $scope.optionValue[currentMessageId] = currentComment.commentId;
//                        }
//                    }
//                    
//                }
//                else 
//                {
//                    for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
//                    {
//                        var currentComment = $scope.messageList[i].comment[c];
//                        $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);
//                        
//                        if(currentComment.isLikeByUser == "yes")
//                        {
//                            $scope.optionValue[currentMessageId] = currentComment.commentId;
//                            $scope.messageList[i].anyAction = "no";
//                            $scope.messageList[i].messageStatus = "submitted";
//                        }
//                    }
//                }
//                if($scope.messageList[i].suggestion == undefined)
//                 {
//                      
//                    $scope.messageList[i].suggestion = []; 
//                    $scope.messageList[i].suggestionLength = 0;
//                 } 
//                 else {
//                     
//                    var suggestionList = $scope.messageList[i].suggestion;
//                    for(var c = 0 ; c < suggestionList.length ; c++ )
//                    {
//                        var dateAndTime = ($scope.messageList[i].suggestion[c].createdOn).split(" ");
//                        var dateArray = dateAndTime[0].split("-");
//                        var dateString = dateArray[0]+dateArray[1]+dateArray[2];
//                        $scope.messageList[i].suggestion[c].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
//                        $scope.messageList[i].suggestion[c].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3); 
//                    }
//                     
//                    $scope.messageList[i].suggestionLength = $scope.messageList[i].suggestion.length;
//                 }
//                
//            }
//            
//            if(response.data.status == "fail")
//            {
//                $scope.isAlertForInbox = true;
//                $scope.inboxAlertMessage = "Some issue in backend we couldnt recv your messages";
//            }
//            
//            else if($scope.messageList.length == 0)
//            {
//                $scope.isAlertForInbox = true;
//                $scope.inboxAlertMessage = "You have no messages.";
//            }    
//            
//            else {
//            
//               $scope.isAlertForInbox = false;
//            }
//            
//            
//        })
//        .error(function(){
//            alert("some error");
//        });    
        
    
    }
    
    
    $scope.upVote = function(messageId)
    {
        
            // ** Submitting Options ** //
            var upVote_url = appConfig.API_HOST + appConstants.COMMENT_CREATE + messageId + "/likes";
            var formData = new FormData();            
            // increasing the number of comments here //
            for( var l = 0; l < $scope.messageList.length ; l++)
            {
                if($scope.messageList[l].messageId == messageId)
                {
                    var messageTotalAudience = $scope.messageList[l].totalReach;
                    $scope.messageList[l].totalLike+=1;
                    $scope.messageList[l].isLikeByUser = 'yes';
                }
            }
            
            formFactory.submit(formData, upVote_url)
            .success(function(response) {

                if(response.status == "success")
                {
                    // do nothing //

                }
                else{

                    // on failure returning back to orginal //
                    SweetAlert.swal("Oops...", "Something went wrong!", "error");
                    
                    for( var l = 0; l < $scope.messageList.length ; l++)
                    {
                        if($scope.messageList[l].messageId == messageId)
                        {
                            var messageTotalAudience = $scope.messageList[l].totalReach;
                            $scope.messageList[l].totalLike-=1;
                            $scope.messageList[l].isLikeByUser = 'no';
                        }
                    }
                    
                }

            }).error(function(){

                    // on failure returning back to orginal //
                    SweetAlert.swal("Oops...", "Something went wrong!", "error");
                    
                    for( var l = 0; l < $scope.messageList.length ; l++)
                    {
                        if($scope.messageList[l].messageId == messageId)
                        {
                            var messageTotalAudience = $scope.messageList[l].totalReach;
                            $scope.messageList[l].totalLike-=1;
                            $scope.messageList[l].isLikeByUser = 'no';
                        }
                    }
            });
        
    };
    
    $scope.downVote = function(messageId)
    {
            // ** Submitting Options ** //
            var downVote_url = appConfig.API_HOST + appConstants.COMMENT_CREATE + messageId + "/dislikes";
            var formData = new FormData();            
            // increasing the number of comments here //
            for( var l = 0; l < $scope.messageList.length ; l++)
            {
                if($scope.messageList[l].messageId == messageId)
                {
                    var messageTotalAudience = $scope.messageList[l].totalReach;
                    $scope.messageList[l].totalDisLike+=1;
                    $scope.messageList[l].isDisLikeByUser = 'yes';
                }
            }
            formFactory.submit(formData, downVote_url)
            .success(function(response) {

                if(response.status == "success")
                {
                    // do nothing //

                }
                else{

                    // on failure returning back to orginal //
                    SweetAlert.swal("Oops...", "Something went wrong!", "error");
                    
                    for( var l = 0; l < $scope.messageList.length ; l++)
                    {
                        if($scope.messageList[l].messageId == messageId)
                        {
                            var messageTotalAudience = $scope.messageList[l].totalReach;
                            $scope.messageList[l].totalDisLike-=1;
                            $scope.messageList[l].isDisLikeByUser = 'no';
                        }
                    }
                    
                }

            }).error(function(){

                    // on failure returning back to orginal //
                    SweetAlert.swal("Oops...", "Something went wrong!", "error");
                    
                    for( var l = 0; l < $scope.messageList.length ; l++)
                    {
                        if($scope.messageList[l].messageId == messageId)
                        {
                            var messageTotalAudience = $scope.messageList[l].totalReach;
                            $scope.messageList[l].totalDisLike-=1;
                            $scope.messageList[l].isDisLikeByUser = 'no';
                        }
                    }
            });
    };
    
    $scope.submitOpinion = function(messageId,index)
    {
        var choosenCommentId = $scope.optionValue[messageId];
        
        
        
        if(choosenCommentId == undefined)
        {
            SweetAlert.swal("Oops...", "You need to choose one option", "error");
        }
        else {
            
            // handling the double click issues //
            $scope.messageList[index].buttonDisabled = true;
            
            // ** Submitting Options ** //
            var comment_url = appConfig.API_HOST + appConstants.COMMENT_CREATE + messageId + "/" + "comments/" + choosenCommentId + "/likes" ;
            var formData = new FormData();
            formData.append('commentId',choosenCommentId);
            /** for recv this key has to be no and for sender it has to be yes**/
            formData.append('closeMessage','no');
            
            
            // increasing the number of comments here //
            for( var l = 0; l < $scope.messageList.length ; l++)
            {
                if($scope.messageList[l].messageId == messageId)
                {
                    var messageTotalAudience = $scope.messageList[l].totalReach;

                    for(var c = 0 ; c < $scope.messageList[l].comment.length ; c++)
                    {
                        if($scope.messageList[l].comment[c].commentId  == choosenCommentId)
                        {    

                        var commentTotalAudeinceNew = $scope.messageList[l].comment[c].totalLike + 1;     
                            
                        if($scope.messageList[l]['isGlobal'] == "yes")
                        {
                            var messageTotalAudience = messageTotalAudience + 1; 
                            $scope.messageList[l].totalReach = messageTotalAudience;
                        }
                        
                            
                        var percentVote = parseFloat ( (commentTotalAudeinceNew / messageTotalAudience ) * 100 );
                        percentVote.toFixed(2);    
                            
                        var orginalPrecentLike = $scope.messageList[l].comment[c].percentLike;
                        var originalTotalLike = $scope.messageList[l].comment[c].totalLike;  
                        $scope.messageList[l].comment[c].percentLike = percentVote.toFixed(2);  
                        $scope.messageList[l].comment[c].totalLike  += 1; 
                        $scope.messageList[l].anyAction = "no";
                        $scope.messageList[l].messageStatus = "submitted"; 
                        $scope.messageList[l].comment[c].isLikeByUser = 'yes';

                        }
                    }

                }
            }
            
            
            commentFactory.send(formData, comment_url)
            .success(function(response) {

                if(response.status == "success")
                {

                     SweetAlert.swal("Accepted!", "Now you can follow this message from Inbox", "success");    
                    
//                    for( var l = 0; l < $scope.messageList.length ; l++)
//                    {
//                        if($scope.messageList[l].messageId == messageId)
//                        {
//                            var messageTotalAudience = $scope.messageList[l].totalReach;
//                            var commentTotalAudeinceNew = $scope.messageList[l].comment[choosenCommentId].totalLike + 1; 
//                            var percentVote = parseFloat ( (commentTotalAudeinceNew / messageTotalAudience ) * 100 );
//                            
//                            
//                            percentVote.toFixed(2);
//                            
//                            
//                            for(var c = 0 ; c < $scope.messageList[l].comment.length ; c++)
//                            {
//                                if($scope.messageList[l].comment[c].commentId  == choosenCommentId)
//                                {    
//                                
//                                $scope.messageList[l].comment[c].percentLike =  percentVote;
//                                $scope.messageList[l].comment[c].totalLike  += 1; 
//                                $scope.messageList[l].anyAction = "no";
//                                $scope.messageList[l].messageStatus = "submitted"; 
//                                $scope.messageList[l].comment[c].isLikeByUser = 'yes';    
//                                    
//                                }
//                            }
//                            
//                        }
//                    }

                }
                else{

                    // on failure returning back to orginal //
                    SweetAlert.swal("Oops...", "Something went wrong!", "error");
                    
                    $scope.messageList[index].buttonDisabled = false;
                    
                    for( var l = 0; l < $scope.messageList.length ; l++)
                    {
                        if($scope.messageList[l].messageId == messageId)
                        {
                            var messageTotalAudience = $scope.messageList[l].totalReach;                            
                            
                            for(var c = 0 ; c < $scope.messageList[l].comment.length ; c++)
                            {
                                if($scope.messageList[l].comment[c].commentId  == choosenCommentId)
                                {    
                                                                    
                                $scope.messageList[l].comment[c].percentLike =  orginalPrecentLike;
                                $scope.messageList[l].comment[c].totalLike  = originalTotalLike; 
                                $scope.messageList[l].anyAction = "yes";
                                $scope.messageList[l].messageStatus = "open"; 
                                $scope.messageList[l].comment[c].isLikeByUser = 'no';        
                                }
                            }
                            
                           $scope.optionValue[messageId] = "";   
                            
                            /** for decreasing the audeince **/
                            if($scope.messageList[l]['isGlobal'] == "yes")
                            {
                                $scope.messageList[l].totalReach = $scope.messageList[l].totalReach - 1;
                            } 
                            
                            
                        }
                    }
                    
                }

            }).error(function(){

                    // on failure returning back to orginal //
                    SweetAlert.swal("Oops...", "Something went wrong!", "error");
                    
                    $scope.messageList[index].buttonDisabled = false;
                
                    for( var l = 0; l < $scope.messageList.length ; l++)
                    {
                        if($scope.messageList[l].messageId == messageId)
                        {
                            var messageTotalAudience = $scope.messageList[l].totalReach;

                            for(var c = 0 ; c < $scope.messageList[l].comment.length ; c++)
                            {
                                if($scope.messageList[l].comment[c].commentId  == choosenCommentId)
                                {  
                                    
                                $scope.messageList[l].comment[c].percentLike =  orginalPrecentLike;
                                $scope.messageList[l].comment[c].totalLike = originalTotalLike;     
                                $scope.messageList[l].anyAction = "yes";
                                $scope.messageList[l].messageStatus = "open"; 
                                $scope.messageList[l].comment[c].isLikeByUser = 'no';    
                                    
                                }
                            }
                            
                           $scope.optionValue[messageId] = "";
                            
                            /** for decreasing the audeince **/
                            if($scope.messageList[l]['isGlobal'] == "yes")
                            {
                                $scope.messageList[l].totalReach = $scope.messageList[l].totalReach - 1;
                            }    
                            
                        }
                    }

            });
        
        }
        
        
        
    }
    
    $scope.currentPage = 1;
    $scope.itemPerPage = 10;
    $scope.stopScrollingForData = "no";
    $scope.messageScrollBusy = false;
    
    
    
    if($scope.messageList == undefined)
    {
        $scope.messageList = [];
    }

    
    // the ifinite scrolling pagination function 
    $scope.myPagingFunction = function()
    {
        if ($scope.messageScrollBusy) return;
        $scope.messageScrollBusy = true;
        
        if($scope.stopScrollingForData == 'no')
        {
        // to get all the messages //
        $scope.isAlertForInbox = false;
        var messageRecvUrl = appConfig.API_HOST + appConstants.MESSAGE_GLOBAL + "&page="+$scope.currentPage+"&item="+10;

        messageFactory.recv(messageRecvUrl)
            .success(function(response) {
            
            var responseArray = response.data.messages;
            var start = $scope.messageList.length;   
            var oldLength =  $scope.messageList.length ;
            
            if(response.data.messages.length == 0)
            {
                 $scope.stopScrollingForData = 'yes';
            }
            else {
            
                
            for(var i = 0 ; i < responseArray.length ; i++)
            {
                $scope.messageList[start] = responseArray[i];
                start++;
            }
                  
            for (var i = oldLength ; i < $scope.messageList.length ; i++ )
            {
                var currentMessageId = $scope.messageList[i].messageId;
                $scope.messageList[i].anyAction = "yes";
                $scope.messageList[i].messageStatus = "open";
                $scope.messageList[i].buttonDisabled = false;
                
                var dateAndTime = ($scope.messageList[i].createdOn).split(" ");
                var dateArray = dateAndTime[0].split("-");
                var dateString = dateArray[0]+dateArray[1]+dateArray[2];
                //$scope.messageList[i].date = dateAndTime[0];
                $scope.messageList[i].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
                $scope.messageList[i].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3); 
                
                if( $scope.messageList[i].messageType == "0")
                {
                    $scope.messageList[i].anyAction = "no"; 
                    
                }
                else if($scope.messageList[i].isClosed == "yes")
                {
                    $scope.messageList[i].anyAction = "no";
                    $scope.messageList[i].messageStatus = "closed";
                    
                    for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
                    {
                        var currentComment = $scope.messageList[i].comment[c];
                        $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);
                        
                        if(currentComment.isLikeByUser == "yes")
                        {
                            $scope.optionValue[currentMessageId] = currentComment.commentId;
                        }
                    }
                    
                }
                else 
                {
                    for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
                    {
                        var currentComment = $scope.messageList[i].comment[c];
                        $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);
                        
                        if(currentComment.isLikeByUser == "yes")
                        {
                            $scope.optionValue[currentMessageId] = currentComment.commentId;
                            $scope.messageList[i].anyAction = "no";
                            $scope.messageList[i].messageStatus = "submitted";
                        }
                    }
                }
                if($scope.messageList[i].suggestion == undefined)
                 {
                      
                    $scope.messageList[i].suggestion = []; 
                    $scope.messageList[i].suggestionLength = 0;
                 } 
                 else {
                     
                    var suggestionList = $scope.messageList[i].suggestion;
                    for(var c = 0 ; c < suggestionList.length ; c++ )
                    {
                        var dateAndTime = ($scope.messageList[i].suggestion[c].createdOn).split(" ");
                        var dateArray = dateAndTime[0].split("-");
                        var dateString = dateArray[0]+dateArray[1]+dateArray[2];
                        $scope.messageList[i].suggestion[c].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
                        $scope.messageList[i].suggestion[c].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3); 
                    }
                     
                    $scope.messageList[i].suggestionLength = $scope.messageList[i].suggestion.length;
                 }
                
            }
            
            if(response.data.status == "fail")
            {
                $scope.isAlertForInbox = true;
                $scope.inboxAlertMessage = "Some issue in backend we couldnt recv your messages";
            }
            
            else if($scope.messageList.length == 0)
            {
                $scope.isAlertForInbox = true;
                $scope.inboxAlertMessage = "You have no messages.";
            }    
            
            else {
            
               $scope.isAlertForInbox = false;
            }
            
            
            }
                
            $scope.currentPage = $scope.currentPage + 1;   
            $scope.messageScrollBusy = false ;    
                  
        })
        .error(function(){
            alert("some error");
        });   
        
        //increasing the number of page
        
            
        }

        
    }
    
   
        
    
    
}]);

_revlegApp.controller("notificationController",[ "$scope" ,"$routeParams" , "$location" ,  "anchorSmoothScroll" , "$rootScope" , "socket" , "authService" , "ipCookie" , "commonFunctions" , "socketService" , function($scope , $routeParams , $location , anchorSmoothScroll , $rootScope , socket , authService , ipCookie , commonFunctions , socketService ) {

//         var globalNotificationMax = 6;
         $scope.notificationList = [];
         $rootScope.notificationBellClass = "notificationBellNormal";
         $rootScope.notification_count = 0;
         $rootScope.iSNotificationCountVisible = false;
         
            
          $scope.$watch(
          function(){ return  authService.checkLogin() },

           function(newVal) {
               
               
               //for logout //
               
               if(!newVal)
               {
                   console.log("connection disconnet");
                   //var socketConnection = socketService.getSocketObject();
                   //socketConnection.disconnect();
               }
               
               
               if(newVal)
               {
                   
                                  
                   console.log("here for notification");
                   
                   var socketConnection = socket.startSocketConnection(); 
                   
                   socketConnection.emit('authenticate', {
                   userId: ipCookie('userId'),
                   });
                   
                     socketService.saveSocketObject(socketConnection);
    
                     socketConnection.on('notification', function (data) {
                         

                       var header = data.header;    
                       var singleNotificationObj = {};
                       $rootScope.notificationBellClass = "notificationBellRed";    
                       singleNotificationObj.oldClass = "newNotification";    
                         
                       $rootScope.notification_count++; 
                       $rootScope.iSNotificationCountVisible = true;   
                         
                         
//                       if($scope.notificationList.length > globalNotificationMax )
//                       {
//                          // POP //
//                          $scope.notificationList.pop(); 
//                       }       

                       if(header.notifyUserForProfileMileStone)
                       {
                           //alert("oh its milestone achieved");
                           singleNotificationObj.type = "notifyUserForProfileMileStone" ;
                           ipCookie('userScore',data.data.points,{ expires: 365 });
                           $scope.notificationList.unshift(singleNotificationObj);

                       }
                       if(header.notifyUserForRightAnswer)
                       {
                           //alert("oh its Right Awnser achieved");
                           // Push on the top //
                           singleNotificationObj.type = "notifyUserForRightAnswer" ;
                           singleNotificationObj.messageObj = data.data.notification ;  
                           if(singleNotificationObj.messageObj.messageSubject.length > 25)
                           {
                                singleNotificationObj.messageObj.messageSubject =  singleNotificationObj.messageObj.messageSubject.substr(0,25);
                                singleNotificationObj.messageObj.messageSubject += " .....";
                           }
                           if(singleNotificationObj.messageObj.messageBody.length > 40)
                           {
                                singleNotificationObj.messageObj.messageBody =  singleNotificationObj.messageObj.messageBody.substr(0,40);
                                singleNotificationObj.messageObj.messageBody += " .....";
                           }


                           $scope.notificationList.unshift(singleNotificationObj);


                       }
                       if(header.notifyUserForMessageClose)
                       {
                           //alert("oh its message closed but not right awnser");
                           singleNotificationObj.type = "notifyUserForMessageClose" ;
                           singleNotificationObj.messageObj = data.data.notification ;  
                           if(singleNotificationObj.messageObj.messageSubject.length > 25)
                           {
                                singleNotificationObj.messageObj.messageSubject =  singleNotificationObj.messageObj.messageSubject.substr(0,25);
                                singleNotificationObj.messageObj.messageSubject += " .....";
                           }
                           if(singleNotificationObj.messageObj.messageBody.length > 40)
                           {
                                singleNotificationObj.messageObj.messageBody =  singleNotificationObj.messageObj.messageBody.substr(0,40);
                                singleNotificationObj.messageObj.messageBody += " .....";
                           }
                           $scope.notificationList.unshift(singleNotificationObj);
                       }
                       if(header.notifyUserForVotingMileStone)
                       {
                          //alert("oh its vting milestone achieved"); 
                          singleNotificationObj.type = "notifyUserForVotingMileStone" ;
                          singleNotificationObj.messageObj = data.data.notification ;  
                          singleNotificationObj.percentLike =  data.data.percentLike.toFixed(2) ;
                          if(singleNotificationObj.messageObj.messageSubject.length > 25)
                           {
                                singleNotificationObj.messageObj.messageSubject =  singleNotificationObj.messageObj.messageSubject.substr(0,25);
                                singleNotificationObj.messageObj.messageSubject += " .....";
                           } 
                          if(singleNotificationObj.messageObj.messageBody.length > 50)
                           {
                                singleNotificationObj.messageObj.messageBody =  singleNotificationObj.messageObj.messageBody.substr(0,50);
                                singleNotificationObj.messageObj.messageBody += " .....";
                           }
                          $scope.notificationList.unshift(singleNotificationObj);
                       }
                       if(header.notifyUserForSuggestion)
                       {
                           //alert("oh its message closed but not right awnser");
                           singleNotificationObj.type = "notifyUserForSuggestion" ;
                           singleNotificationObj.isOwner = data.isOwner;
                           singleNotificationObj.messageObj = data.data.notification ;  

                           if(singleNotificationObj.messageObj.messageSubject.length > 25)
                           {
                                singleNotificationObj.messageObj.messageSubject =  singleNotificationObj.messageObj.messageSubject.substr(0,25);
                                singleNotificationObj.messageObj.messageSubject += " .....";
                           }
                           
                           if(singleNotificationObj.messageObj.messageBody.length > 40)
                           {
                                singleNotificationObj.messageObj.messageBody =  singleNotificationObj.messageObj.messageBody.substr(0,40);
                                singleNotificationObj.messageObj.messageBody += " .....";
                           }
                           
//                           for each(var item in singleNotificationObj.messageObj.suggestion)
//                           {
//                               singleNotificationObj.messageObj.suggestionBody = item.suggestionBody;
//                               singleNotificationObj.messageObj.suggestionCreaterUserName = item.createrUserName;
//                           }
                           
                           angular.forEach(singleNotificationObj.messageObj.suggestion, function(value, key) {
                               
                               singleNotificationObj.messageObj.suggestionBody = value.suggestionBody;
                               singleNotificationObj.messageObj.suggestionCreaterUserName = value.createrUserName;
                               
                           });
                           
                           if(singleNotificationObj.messageObj.suggestionBody.length > 50)
                           {
                               singleNotificationObj.messageObj.suggestionBody = singleNotificationObj.messageObj.suggestionBody.substr(0,40);
                               singleNotificationObj.messageObj.suggestionBody += ".......";
                           }
                           
                           $scope.notificationList.unshift(singleNotificationObj);
                       }

                    });
            }    
                   
           }           
               
              
           );  

               

     
            $scope.not_votingMileStoneClick = function(messageId,index)
            {
                
                $rootScope.notificationBellClass = "notificationBellNormal";
                //var index = commonFunctions.getNotificationIndex($scope.notificationList,messageId);
                
                if($scope.notificationList[index].oldClass == "newNotification")
                {
                    $scope.notificationList[index].oldClass = "oldNotification";
                }    
                
                $rootScope.scrollIntoViewMessageId = "none";
                if($location.path() == '/outbox')
                {
                     /** expand the correct **/
                     $location.path("/outbox/" +messageId);
                }
                else {

                    $rootScope.scrollIntoViewMessageId = messageId;
                    $location.path("/outbox");
                    
                }
                
//                $rootScope.notification_count--; 
//                if($rootScope.notification_count == 0)
//                {
//                $rootScope.iSNotificationCountVisible = false;   
//                }
                
                $rootScope.notification_count = 0;
                $rootScope.iSNotificationCountVisible = false;
                
                
            };
            $scope.not_rightAwnserClick = function(messageId,index)
            {
                //var index = commonFunctions.getNotificationIndex($scope.notificationList,messageId);
                //$scope.notificationList.splice(index,1);
                
                $rootScope.notificationBellClass = "notificationBellNormal";
                if($scope.notificationList[index].oldClass == "newNotification")
                {
                    $scope.notificationList[index].oldClass = "oldNotification";
                }
                
                $rootScope.scrollIntoViewMessageId = "none";
                if($location.path() == '/inbox')
                {
                     /** expand the correct **/
                     $location.path("/inbox/" +messageId);
                }
                else {

                    $rootScope.scrollIntoViewMessageId = messageId;
                    $location.path("/inbox");
                    
                }
                
//                $rootScope.notification_count--; 
//                if($rootScope.notification_count == 0)
//                {
//                $rootScope.iSNotificationCountVisible = false;   
//                }
                
                $rootScope.notification_count = 0;
                $rootScope.iSNotificationCountVisible = false;   
                
            };
            $scope.not_messageCloseClick = function(messageId,index)
            {
                //var index = commonFunctions.getNotificationIndex($scope.notificationList,messageId);
                //$scope.notificationList.splice(index,1);
                
                $rootScope.notificationBellClass = "notificationBellNormal";
                if($scope.notificationList[index].oldClass == "newNotification")
                {
                    $scope.notificationList[index].oldClass = "oldNotification";
                }
                
                $rootScope.scrollIntoViewMessageId = "none";
                if($location.path() == '/inbox')
                {
                     /** expand the correct **/
                     $location.path("/inbox/" +messageId);
                }
                else {

                    $rootScope.scrollIntoViewMessageId = messageId;
                    $location.path("/inbox");
                }
                
//                $rootScope.notification_count--; 
//                if($rootScope.notification_count == 0)
//                {
//                $rootScope.iSNotificationCountVisible = false;   
//                }
                
                $rootScope.notification_count = 0;
                $rootScope.iSNotificationCountVisible = false; 
            };
            $scope.not_messageSuggestionClick = function(messageId,index,isOwner)
            {
                //var index = commonFunctions.getNotificationIndex($scope.notificationList,messageId);
                //$scope.notificationList.splice(index,1);
                
                $rootScope.notificationBellClass = "notificationBellNormal";
                if($scope.notificationList[index].oldClass == "newNotification")
                {
                    $scope.notificationList[index].oldClass = "oldNotification";
                }
                
                  $rootScope.scrollIntoViewMessageId = "none";
                
                  if(isOwner == 'yes')
                  {
                    // route to outbox
                    if($location.path() == '/outbox')
                    {
                    /** expand the correct **/
                    $location.path("/outbox/" +messageId);
                    $rootScope.scrollIntoViewMessageId = messageId;    
                    } 
                    else {
                    
                    $rootScope.scrollIntoViewMessageId = messageId;
                    $location.path("/outbox");    
                        
                    }  
                      
                  }
                  else {
                     
                    // route to inbox 
                    if($location.path() == '/inbox')
                    {
                     /** expand the correct **/
                     $location.path("/inbox/" +messageId);
                     $rootScope.scrollIntoViewMessageId = messageId;    
                    } 
                    else {
                    
                    $rootScope.scrollIntoViewMessageId = messageId;
                    $location.path("/inbox");    
                        
                    }  
                      
                   
                  }
                
//                $rootScope.notification_count--; 
//                if($rootScope.notification_count == 0)
//                {
//                $rootScope.iSNotificationCountVisible = false;   
//                }
                
                $rootScope.notification_count = 0;
                $rootScope.iSNotificationCountVisible = false; 
                
            };
            
            $scope.not_profileMilestone = function(index)
            {
                
                $rootScope.notificationBellClass = "notificationBellNormal";
                if($scope.notificationList[index].oldClass == "newNotification")
                {
                    $scope.notificationList[index].oldClass = "oldNotification";
                }
                
                  //$scope.notificationList.splice(index,1);    
                  $location.path("/profile");     
                  
                
//                $rootScope.notification_count--; 
//                if($rootScope.notification_count == 0)
//                {
//                $rootScope.iSNotificationCountVisible = false;   
//                }
                
                $rootScope.notification_count = 0;
                $rootScope.iSNotificationCountVisible = false; 
            };
    
       

}]);

_revlegApp.controller("poolController",[ "$scope" , "messageFactory" , "commentFactory" , "$routeParams", "authService" , "SweetAlert" , "anchorSmoothScroll" , "$rootScope" , "$timeout", "$document" , "$location" , function($scope , messageFactory , commentFactory , $routeParams , authService , SweetAlert , anchorSmoothScroll , $rootScope , $timeout,$document,$location) {

    
    $scope.user = {};
    $scope.optionValue = {};
    $scope.inputSuggestion = [];
    $scope.isSuggestionSubmitErrorMessage = true;
    $scope.routeCode = null;
    
    if(authService.checkLogin() == true)
    {
        // when logged in  //
        //calling the message url again .. this time with session
        /** need to work for a single msg incomplete **/
        var messageRecvUrl = appConfig.API_HOST + appConstants.SINGLE_MSG + $routeParams.messageId;
        messageFactory.recv(messageRecvUrl)
        .success(function(response) {

        var offset = 0;
        $scope.messageList = response.data.message;
        
         
        if(response.data.status == "fail")
        {
            $scope.isAlertForInbox = true;
            $scope.inboxAlertMessage = "Some issue in backend we couldnt receive your messages";
        }

        else if($scope.messageList.length == 0)
        {
            $scope.isAlertForInbox = true;
            $scope.inboxAlertMessage = "No Message Available";
        }    

        else {

        $scope.isAlertForInbox = false;

        // for single mssage start //

        /** for mcq msg **/
        for (var i = 0 ; i < $scope.messageList.length ; i++ )
        {
            var currentMessageId = $scope.messageList[i].messageId;
            $scope.messageList[i].anyAction = "yes";
            $scope.messageList[i].messageStatus = "open";
            $scope.routeCode = $scope.messageList[i].url;
            
            
            var poolURL =  appConfig.APP_HOST+"#!/pool/"+currentMessageId;

            var fbContentShare = "https://www.facebook.com/sharer/sharer.php?u="+escape(poolURL);
            $scope.messageList[0].FBPoolURL = fbContentShare; 

            var twitterContentShare = "https://twitter.com/home?status="+escape(poolURL);  
            $scope.messageList[0].twitterPoolURL = twitterContentShare;

            var googlePlusContentShare = "https://plus.google.com/share?url="+escape(poolURL); 
            $scope.messageList[0].googlePlusPollURL = googlePlusContentShare;

            var linkedinContentShare = "https://www.linkedin.com/shareArticle?mini=true&url="+poolURL+"&title="+$scope.messageList[i].messageSubject+"&summary="+$scope.messageList[i].messageBody+"&source=http://revleg.com";              
            $scope.messageList[0].linkedinPollURL = linkedinContentShare;
            
           
            // to handle the loggin and loggout to show commenting or not 
            $scope.messageList[0].isCommentingAllowed = !(authService.checkLogin());
            var dateAndTime = ($scope.messageList[i].createdOn).split(" ");
            var dateArray = dateAndTime[0].split("-");
            var dateString = dateArray[0]+dateArray[1]+dateArray[2];
            //$scope.messageList[i].date = dateAndTime[0];
            $scope.messageList[i].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
            $scope.messageList[i].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3);     

            if($scope.messageList[i].isClosed == "yes")
            {
                $scope.messageList[i].anyAction = "no";
                $scope.messageList[i].messageStatus = "closed";

                for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
                {
                    var currentComment = $scope.messageList[i].comment[c];
                    $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);

                    if(!authService.checkLogin())
                    {
                        //when not loggedin
                        $scope.messageList[i].anyAction = "yes";
                        $scope.messageList[i].comment[c].isLikeByUser = "no";
                    }

                }

            }
            else 
            {
                for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
                {
                    var currentComment = $scope.messageList[i].comment[c];
                    $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);

                    if(!authService.checkLogin())
                    {
                        //when not loggedin //
                        $scope.messageList[i].anyAction = "yes";
                        $scope.messageList[i].comment[c].isLikeByUser = "no";
                    }
                }
            }
            if($scope.messageList[i].suggestion == undefined)
            {

                $scope.messageList[i].suggestion = []; 
                $scope.messageList[i].suggestionLength = 0;
            } 
            else {

                var suggestionList = $scope.messageList[i].suggestion;
                for(var c = 0 ; c < suggestionList.length ; c++ )
                {
                    var dateAndTime = ($scope.messageList[i].suggestion[c].createdOn).split(" ");
                    var dateArray = dateAndTime[0].split("-");
                    var dateString = dateArray[0]+dateArray[1]+dateArray[2];
                    $scope.messageList[i].suggestion[c].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
                    $scope.messageList[i].suggestion[c].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3); 
                }
                $scope.messageList[i].suggestionLength = $scope.messageList[i].suggestion.length;
            }    

        }
            
            
        // re-routing using the url property of the message 
        if($scope.routeCode == 2)
        {
            //Route it to openQuestions
            $location.path('/openquestions/'+$routeParams.messageId);
        }
        
        if($scope.routeCode == 1)
        {
            // Route it to Inbox
            $location.path('/inbox/'+$routeParams.messageId);
            
        }
        
        if($scope.routeCode == 0)
        {
            //Route it to Outbox
            $location.path('/outbox/'+$routeParams.messageId);
        }    
        // end //    
            

        }


    })
    .error(function(){
        alert("some error");
    });
                
        
        
        
    }
    else {
    
    
    $rootScope.$on('event-signin-success',function(event,data){
          
        //calling the message url again .. this time with session
        /** need to work for a single msg incomplete **/
        var messageRecvUrl = appConfig.API_HOST + appConstants.SINGLE_MSG + $routeParams.messageId;
        messageFactory.recv(messageRecvUrl)
        .success(function(response) {

        var offset = 0;
        $scope.messageList = response.data.message;
        
         
        if(response.data.status == "fail")
        {
            $scope.isAlertForInbox = true;
            $scope.inboxAlertMessage = "Some issue in backend we couldnt receive your messages";
        }

        else if($scope.messageList.length == 0)
        {
            $scope.isAlertForInbox = true;
            $scope.inboxAlertMessage = "You have no message of this ID";
        }    

        else {

        $scope.isAlertForInbox = false;

        // for single mssage start //

        /** for mcq msg **/
        for (var i = 0 ; i < $scope.messageList.length ; i++ )
        {
            var currentMessageId = $scope.messageList[i].messageId;
            $scope.messageList[i].anyAction = "yes";
            $scope.messageList[i].messageStatus = "open";
            $scope.routeCode = $scope.messageList[i].url;
            
            
            var poolURL =  appConfig.APP_HOST+"#!/pool/"+currentMessageId;

            var fbContentShare = "https://www.facebook.com/sharer/sharer.php?u="+escape(poolURL);
            $scope.messageList[i].FBPoolURL = fbContentShare; 

            var twitterContentShare = "https://twitter.com/home?status="+escape(poolURL);  
            $scope.messageList[i].twitterPoolURL = twitterContentShare;

            var googlePlusContentShare = "https://plus.google.com/share?url="+escape(poolURL); 
            $scope.messageList[i].googlePlusPollURL = googlePlusContentShare;

            var linkedinContentShare = "https://www.linkedin.com/shareArticle?mini=true&url="+poolURL+"&title="+$scope.messageList[i].messageSubject+"&summary="+$scope.messageList[i].messageBody+"&source=http://revleg.com";              
            $scope.messageList[i].linkedinPollURL = linkedinContentShare;
           
            // to handle the loggin and loggout to show commenting or not 
            $scope.messageList[0].isCommentingAllowed = !(authService.checkLogin());
            var dateAndTime = ($scope.messageList[i].createdOn).split(" ");
            var dateArray = dateAndTime[0].split("-");
            var dateString = dateArray[0]+dateArray[1]+dateArray[2];
            //$scope.messageList[i].date = dateAndTime[0];
            $scope.messageList[i].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
            $scope.messageList[i].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3);     

            if($scope.messageList[i].isClosed == "yes")
            {
                $scope.messageList[i].anyAction = "no";
                $scope.messageList[i].messageStatus = "closed";

                for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
                {
                    var currentComment = $scope.messageList[i].comment[c];
                    $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);

                    if(!authService.checkLogin())
                    {
                        //when not loggedin
                        $scope.messageList[i].anyAction = "yes";
                        $scope.messageList[i].comment[c].isLikeByUser = "no";
                    }

                }

            }
            else 
            {
                for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
                {
                    var currentComment = $scope.messageList[i].comment[c];
                    $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);

                    if(!authService.checkLogin())
                    {
                        //when not loggedin //
                        $scope.messageList[i].anyAction = "yes";
                        $scope.messageList[i].comment[c].isLikeByUser = "no";
                    }
                }
            }
            if($scope.messageList[i].suggestion == undefined)
            {

                $scope.messageList[i].suggestion = []; 
                $scope.messageList[i].suggestionLength = 0;
            } 
            else {

                var suggestionList = $scope.messageList[i].suggestion;
                for(var c = 0 ; c < suggestionList.length ; c++ )
                {
                    var dateAndTime = ($scope.messageList[i].suggestion[c].createdOn).split(" ");
                    var dateArray = dateAndTime[0].split("-");
                    var dateString = dateArray[0]+dateArray[1]+dateArray[2];
                    $scope.messageList[i].suggestion[c].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
                    $scope.messageList[i].suggestion[c].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3); 
                }
                $scope.messageList[i].suggestionLength = $scope.messageList[i].suggestion.length;
            }    

        }
            
            
        // re-routing using the url property of the message 
        if($scope.routeCode == 2)
        {
            //Route it to openQuestions
            $location.path('/openquestions/'+$routeParams.messageId);
        }
        
        if($scope.routeCode == 1)
        {
            // Route it to Inbox
            $location.path('/inbox/'+$routeParams.messageId);
            
        }
        
        if($scope.routeCode == 0)
        {
            //Route it to Outbox
            $location.path('/outbox/'+$routeParams.messageId);
        }    
        // end //    
            

        }


    })
    .error(function(){
        alert("some error");
    });
          
    });
    
    
    
    /** need to work for a single msg incomplete **/
    var messageRecvUrl = appConfig.API_HOST + appConstants.SINGLE_MSG + $routeParams.messageId;
    messageFactory.recv(messageRecvUrl)
    .success(function(response) {

        var offset = 0;
        $scope.messageList = response.data.message;
        
         
        if(response.data.status == "fail")
        {
            $scope.isAlertForInbox = true;
            $scope.inboxAlertMessage = "Some issue in backend we couldnt receive your messages";
        }

        else if($scope.messageList.length == 0)
        {
            $scope.isAlertForInbox = true;
            $scope.inboxAlertMessage = "You have no message of this ID";
        }    

        else {

        $scope.isAlertForInbox = false;

        // for single mssage start //

        /** for mcq msg **/
        for (var i = 0 ; i < $scope.messageList.length ; i++ )
        {
            var currentMessageId = $scope.messageList[i].messageId;
            $scope.messageList[i].anyAction = "yes";
            $scope.messageList[i].messageStatus = "open";
            
            
            var poolURL =  appConfig.APP_HOST+"#!/pool/"+currentMessageId;

            var fbContentShare = "https://www.facebook.com/sharer/sharer.php?u="+escape(poolURL);
            $scope.messageList[i].FBPoolURL = fbContentShare; 

            var twitterContentShare = "https://twitter.com/home?status="+escape(poolURL);  
            $scope.messageList[i].twitterPoolURL = twitterContentShare;

            var googlePlusContentShare = "https://plus.google.com/share?url="+escape(poolURL); 
            $scope.messageList[i].googlePlusPollURL = googlePlusContentShare;

            var linkedinContentShare = "https://www.linkedin.com/shareArticle?mini=true&url="+poolURL+"&title="+$scope.messageList[i].messageSubject+"&summary="+$scope.messageList[i].messageBody+"&source=http://revleg.com";              
            $scope.messageList[i].linkedinPollURL = linkedinContentShare;
           
            // to handle the loggin and loggout to show commenting or not 
            $scope.messageList[0].isCommentingAllowed = !(authService.checkLogin());
            var dateAndTime = ($scope.messageList[i].createdOn).split(" ");
            var dateArray = dateAndTime[0].split("-");
            var dateString = dateArray[0]+dateArray[1]+dateArray[2];
            //$scope.messageList[i].date = dateAndTime[0];
            $scope.messageList[i].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
            $scope.messageList[i].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3);     

            if($scope.messageList[i].isClosed == "yes")
            {
                $scope.messageList[i].anyAction = "no";
                $scope.messageList[i].messageStatus = "closed";

                for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
                {
                    var currentComment = $scope.messageList[i].comment[c];
                    $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);

                    if(!authService.checkLogin())
                    {
                        //when not loggedin
                        $scope.messageList[i].anyAction = "yes";
                        $scope.messageList[i].comment[c].isLikeByUser = "no";
                    }

                }

            }
            else 
            {
                for(var c = 0 ; c < $scope.messageList[i].comment.length ; c++)
                {
                    var currentComment = $scope.messageList[i].comment[c];
                    $scope.messageList[i].comment[c].percentLike = $scope.messageList[i].comment[c].percentLike.toFixed(2);

                    if(!authService.checkLogin())
                    {
                        //when not loggedin //
                        $scope.messageList[i].anyAction = "yes";
                        $scope.messageList[i].comment[c].isLikeByUser = "no";
                    }
                }
            }
            if($scope.messageList[i].suggestion == undefined)
            {

                $scope.messageList[i].suggestion = []; 
                $scope.messageList[i].suggestionLength = 0;
            } 
            else {

                var suggestionList = $scope.messageList[i].suggestion;
                for(var c = 0 ; c < suggestionList.length ; c++ )
                {
                    var dateAndTime = ($scope.messageList[i].suggestion[c].createdOn).split(" ");
                    var dateArray = dateAndTime[0].split("-");
                    var dateString = dateArray[0]+dateArray[1]+dateArray[2];
                    $scope.messageList[i].suggestion[c].date = moment(dateString,"YYYYMMDD").format('MMMM Do');
                    $scope.messageList[i].suggestion[c].time = (dateAndTime[1]).substring(0,(dateAndTime[1]).length -3); 
                }
                $scope.messageList[i].suggestionLength = $scope.messageList[i].suggestion.length;
            }    

        }

        if(response.data.status == "fail")
        {
            $scope.isAlertForInbox = true;
            $scope.inboxAlertMessage = "Some issue in backend we couldnt recv your messages";
        }

        else if($scope.messageList.length == 0)
        {
            $scope.isAlertForInbox = true;
            $scope.inboxAlertMessage = "You have no messages.";
        }    

        else {

           $scope.isAlertForInbox = false;
        }        
      
        
            
            
        if(authService.checkLogin() == true)
        {
            // already logged in 
        }
        else {

            // not logged in 

        }    


        }


    })
    .error(function(){
        alert("some error");
    });
    
    }
    $scope.submitOpinion = function(messageId)
    {
        SweetAlert.swal("Oops...", "You need to login first", "error");
    }

}]);

_revlegApp.controller("phaseOneSanatizeController",[ "$scope" , "APIFactory" , "formFactory" , "$routeParams", "authService" , function($scope , APIFactory , formFactory , $routeParams , authService) {
    
    
    var getPrimaryDataUrl = appConfig.API_HOST + appConstants.ADMIN_GET_PRIMARY + "/" + $routeParams.type ;
       
    var type =  $routeParams.type;
    
    //var getPrimaryDataUrl= "http://demo4037057.mockable.io/getprimarydata";
    
     APIFactory.get(getPrimaryDataUrl)
        .success(function(response) {
            
            var unformattedData = response.data.data;
            var portionData = [];
            
            if(unformattedData.length > 200)
            {
                for(var k=0; k < 200 ; k++ )
                {
                    portionData[k] = unformattedData[k];
                }
                
                $scope.dataList = portionData;
            }
            else {
            
                $scope.dataList = unformattedData;
            }
            
            
        })
        .error(function(){
            alert("so");
        });    
    
    
     $scope.updateData = function(index){
            
        var formData = new FormData();

        formData.append('id',$scope.dataList[index].id);
        formData.append('value',$scope.dataList[index].value);
        formData.append('type',type);


        var primaryEditUrl = appConfig.API_HOST + appConstants.ADMIN_EDIT_PRIMARY + "/" + type ;
        formFactory.submit(formData, primaryEditUrl)
        .success(function(response) {


           if(response.status == 'success')
           {

                alert("success");
           }
           else  {

                alert("some error");
           }
        }).error(function(){

            alert("some error");

        });
         
     };
    
     $scope.deleteData = function(index){
         
         
        var formData = new FormData();

        formData.append('id',$scope.dataList[index].id);
        formData.append('type',type); 

        var primaryEditUrl = appConfig.API_HOST + appConstants.ADMIN_DELTE_PRIMARY + "/" + $routeParams.type ;
        formFactory.submit(formData, primaryEditUrl)
        .success(function(response) {


           if(response.status == 'success')
           {

               $scope.dataList.splice(index, 1);
               
           }
           else  {

                alert("some error");
           }
            
        }).error(function(){

            alert("some error");

        });
         
     };    
}]);


_revlegApp.controller("phaseTwoSanatizeController",[ "$scope" , "APIFactory" , "formFactory" ,  "$routeParams", "authService" , function($scope , APIFactory , formFactory ,  $routeParams , authService) {
    
    
    var getPrimaryDataUrl = appConfig.API_HOST + appConstants.ADMIN_GET_SYMBOLS + "/" + $routeParams.type + "/" + $routeParams.min + "/" + $routeParams.max;
    //var getPrimaryDataUrl= "http://demo4037057.mockable.io/getsecondarydata";
    
    var type =  $routeParams.type;
    
    var abbreviationMaker = function(input)
    {

        var r1 = input.replace(" for ",' ');
        var r2 = r1.replace(" of ",' ');
        var r3 = r2.replace(" and ",' ');
   
        input = r3;

        var words, acronym, nextWord;

        words = input.split(' ');
        acronym= "";
        index = 0

            while (index<words.length) {
            nextWord = words[index];
            acronym = acronym + nextWord.charAt(0);
            index = index + 1 ;
            }
        return acronym;
    }
    
     APIFactory.get(getPrimaryDataUrl)
        .success(function(response) {
            
            var unformattedData = response.data.data;
            var portionData = [];
            
            if(unformattedData.length > 200)
            {
                for(var k=0; k < 200 ; k++ )
                {
                    portionData[k] = unformattedData[k];
                }
                
                $scope.dataList = portionData;
            }
            else {
            
                $scope.dataList = unformattedData;
            }
            
            
            
            for(var i =0 ; i < $scope.dataList.length ; i++ )
            {
                $scope.dataList[i].input = abbreviationMaker($scope.dataList[i].primary);
                $scope.dataList[i].primaryInput = $scope.dataList[i].primary;
            }
            
            
        })
        .error(function(){
            alert("some error");
        });  
    
     
      $scope.deletePrimaryData = function(index){
         
         
        var formData = new FormData();

        formData.append('id',$scope.dataList[index].id);
        formData.append('type',type); 

        var primaryEditUrl = appConfig.API_HOST + appConstants.ADMIN_DELTE_PRIMARY + "/" + $routeParams.type ;
        formFactory.submit(formData, primaryEditUrl)
        .success(function(response) {


           if(response.status == 'success')
           {

               $scope.dataList.splice(index, 1);
               
           }
           else  {

                alert("some error");
           }
            
        }).error(function(){

            alert("some error");

        });
         
     };     


    $scope.editPrimaryData = function(index){
            
        var formData = new FormData();

        formData.append('id',$scope.dataList[index].id);
        formData.append('value',$scope.dataList[index].primaryInput);
        formData.append('type',type);


        var primaryEditUrl = appConfig.API_HOST + appConstants.ADMIN_EDIT_PRIMARY + "/" + type ;
        formFactory.submit(formData, primaryEditUrl)
        .success(function(response) {


           if(response.status == 'success')
           {

                alert("success");
           }
           else  {

                alert("some error");
           }
        }).error(function(){

            alert("some error");

        });
         
     }; 

    $scope.addSecondaryData = function(index){
            
        var formData = new FormData();

        formData.append('id',$scope.dataList[index].id);
        formData.append('value',$scope.dataList[index].input);
        formData.append('type',type);


        var primaryEditUrl = appConfig.API_HOST + appConstants.ADMIN_ADD_SYMBOLS+ "/" + type ;
        formFactory.submit(formData, primaryEditUrl)
        .success(function(response) {


           if(response.status == 'success')
           {

                alert("success");
           }
           else  {

                alert("some error");
           }
        }).error(function(){

            alert("some error");

        });
         
     };
    
     $scope.deleteSecondaryData = function(secondaryId,secondaryIndex,parentIndex){
         
       $scope.dataList[parentIndex].symbols.splice(secondaryIndex, 1);  
        var formData = new FormData();

            formData.append('id',secondaryId);
            formData.append('type',type); 

        var primaryEditUrl = appConfig.API_HOST + appConstants.ADMIN_DELETE_SYMBOLS + "/" + $routeParams.type ;
        formFactory.submit(formData, primaryEditUrl)
        .success(function(response) {


           if(response.status == 'success')
           {

               $scope.dataList[parentIndex].symbols.splice(secondaryIndex, 1);
               
           }
           else  {

                alert("some error");
           }
            
        }).error(function(){

            alert("some error");

        });
         
     }; 
    
}]);    

_revlegApp.controller('signupFlowController',["$scope" ,"$http", 'ipCookie', '$modal', '$routeParams' , "$location", "growl" , "getLocationList" , "$rootScope"  , "authService",function($scope,$http,ipCookie,$modal,$routeParams,$location,growl,getLocationList,$rootScope,authService){
    
    
    $scope.forAdmin = "no";
    
    
    $scope.step = 1;
    $scope.isSchoolViewVisible = false;
    $scope.isUndergradauteViewVisible = false;
    $scope.isLocationViewVisible = false;
    $scope.isPassionViewVisible = false;
    $scope.isCoachingViewVisible = false;
    $scope.ifSignupFlowIsVisible = true;
    
    $scope.college = {};
    
    $scope.viewChoosen = function(viewIndex)
    {
        $scope.step = 2;
        
        if(viewIndex == 2)
        {
        $scope.isUndergradauteViewVisible = true;
        }
        if(viewIndex == 1)
        {
        $scope.isSchoolViewVisible = true;
        }
    }
    
    $scope.goBack = function()
    {
        $scope.step = 1;
        $scope.isSchoolViewVisible = false;
        $scope.isUndergraduateViewVisible = false;
        $scope.isLocationViewVisible = false;
        $scope.isPassionViewVisible = false;
        $scope.isCoachingViewVisible = false;
        $scope.ifSignupFlowIsVisible = true;
        
    };
    
    
    if(!ipCookie('isProfileComplete'))
    {    
//        var modalInstance = $modal.open({
//        templateUrl: 'views/profileInfo.html',
//        controller: 'modalInfoController', 
//        });
    }
    else {
    
        $scope.ifSignupFlowIsVisible = false;
    }
    
    $scope.isAlertDisplay = false;

    if(!ipCookie('isProfileComplete'))
    {
         $scope.isAlertDisplay = true;
    }
    
    
    $scope.$watch(
        function(){ return  authService.isProfileComplete() },

        function(newVal,oldVal) {
          
            if(oldVal == 0 && newVal == 1)
            {
                $scope.step = 3;
            }
        }
       ); 
    
    
    var capitaliseFirstLetterForArray = function(inputArray)
        {

            for( var i = 0; i < inputArray.length ; i++)
            {
                var string = inputArray[i]; 
                if(string.indexOf(".") == -1 && string.length >3 ){
                    inputArray[i] =  string.charAt(0).toUpperCase() + string.slice(1);
                }
                else {
                    inputArray[i] =  string.toUpperCase();
                }            
            }

            return inputArray;
        }; 
    
        var capitaliseFirstLetter = function(string)
        {

            if(string.indexOf(".") == -1 && string.length >3 ){
                return string.charAt(0).toUpperCase() + string.slice(1);
            }
            else {
                return string.toUpperCase();
            }
        };  
    
        var ucFirstAllWords = function(string)
        {
            var pieces = string.split(" ");
            for ( var i = 0; i < pieces.length; i++ )
            {
                var j = pieces[i].charAt(0).toUpperCase();
                pieces[i] = j + pieces[i].substr(1);
            }
            return pieces.join(" ");
        }
        
         $scope.getSingleValueProfileSuggestions = function(val,field) {

        /** old way of calling **/   
//        var autocompleteURL = appConfig.API_HOST + appConstants.PROFILE_SUGGESTION + "/"+field + "/" + val;    
//        return $http.get(autocompleteURL, {
//        }).then(function(res){
//          return res.data.data.suggestion;
//        });
        
            
        if(field == 'college')
        {
            var autocompleteURL = appConfig.API_HOST + appConstants.GET_COLLEGE_SUGGESTIONS + "/" + val;    
        }
        
        if(field == 'school')
        {
            var autocompleteURL = appConfig.API_HOST + appConstants.GET_SCHOOL_SUGGESTIONS + "/" + val;    
        }   
            
        if(field == 'coaching')
        {
            var autocompleteURL = appConfig.API_HOST + appConstants.GET_COACHING_SUGGESTIONS + "/" + val;    
        }  
                        
        if(field == 'branch')
        {
            var autocompleteURL = appConfig.API_HOST + appConstants.GET_COLLEGE_BRANCH_SUGGESTIONS + "/" + val;    
        }       
            
        return $http.get(autocompleteURL, {
            }).then(function(res){
                if(res.data.data.suggestion.length > 7)
                {
                    return res.data.data.suggestion.slice(0,7);
                }
                else {

                    return res.data.data.suggestion;
                }
            });    
        };
    
    
        $scope.getSingleValueCitySuggestions = function(val) {

        var autocompleteURL = appConfig.API_HOST + appConstants.LOCATION_LIST + "/india" + "/other"+ "/" + val ;
        return $http.get(autocompleteURL, {
        }).then(function(res){
                if(res.data.data.suggestion.length > 7)
                {
                    return res.data.data.suggestion.slice(0,7);
                }
                else {

                    return res.data.data.suggestion;
                }
        });
        };
    
        $scope.getSingleValueArea3Suggestions = function(val) {

        var autocompleteURL = appConfig.API_HOST + appConstants.GET_AREA_SUGGESTION + "/karnataka/bengaluru"+ "/" + val ;
        return $http.get(autocompleteURL, {
        }).then(function(res){
                if(res.data.data.suggestion.length > 7)
                {
                    return res.data.data.suggestion.slice(0,7);
                }
                else {

                    return res.data.data.suggestion;
                }
        });
        };
        /** end **/   
    
        $scope.loadSuggestions = function(val,field) {

        return $http.get(appConfig.API_HOST + appConstants.PROFILE_SUGGESTION, {
              params: {
                 field: field,
                 input: val
              }
            }).then(function(res){
                if(res.data.data.suggestion.length > 7)
                {
                    return res.data.data.suggestion.slice(0,7);
                }
                else {

                    return res.data.data.suggestion;
                }
            });
         };
        
        /** end **/
    
        $scope.college = {};
        $scope.school = {};
    
        $scope.college.loader = true;
        $scope.college.form = true;
        $scope.college.messageDisplay =true;

        $scope.school.loader = true;
        $scope.school.form = true;
        $scope.school.messageDisplay =true;
    
    
         /** on year change **/
            $scope.onYearChange = function(newValue)
            {

                    $('#inputYear').val(newValue).change();
                    $('#collegeViewForm').bootstrapValidator('revalidateField', 'year');
            };
            $scope.onYearClick = function()
            {

                   if($('#inputYear').val() == null)
                    {

                    }
                    else {

                        $('#collegeViewForm').bootstrapValidator('revalidateField', 'year');
                    }
            };
            
            /** on year change **/
            $scope.onPreparingForChange = function(newValue)
            {

                    $('#inputPreparingFor').val(newValue).change();
                    $('#collegeViewForm').bootstrapValidator('revalidateField', 'preparingFor');
            };
            
            $scope.onPreparingForClick = function()
            {

                   if($('#inputPreparingFor').val() == null)
                    {

                    }
                    else {

                        $('#collegeViewForm').bootstrapValidator('revalidateField', 'preparingFor'); 
                    }
            };
            
            
            /** on class change **/
            $scope.onClassChange = function(newValue)
            {

                    $('#inputClass').val(newValue).change();
                    $('#schoolViewForm').bootstrapValidator('revalidateField', 'class');
            };
            $scope.onClassClick = function()
            {

                   if($('#inputCLass').val() == null)
                    {

                    }
                    else {

                        $('#schoolViewForm').bootstrapValidator('revalidateField', 'class'); 
                    }
            };
            
            /** on year change **/
            $scope.onSchoolPreparingForChange = function(newValue)
            {

                    $('#inputSchoolPreparingFor').val(newValue).change();
                    $('#schoolViewForm').bootstrapValidator('revalidateField', 'schoolPreparingFor');
            };
            
            $scope.onSchoolPreparingForClick = function()
            {

                   if($('#inputSchoolPreparingFor').val() == null)
                    {

                    }
                    else {

                        $('#schoolViewForm').bootstrapValidator('revalidateField', 'schoolPreparingFor'); 
                    }
            };
    
    
            $scope.class = '12th';
    
            $scope.year = '';
            $scope.preparingFor = '';
            $scope.collegeCity = '';
            $scope.schoolPreparingFor = '';
            
            $scope.schoolCity = 'Bengaluru';

            /** especially for admin **/
            $scope.years = [
                {name:'1st', value:'1st'},
                {name:'2nd', value:'2nd'},
                {name:'3rd', value:'3rd'},
                {name:'4th', value:'4th'},
                {name:'5th', value:'5th'},
                {name:'6th', value:'6th'}   
            ];

           $scope.preparingForList = [
                {name:'Masters', value:'Masters'},
                {name:'Public Services', value:'Public Services'},
                {name:'Military', value:'Military'},
                {name:'entrepreneurship', value:'entrepreneurship'},
                {name:'Double Graduation', value:'Double Graduation'},
                {name:'Short Term Courses', value:'Short Term Courses'},
                {name:'other', value:'other'}
            ];

            $scope.classes = [
                {name:'12th', value:'12th'},
            ];

            $scope.schoolPreparingForList = [
                {name:'Engineering', value:'Engineering'},
                {name:'Pure Science', value:'Pure Science'},
                {name:'Research', value:'Research'},
                {name:'Medical', value:'Medical'},
                {name:'Marine', value:'Marine'},
                {name:'Defense', value:'Defense'},
                {name:'Fashion & Design', value:'Fashion & Design'},
                {name:'Humanities & Social Science', value:'Humanities & Social Science'},
                {name:'Law ', value:'Law '},
                {name:'University Admission', value:'University Admission'},
                {name:'other', value:'other'}
            ];
         
}]);
