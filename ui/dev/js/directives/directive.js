_revlegApp.directive("backgroundImageWidget", ["authService", "$rootScope", "$location",
    function (authService , $rootScope , location) {
        return {
            
           restrict: 'C',
           link : function(scope, element, attributes){
                              
               
                  $rootScope.$on('removeImage',function(event,data){
                     
                    element.removeClass('addImage');
                                 
                  });
               
                 $rootScope.$on('addImage',function(event,data){
                     
                    element.addClass('addImage');
                                 
                  });
               
               
            }
        }   
}]);



//_revlegApp.directive("wrapperheightWidget", ["authService", "$rootScope", "$location",
//    function (authService , $rootScope , location) {
//        return {
//            
//           restrict: 'C',
//           link : function(scope, element, attributes){
//                              
//               
//                  $rootScope.$on('addExtraHeight',function(event,data){
//                     
//                    element.addClass('extraHeight');
//                                 
//                  });
//               
//                 $rootScope.$on('removeExtraHeight',function(event,data){
//                     
//                    element.removeClass('extraHeight');
//                                 
//                  });
//               
//               
//            }
//        }   
//}]);


_revlegApp.directive("menuhideWidget", ["authService", "$rootScope", "$location",
    function (authService , $rootScope , location) {
        return {
            
           restrict: 'AEC',
           link : function(scope, element, attributes){
                              
               if(!authService.checkLogin())
               {
                   element.addClass('hideBeforeLogin');
               }
               
               scope.$watch(
                function(){ return  authService.checkLogin() },

                function(newVal) {
                  if(newVal == true){
                      
                      element.removeClass('hideBeforeLogin');
                  }
                  else {
                     
                      element.addClass('hideBeforeLogin');
                  }
                  
                }
               );                 
               
            }
        }   
}]);

_revlegApp.directive("signinFormValidator",["formFactory","$location","$rootScope","sessionService", "intendedURLService","ipCookie","$route", function(formFactory,$location,$rootScope,sessionService,intendedURLService,ipCookie,$route){
    return {

        scope : false,
        transclude : false,
        link : function( scope, element , attrs) {

            scope.loader = "true";
            scope.form = "true";
            scope.messageDisplay = "true";

            var submitLoginForm = function() {
            
            // if client side vaidations
                
            var formData = new FormData();
            formData.append('emailId',scope.emailId);
            formData.append('password',scope.password);
            var interestSubmitUrl = appConfig.API_HOST + appConstants.LOGIN;
            formFactory.submit(formData, interestSubmitUrl)
                .success(function(response) {
                    scope.loader = "true";

                   if(response.status == 'success')
                   {
                        /** to be changed from username to displayname**/
                        sessionService.create(response.data.userData.basicInfo);
                        scope.loader = "true";
                        scope.$parent.isForm = "true";
                        $rootScope.$broadcast('event-signin-success');
                        if(ipCookie('isProfileComplete') == false)
                        {
                            $location.path("/profile");
                        }
                        
                        // identifying which url should be routed after login //
                        if($route.routes[$location.path()].needToBeRoutedAfterLogin)
                        {
                            $location.path("/search");
                        }
                        
                        
                   }
                   else  {

                        scope.loader = "true";
                        scope.messageDisplay = false;
                        scope.form = "true";
                        scope.loader = "true"; 
                        scope.message = response.data.message;
                   }
                }).error(function(){

                    scope.loader = "true";
                    scope.messageDisplay = false;
                    scope.form = "true"; 
                    scope.message = " The password you entered is incorrect. Please try again.";

                });    
                
            };

            $(element[0]).bootstrapValidator({
                submitButtons: 'button[type="submit"]',
                fields: {
//                    emailId: {
//                        validators: {
//                            notEmpty: {
//                                message: 'Please Provide EmailId'
//                            }
//
//                        }
//                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide password'
                            }

                        }
                    }
                }

            })
            .on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            submitLoginForm();
           
        });


        }
    }

}]);

_revlegApp.directive("signupFormValidator",["formFactory","sessionService","$rootScope","$location",                                     "$timeout","sessionService",function(formFactory,sessionService,$rootScope,$location,$timeout,sessionService){

	return {

        scope : false,
        transclude : false, 
		link : function(scope , element , attrs){

			var submitSignupForm = function() {
				
				var formData = new FormData();
                formData.append('fullName',scope.fullName);
				formData.append('emailId',scope.emailId);
				formData.append('password',scope.password);
                formData.append('userType',"3");
                formData.append('referCode',$rootScope.ref_refCode);
                formData.append('referedBy',$rootScope.ref_userId);

                var profileSubmitUrl = appConfig.API_HOST+appConstants.SIGNUP;
                
                $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: 'white', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: 1, 
                    color: '#fff' 
                } });  
                
                $('.blockMsg').html("<h1 style='color:green;'>Please wait</h1>");
                
                formFactory.submit(formData, profileSubmitUrl)
                	.success(function(response) {

                        if(response.status == "success")
                        {
                            
                            
                            var messageDisplayforVerification = "<h2 style='color:green;'>A verification link <br><br> has been sent to your emailId <br><br>"+scope.emailId+"</h2>";
                            $('.blockMsg').html(messageDisplayforVerification);
                           
                            $timeout(function(){
                                //$modalInstance.dismiss('cancel');
                                $.unblockUI();
                                scope.$parent.isForm = "true";
                            },4500); 
                            
                            
                            /** for refer laoding page **/
                            if($rootScope.ref_refCode != "")
                            {
                                $location.path('/search');
                            }
                            
                        }
                        else{

                            scope.$parent.isMessageDisplay = false;
                            scope.message = response.data.message;
                            $.unblockUI();

                        }

                	}).error(function(data, status, headers, config){

                        scope.$parent.isMessageDisplay = false;
                        scope.message = data.data.message;
                        $.unblockUI();

                    });

			};

			$(element[0])
            .bootstrapValidator({
				submitButtons: 'button[type="submit"]',
                fields: {
                    fullName: {
                       
                        validators: {
                            regexp: {
                                regexp: /^[a-zA-Z ]*$/,
                                message: 'The full name can consist of alphabetical characters and spaces only'
                            },
                            stringLength: {
                                max: 50,
                                message: 'The full name must be less than 50 characters'
                            },
                            
                            notEmpty: {
                                message: 'Please Provide Full name'
                            }
                        }
                    },
                    emailId: {
                     
                        validators: {
                            notEmpty: {
                                message: 'Please Provide EmailId'
                            },
                            remote: {
                                url: appConfig.API_HOST + appConstants.CHECK_EMAIL,
                                message: 'This emailId already exist'
                            }
                        }
                        
                    }
                    ,
                    password: {
                     
                        validators: {
                            notEmpty: {
                                message: 'Please Provide Password'
                            },
                            identical: {
                                field: 'confirmPassword',
                                message: 'The password and its confirm are not the same'
                            }

                        }
                    }
                    ,
                    confirmPassword: {
                     
                        validators: {
                            notEmpty: {
                                message: 'Please Confirm Password'
                            },
                            identical: {
                                field: 'password',
                                message: 'The password and its confirm are not the same'
                            }

                        }
                    }
                }

            })
			.on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();
            submitSignupForm();
           
        });

		}
	}
}]);


_revlegApp.directive("coachingFormValidator",["formFactory", "authService" , "blockUI" , "$timeout" , "growl" , "$rootScope" , function(formFactory,authService,blockUI,$timeout,growl,$rootScope ){
	return {

		link : function( scope, element , attrs) {   
            
            scope.resetView = function()
            {
                /** specefic for every template **/
                var templateId = '6';
                var formData = new FormData();
                
                if(scope.forAdmin == "no") 
                {
                    var resetFormUrl =  appConfig.API_HOST + appConstants.DELETE_VIEW + "/6";
                }
                else {
                    
                    var resetFormUrl =  appConfig.API_HOST + appConstants.ADMIN_DELETE_VIEW + "/6";
                    formData.append('userId',scope.userIdForEdit);   
                }
                
                
//                var resetFormUrl = appConfig.API_HOST + appConstants.DELETE_VIEW + "/3";
                
                /** specefic for each template **/
                var coachingBlockUI = blockUI.instances.get('coachingForm');
                coachingBlockUI.start();
    
                formFactory.submit(formData, resetFormUrl)
                	.success(function(response) {
                        
                        if(response.status == "success")
                        {
                            
                            coachingBlockUI.message("Successfully Deleted");

                            /** if successfull clean everything **/
                            scope.coachingName="";
                            scope.coachingArea="";
                            scope.examList="";
                            $('#examList').tagsinput('removeAll');

                            /** Reseting the form **/
                            $(element[0]).bootstrapValidator('resetForm',true);
                            $timeout(function() { 
                              coachingBlockUI.stop();
                              scope.coaching.switchStatus = false;
                              scope.coaching.isComplete = false; 
                            }, 1500);  
                            
                        }
                        else {
                        
                            coachingBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              coachingBlockUI.stop();
                            }, 1500);  
                        
                        }
                        
                    })
                    .error(function(){
                    
                            coachingBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              coachingBlockUI.stop();
                            }, 1500);  
                    
                    });
                
                
            }
            
            
            
            
			var submitCoachingForm = function() {
                

				var formData = new FormData();
				formData.append('coaching',scope.coachingName);
                formData.append('area',scope.coachingArea);
                formData.append('city',scope.coachingCity);
                formData.append('preparingFor',scope.examList);
                
                if(scope.forAdmin == "no") 
                {
                    var coachingSubmitUrl =  appConfig.API_HOST +appConstants.ACCEPT_TEMPLATES + "/6";
                }
                else {
                    
                    var coachingSubmitUrl =  appConfig.API_HOST +appConstants.ADMIN_ACCEPT_TEMPLATES + "/6";
                    formData.append('userId',scope.userIdForEdit);   
                }
                
                                    
                var coachingBlockUI = blockUI.instances.get('coachingForm');
                coachingBlockUI.start();
                
                formFactory.submit(formData, coachingSubmitUrl)
                	.success(function(response) {
                        if(response.status == "success")
                        {
                            coachingBlockUI.message("Successfully Submitted");
                            
                            
                            $timeout(function() { 
                              coachingBlockUI.stop();
                              scope.coaching.switchStatus = true;
                                
//                             if(scope.college.isComplete == false) 
//                             {  
//                                growl.addSuccessMessage('<b>Congrats: &nbsp;</b> Your Basic Profile is complete  <br> Now you can navigate !!!');
//                             }     
                                
                              scope.coaching.isComplete = true; 
                              $rootScope.viewIsCompleteCount+=1;    
                            }, 1500);  
                            
//                            scope.college.messageDisplay = false;
//                            scope.college.form = "false";
//                            scope.college.loader = "true";
//                            scope.college.message = "Your College View has been submitted successfully";
                            
                            
//                             authService.profileCompleted();
                        }
                        else{

                            
                            coachingBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              schoolBlockUI.stop();
                            }, 1500);  
                        
//                            scope.college.loader = "true";
//                            scope.college.messageDisplay = false;
//                            scope.college.form = "false"; 
//                            scope.college.message = response.data.message;

                        }
                    }).error(function(){

                    
                    coachingBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              coachingBlockUI.stop();
                            }, 1500);  
                        
//                        scope.college.loader = "true";
//                        scope.college.messageDisplay = false;
//                        scope.college.form = "false"; 
//                        scope.college.message = "Some issue in backend , please refresh and retry";

                    });


			};

            $(element[0])
            .find('[name="examList"]')
                .change(function (e) {
                    $(element[0]).bootstrapValidator('revalidateField', 'examList');    
                })
                .end()
            .bootstrapValidator({
                excluded: ':disabled',
			    submitButtons: 'button[type="submit"]',
                fields: {
                    coachingName: {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide coaching Name'
                            }

                        }
                    },
                    coachingCity : {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide City'
                            }

                        }
                    },
                    coachingArea  : {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide Area'
                            }

                        }
                    },
                    examList : {
                        validators: {
                            notEmpty: {
                                message: 'Please provide what you preparing For'
                            }

                        }
                    }
                    
                }

            })
			.on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            submitCoachingForm();
           
        });


		}
	}

}]);

_revlegApp.directive("schoolFormValidator",["formFactory", "authService" , "blockUI" , "$timeout" , "growl" , "$rootScope" , function(formFactory,authService,blockUI,$timeout,growl,$rootScope ){
	return {

		link : function( scope, element , attrs) {   
            
            scope.resetView = function()
            {
                /** specefic for every template **/
                var templateId = '2';
                var formData = new FormData();
                
                if(scope.forAdmin == "no") 
                {
                    var resetFormUrl =  appConfig.API_HOST + appConstants.DELETE_VIEW + "/2";
                }
                else {
                    
                    var resetFormUrl =  appConfig.API_HOST + appConstants.ADMIN_DELETE_VIEW + "/2";
                    formData.append('userId',scope.userIdForEdit);   
                }
                
                
//                var resetFormUrl = appConfig.API_HOST + appConstants.DELETE_VIEW + "/3";
                
                /** specefic for each template **/
                var schoolBlockUI = blockUI.instances.get('schoolForm');
                schoolBlockUI.start();
    
                formFactory.submit(formData, resetFormUrl)
                	.success(function(response) {
                        
                        if(response.status == "success")
                        {
                            
                            schoolBlockUI.message("Successfully Deleted");

                            /** if successfull clean everything **/
                            scope.schoolName = "";
                            scope.schoolArea="";
                            scope.schoolPreparingFor="";

                            /** Reseting the form **/
                            $(element[0]).bootstrapValidator('resetForm',true);
                            $timeout(function() { 
                              schoolBlockUI.stop();
                              scope.school.switchStatus = false;
                              scope.school.isComplete = false; 
                            }, 1500);  
                            
                        }
                        else {
                        
                            schoolBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              schoolBlockUI.stop();
                            }, 1500);  
                        
                        }
                        
                    })
                    .error(function(){
                    
                            schoolBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              schoolBlockUI.stop();
                            }, 1500);  
                    
                    });
                
                
            }
            
            
            
            
			var submitSchoolForm = function() {
                
                
               
                
				var formData = new FormData();
				formData.append('school',scope.schoolName);
				formData.append('class',scope.class);
                formData.append('area',scope.schoolArea);
                formData.append('city',scope.schoolCity);
                
                
                
                if(scope.schoolPreparingFor != 'other')
                {
                   formData.append('preparingFor',scope.schoolPreparingFor);  
                }
                else{
                        formData.append('preparingFor',"");
                }
                
                if(scope.forAdmin == "no") 
                {
                    var schoolSubmitUrl =  appConfig.API_HOST +appConstants.ACCEPT_TEMPLATES + "/2";
                }
                else {
                    
                    var schoolSubmitUrl =  appConfig.API_HOST +appConstants.ADMIN_ACCEPT_TEMPLATES + "/2";
                    formData.append('userId',scope.userIdForEdit);   
                }
                
                
                
                                    
                var schoolBlockUI = blockUI.instances.get('schoolForm');
                schoolBlockUI.start();
                
                   
                
                formFactory.submit(formData, schoolSubmitUrl)
                	.success(function(response) {
                        if(response.status == "success")
                        {
                            schoolBlockUI.message("Successfully Submitted");
                            
                            
                            $timeout(function() { 
                              schoolBlockUI.stop();
                              scope.school.switchStatus = true;
                                
//                             if(scope.college.isComplete == false) 
//                             {  
//                                growl.addSuccessMessage('<b>Congrats: &nbsp;</b> Your Basic Profile is complete  <br> Now you can navigate !!!');
//                             }     
                                
                              scope.school.isComplete = true; 
                              $rootScope.viewIsCompleteCount+=1;    
                                
                              if(!authService.isProfileComplete())
                               {
                                   authService.profileCompleted();
                               }     
                                
                            }, 1500);  
                            
//                            scope.college.messageDisplay = false;
//                            scope.college.form = "false";
//                            scope.college.loader = "true";
//                            scope.college.message = "Your College View has been submitted successfully";
                            
                            
//                             authService.profileCompleted();
                        }
                        else{

                            
                            schoolBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              schoolBlockUI.stop();
                            }, 1500);  
                        
//                            scope.college.loader = "true";
//                            scope.college.messageDisplay = false;
//                            scope.college.form = "false"; 
//                            scope.college.message = response.data.message;

                        }
                    }).error(function(){

                    
                    schoolBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              schoolBlockUI.stop();
                            }, 1500);  
                        
//                        scope.college.loader = "true";
//                        scope.college.messageDisplay = false;
//                        scope.college.form = "false"; 
//                        scope.college.message = "Some issue in backend , please refresh and retry";

                    });


			};

			$(element[0]).bootstrapValidator({
			    submitButtons: 'button[type="submit"]',
                fields: {
                    schoolName: {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide school Name'
                            }

                        }
                    },
                    schoolClass  : {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide class'
                            }

                        }
                    },
                    schoolCity : {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide City'
                            }

                        }
                    },
                    schoolArea  : {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide Area'
                            }

                        }
                    },
                    schoolPreparingFor  : {
                        validators: {
                            notEmpty: {
                                message: 'Please provide what you preparing For'
                            }

                        }
                    }
                    
                }

            })
			.on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            submitSchoolForm();
           
        });


		}
	}

}]);



_revlegApp.directive("collegeFormValidator",["formFactory", "authService" , "blockUI" , "$timeout" , "growl" , "$rootScope" , function(formFactory,authService,blockUI,$timeout,growl,$rootScope ){
	return {

		link : function( scope, element , attrs) {   
            
            scope.resetView = function()
            {
                /** specefic for every template **/
                var templateId = '3';
                var formData = new FormData();
                
                if(scope.forAdmin == "no") 
                {
                    var resetFormUrl =  appConfig.API_HOST + appConstants.DELETE_VIEW + "/3";
                }
                else {
                    
                    var resetFormUrl =  appConfig.API_HOST + appConstants.ADMIN_DELETE_VIEW + "/3";
                    formData.append('userId',scope.userIdForEdit);   
                }
                
                
//                var resetFormUrl = appConfig.API_HOST + appConstants.DELETE_VIEW + "/3";
                
                /** specefic for each template **/
                var collegeBlockUI = blockUI.instances.get('collegeForm');
                collegeBlockUI.start();
    
                formFactory.submit(formData, resetFormUrl)
                	.success(function(response) {
                        
                        if(response.status == "success")
                        {
                            
                            collegeBlockUI.message("Successfully Deleted");

                            /** if successfull clean everything **/
                            scope.collegeName="";
                            scope.branch="";
                            scope.year="";
                            scope.collegeCity="";
                            scope.preparingFor="";

                            /** Reseting the form **/
                            $(element[0]).bootstrapValidator('resetForm',true);
                            $timeout(function() { 
                              collegeBlockUI.stop();
                              scope.college.switchStatus = false;
                              scope.college.isComplete = false; 
                            }, 1500);  
                            
                        }
                        else {
                        
                            collegeBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              collegeBlockUI.stop();
                            }, 1500);  
                        
                        }
                        
                    })
                    .error(function(){
                    
                            collegeBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              collegeBlockUI.stop();
                            }, 1500);  
                    
                    });
                
                
            }
            
            
            
            
			var submitCollegeForm = function() {

//				scope.college.loader= false;
//				scope.college.form = false;

                if(scope.year == null)
                {
                    scope.year = "";
                }
                    
                if(scope.collegeCity == null)
                {
                    scope.collegeCity = "";
                }
                    
                if(scope.preparingFor == null)
                {
                    scope.preparingFor = "";
                }
                
                if(scope.preparingFor == 'other')
                {
                    scope.preparingFor = "";
                }

				var formData = new FormData();
				formData.append('college',scope.collegeName);
				formData.append('branch',scope.branch);
                formData.append('year',scope.year);
                formData.append('city',scope.collegeCity);
                formData.append('preparingFor',scope.preparingFor);
                
                if(scope.forAdmin == "no") 
                {
                    var collegeSubmitUrl =  appConfig.API_HOST +appConstants.ACCEPT_TEMPLATES + "/3";
                }
                else {
                    
                    var collegeSubmitUrl =  appConfig.API_HOST +appConstants.ADMIN_ACCEPT_TEMPLATES + "/3";
                    formData.append('userId',scope.userIdForEdit);   
                }
                
                                    
                var collegeBlockUI = blockUI.instances.get('collegeForm');
                collegeBlockUI.start();
                
                            
                formFactory.submit(formData, collegeSubmitUrl)
                	.success(function(response) {
                        if(response.status == "success")
                        {
                            collegeBlockUI.message("Successfully Submitted");
                            
                            
                            $timeout(function() { 
                              collegeBlockUI.stop();
                              scope.college.switchStatus = true;
                                
//                             if(scope.college.isComplete == false) 
//                             {  
//                                growl.addSuccessMessage('<b>Congrats: &nbsp;</b> Your Basic Profile is complete  <br> Now you can navigate !!!');
//                             }     
                                
                            if(!authService.isProfileComplete())
                               {
                                   authService.profileCompleted();
                               }     
                                
                              scope.college.isComplete = true; 
                              $rootScope.viewIsCompleteCount+=1;    
                            }, 1500);  
                            
//                            scope.college.messageDisplay = false;
//                            scope.college.form = "false";
//                            scope.college.loader = "true";
//                            scope.college.message = "Your College View has been submitted successfully";
                            
                            
//                             authService.profileCompleted();
                        }
                        else{

                            
                            collegeBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              collegeBlockUI.stop();
                            }, 1500);  
//                            scope.college.loader = "true";
//                            scope.college.messageDisplay = false;
//                            scope.college.form = "false"; 
//                            scope.college.message = response.data.message;

                        }
                    }).error(function(){

                    
                            collegeBlockUI.message("Some Error ocurred");
                            collegeBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              collegeBlockUI.stop();
                            }, 1500);  
//                        scope.college.loader = "true";
//                        scope.college.messageDisplay = false;
//                        scope.college.form = "false"; 
//                        scope.college.message = "Some issue in backend , please refresh and retry";

                    });


			};

			$(element[0]).bootstrapValidator({
			    submitButtons: 'button[type="submit"]',
                fields: {
                    collegeName: {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide College Name'
                            }

                        }
                    }
                   ,
                   branch: {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide branch'
                            }

                        }
                    },
                   year: {
                        validators: {
                            notEmpty: {
                                message: 'Please Choose year'
                            }

                        }
                    },
                   preparingFor: {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide what you are preparing for'
                            }

                        }
                    },
                    collegeCity: {
                        validators: {
                            notEmpty: {
                                message: 'Please Choose city'
                            }

                        }
                    },
                }

            })
			.on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            submitCollegeForm();
           
        });


		}
	}

}]);

_revlegApp.directive("collegeAdminFormValidator",["formFactory", "authService" , "blockUI" , "$timeout" , "growl" , "$rootScope" , function(formFactory,authService,blockUI,$timeout,growl,$rootScope ){
	return {

		link : function( scope, element , attrs) {   
            
            scope.resetView = function()
            {
                /** specefic for every template **/
                var templateId = '3';
                var formData = new FormData();
                
                if(scope.forAdmin == "no") 
                {
                    var resetFormUrl =  appConfig.API_HOST + appConstants.DELETE_VIEW + "/3";
                }
                else {
                    
                    var resetFormUrl =  appConfig.API_HOST + appConstants.ADMIN_DELETE_VIEW + "/3";
                    formData.append('userId',scope.userIdForEdit);   
                }
                
                
//                var resetFormUrl = appConfig.API_HOST + appConstants.DELETE_VIEW + "/3";
                
                /** specefic for each template **/
                var collegeBlockUI = blockUI.instances.get('collegeForm');
                collegeBlockUI.start();
    
                formFactory.submit(formData, resetFormUrl)
                	.success(function(response) {
                        
                        if(response.status == "success")
                        {
                            
                            collegeBlockUI.message("Successfully Deleted");

                            /** if successfull clean everything **/
                            scope.collegeName="";
                            scope.branch="";
                            scope.year="";
                            scope.collegeCity="";
                            scope.preparingFor="";

                            /** Reseting the form **/
                            $(element[0]).bootstrapValidator('resetForm',true);
                            $timeout(function() { 
                              collegeBlockUI.stop();
                              scope.college.switchStatus = false;
//                              scope.college.isComplete = false; 
                            }, 1500);  
                            
                        }
                        else {
                        
                            collegeBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              collegeBlockUI.stop();
                            }, 1500);  
                        
                        }
                        
                    })
                    .error(function(){
                    
                            collegeBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              collegeBlockUI.stop();
                            }, 1500);  
                    
                    });
                
                
            }
            
            
            
            
			var submitCollegeForm = function() {

//				scope.college.loader= false;
//				scope.college.form = false;

                if(scope.year == null)
                {
                    scope.year = "";
                }
                    
                if(scope.collegeCity == null)
                {
                    scope.collegeCity = "";
                }
                    
                if(scope.preparingFor == null)
                {
                    scope.preparingFor = "";
                }
                
                if(scope.preparingFor == 'other')
                {
                    scope.preparingFor = "";
                }
				var formData = new FormData();
				formData.append('college',scope.collegeName);
				formData.append('branch',scope.branch);
                formData.append('year',scope.year);
                formData.append('city',scope.collegeCity);
                formData.append('preparingFor',scope.preparingFor);
                
                
                
                if(scope.collegeCity == '')
                {
                   alert("city is not filled , please fill it ") ;
                }
                else 
                {
                if(scope.forAdmin == "no") 
                {
                    var collegeSubmitUrl =  appConfig.API_HOST +appConstants.ACCEPT_TEMPLATES + "/3";
                }
                else {
                    
                    var collegeSubmitUrl =  appConfig.API_HOST +appConstants.ADMIN_ACCEPT_TEMPLATES + "/3";
                    formData.append('userId',scope.userIdForEdit);   
                }
                
                                    
                var collegeBlockUI = blockUI.instances.get('collegeForm');
                collegeBlockUI.start();
                
                formFactory.submit(formData, collegeSubmitUrl)
                	.success(function(response) {
                        if(response.status == "success")
                        {
                            collegeBlockUI.message("Successfully Submitted");
                            
                            
                            $timeout(function() { 
                              collegeBlockUI.stop();
                              scope.college.switchStatus = true;
                                
//                             if(scope.college.isComplete == false) 
//                             {  
//                                growl.addSuccessMessage('<b>Congrats: &nbsp;</b> Your Basic Profile is complete  <br> Now you can navigate !!!');
//                             }     
                                
//                              scope.college.isComplete = true; 
//                              $rootScope.viewIsCompleteCount+=1;    
                                
                              if(!authService.isProfileComplete())
                               {
                                   authService.profileCompleted();
                               }      
                                
                            }, 1500);  
                            
//                            scope.college.messageDisplay = false;
//                            scope.college.form = "false";
//                            scope.college.loader = "true";
//                            scope.college.message = "Your College View has been submitted successfully";
                            
                            
//                             authService.profileCompleted();
                        }
                        else{

//                            scope.college.loader = "true";
//                            scope.college.messageDisplay = false;
//                            scope.college.form = "false"; 
//                            scope.college.message = response.data.message;

                        }
                    }).error(function(){

//                        scope.college.loader = "true";
//                        scope.college.messageDisplay = false;
//                        scope.college.form = "false"; 
//                        scope.college.message = "Some issue in backend , please refresh and retry";

                    });
                }
                
                




			};

			$(element[0]).bootstrapValidator({
			    submitButtons: 'button[type="submit"]',
                fields: {
                    collegeName: {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide College Name'
                            }

                        }
                    }
                   ,
                   branch: {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide branch'
                            }

                        }
                    }
                }

            })
			.on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            submitCollegeForm();
           
        });


		}
	}

}]);

_revlegApp.directive("companyFormValidator",["formFactory", "authService" , "blockUI" , "$timeout" , "growl" , "$rootScope" , function(formFactory,authService,blockUI,$timeout,growl,$rootScope){
	return {

		link : function( scope, element , attrs) {

            
            scope.resetView = function()
            {
                /** specefic for every template **/
                var templateId = '4';
                var formData = new FormData();      
                
                if(scope.forAdmin == "no") 
                {
                    var resetFormUrl =  appConfig.API_HOST + appConstants.DELETE_VIEW + "/4";
                }
                else {
                    
                    var resetFormUrl =  appConfig.API_HOST + appConstants.ADMIN_DELETE_VIEW + "/4";
                    formData.append('userId',scope.userIdForEdit);   
                }
                
                //var resetFormUrl = appConfig.API_HOST + appConstants.DELETE_VIEW + "/4";
                
                /** specefic for each template **/
                var companyBlockUI = blockUI.instances.get('companyForm');
                companyBlockUI.start();         
                
                formFactory.submit(formData, resetFormUrl)
                	.success(function(response) {
                        
                        if(response.status == "success")
                        {
                            
                            companyBlockUI.message("Successfully Deleted");

                            /** if successfull clean everything **/
                            $('#projectList').tagsinput('removeAll');
                            scope.companyName = "";
                            scope.position = "";
                            $('#technicalSkillList').tagsinput('removeAll');


                            /** Reseting the form **/
                            $(element[0]).bootstrapValidator('resetForm',true);
                            $timeout(function() { 
                              companyBlockUI.stop();
                              scope.company.switchStatus = false;
                              //scope.company.isComplete = false; 
                            }, 1500);  
                            
                        }
                        else {
                        
                            companyBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              companyBlockUI.stop();
                            }, 1500); 
                            
                        }
                        
                    })
                    .error(function(){
                    
                        companyBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              companyBlockUI.stop();
                            }, 1500); 
                    
                    });
                
                
            }
            
			var submitCompanyForm = function() {

//				scope.company.loader= false;
//				scope.company.form = false;

				var formData = new FormData();
				formData.append('company',scope.companyName);
				formData.append('position',scope.position);
                
                /** for multi value with tags**/
//				formData.append('projectList',scope.projectList);
                formData.append('projectList',$('#projectList').val());
                
                
				formData.append('skillSet',$('#technicalSkillList').val());
                
                
                if(scope.forAdmin == "no") 
                {
                    var companySubmitUrl = appConfig.API_HOST+appConstants.ACCEPT_TEMPLATES + "/4";
                }
                else {
                    var companySubmitUrl = appConfig.API_HOST+appConstants.ADMIN_ACCEPT_TEMPLATES + "/4" ;
                    formData.append('userId',scope.userIdForEdit);   
                }
                
                
                var companyBlockUI = blockUI.instances.get('companyForm');
                companyBlockUI.start();
                
                formFactory.submit(formData, companySubmitUrl)
                	.success(function(response) {
                        
                        if(response.status == "success")
                        {
                            
                            
                            companyBlockUI.message("Successfully Submitted");
                            
                            $timeout(function() { 
                              companyBlockUI.stop();
                              scope.company.switchStatus = true;
                                
//                             if(scope.company.isComplete == false) 
//                             {  
//                                growl.addSuccessMessage('<b>Congrats: &nbsp;</b> Your Basic Profile is complete  <br> Now you can navigate !!!');
//                             }     
                                
//                              scope.company.isComplete = true; 
//                              $rootScope.viewIsCompleteCount+=1;    
                                
                                if(!authService.isProfileComplete())
                               {
                                   authService.profileCompleted();
                               }      
                                
                            }, 1500);  
                            
//                            scope.company.messageDisplay = false;
//                            scope.company.form = "false";
//                            scope.company.loader = "true";
//                            scope.company.message = "Your company View has been Submitted successfully";
//                            authService.profileCompleted();
                        }
                        else{

//                            scope.company.loader = "true";
//                            scope.company.messageDisplay = false;
//                            scope.company.form = "false"; 
//                            scope.company.message = response.data.message;
                        }

                    }).error(function(){

//                        scope.company.loader = "true";
//                        scope.company.messageDisplay = false;
//                        scope.company.form = "false"; 
//                        scope.company.message = "Some issue in backend , please refresh and retry";

                    });



			};
            

            
			$(element[0])
            .find('[name="projectList"]')
                .change(function (e) {
                    $(element[0]).bootstrapValidator('revalidateField', 'projectList');    
                })
                .end()
            .find('[name="technicalSkillList"]')
                .change(function (e) {
                    $(element[0]).bootstrapValidator('revalidateField', 'technicalSkillList');    
                })
                .end()
            .bootstrapValidator({
                excluded: ':disabled',
			    submitButtons: 'button[type="submit"]',
                fields: {
                    company: {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide Company Name'
                            }

                        }
                    }
                    ,
                    position: {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide your Designation'
                            }

                        }
                    },
                    projectList: {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide your Projects you have worked on'
                            }

                        }
                    },
                    technicalSkillList: {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide your technical Skills'
                            }

                        }
                    }
                    

                }

            })
			.on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            submitCompanyForm();
           
        });


		}
	}

}]);

_revlegApp.directive("bookFormValidator",["formFactory",function(formFactory){
	return {

		link : function( scope, element , attrs) {


            scope.loader = "true";
            scope.form = "true";
            scope.messageDisplay = "true";

			var submitCompanyForm = function() {

				scope.loader= false;
				scope.form = false;

				var formData = new FormData();
				formData.append('bookList',scope.bookList);

                var interestSubmitUrl = appConfig.API_HOST+appConstants.TEMP_BOOKS;

                formFactory.submit(formData, interestSubmitUrl)
                	.success(function(response) {
                            
                        if(response.status == "success")
                        {
                            scope.messageDisplay = false;
                            scope.form = "false";
                            scope.loader = "true";
                            scope.message = "Congratilations!!! Your Book Template has been Submitted successfully";
                        }
                        else{

                            scope.loader = "true";
                            scope.messageDisplay = false;
                            scope.form = "false"; 
                            scope.message = "Some issue in backend , please refresh and retry";

                        }

                    }).error(function(){

                        scope.loader = "true";
                        scope.messageDisplay = false;
                        scope.form = "false"; 
                        scope.message = "Some issue in backend , please refresh and retry";

                    });



			};

			$(element[0]).bootstrapValidator({
			    submitButtons: 'button[type="submit"]',
                fields: {
                    bookList: {
                        validators: {
                            notEmpty: {
                                message: 'There must be some books you like'
                            }

                        }
                    }
                }

            })
			.on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            submitCompanyForm();
           
        });


		}
	}

}]);

_revlegApp.directive("passionFormValidator",["formFactory", "blockUI", "$timeout" , function( formFactory , blockUI , $timeout ){
	return {

		link : function( scope, element , attrs) {

            
            scope.resetView = function()
            {
                /** specefic for every template **/
                var templateId = '5';
                var formData = new FormData();     
                
                if(scope.forAdmin == "no") 
                {
                    var resetFormUrl =  appConfig.API_HOST + appConstants.DELETE_VIEW + "/5";
                }
                else {
                    
                    var resetFormUrl =  appConfig.API_HOST + appConstants.ADMIN_DELETE_VIEW + "/5";
                    formData.append('userId',scope.userIdForEdit);   
                }
                
                
                //var resetFormUrl = appConfig.API_HOST + appConstants.DELETE_VIEW + "/5";
                
                /** specefic for each template **/
                var passionBlockUI = blockUI.instances.get('passionForm');
                passionBlockUI.start();
                
                formFactory.submit(formData, resetFormUrl)
                	.success(function(response) {
                        
                        if(response.status == "success")
                        {
                            
                            passionBlockUI.message("Successfully Deleted");

                            /** if successfull clean everything **/
                            $('#passionList').tagsinput('removeAll');

                            /** Reseting the form **/
                            $(element[0]).bootstrapValidator('resetForm',true);
                            $timeout(function() { 
                              passionBlockUI.stop();
                              scope.passion.switchStatus = false;
                              scope.passion.isComplete = false; 
                            }, 1500);  
                            
                        }
                        else {
                        
                            passionBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              passionBlockUI.stop();
                            }, 1500); 
                        
                        }
                        
                    })
                    .error(function(){
                        
                         passionBlockUI.message("Some Error ocurred");
                         $timeout(function() { 
                              passionBlockUI.stop();
                         }, 1500); 
                
                    });
                
                
            }

			var submitPassionForm = function() {

				//scope.passion.loader= false;
				//scope.passion.form = false;

				var formData = new FormData();
//				formData.append('passionList',scope.passionList);
                formData.append('passionList',$('#passionList').val());
                
                if(scope.forAdmin == "no") 
                {
                    var interestSubmitUrl =  appConfig.API_HOST+appConstants.ACCEPT_TEMPLATES + "/5";
                }
                else {
                    var interestSubmitUrl =  appConfig.API_HOST+appConstants.ADMIN_ACCEPT_TEMPLATES + "/5" ;
                    formData.append('userId',scope.userIdForEdit);   
                }
                
                var passionBlockUI = blockUI.instances.get('passionForm');
                passionBlockUI.start();
                
                formFactory.submit(formData, interestSubmitUrl)
                	.success(function(response) {
                        
                        if(response.status == "success")
                        {
                        
                            passionBlockUI.message("Successfully Submitted");
                            $timeout(function() { 
                              passionBlockUI.stop();
                              scope.passion.switchStatus = true;
                              scope.passion.isComplete = true;    
                            }, 1500);      
                            
//                            scope.passion.messageDisplay = false;
//                            scope.passion.form = "false";
//                            scope.passion.loader = "true";
//                            scope.passion.message = "Your passionate about View has been submitted successfully";
                        }
                        else{

//                            scope.passion.loader = "true";
//                            scope.passion.messageDisplay = false;
//                            scope.passion.form = "false"; 
//                            scope.passion.message = response.data.message;

                        }

                    }).error(function(){

//                        scope.passion.loader = "true";
//                        scope.passion.messageDisplay = false;
//                        scope.passion.form = "false"; 
//                        scope.passion.message = "Some issue in backend , please refresh and retry";

                    });



			};

            scope.submitPassion = function(){
                
                submitPassionForm();
            };
            /** not needed as passion is non mandatory **/
            //			$(element[0])
            //                .find('[name="passionList"]')
            //                .change(function (e) {
            //                    $(element[0]).bootstrapValidator('revalidateField', 'passionList');    
            //                })
            //                .end()
            //                .bootstrapValidator({
            //                excluded: ':disabled',    
            //			    submitButtons: 'button[type="submit"]'
            //            })
            //			.on('success.form.bv', function(e) {
            //            // Prevent form submission
            //            e.preventDefault();
            //
            //            submitPassionForm();
            //           
            //        });


		}
	}

}]);

_revlegApp.directive("sportFormValidator",["formFactory",function(formFactory){
	return {

		link : function( scope, element , attrs) {

			
            scope.loader = "true";
            scope.form = "true";
            scope.messageDisplay = "true";

			var submitCompanyForm = function() {

				scope.loader= false;
				scope.form = false;

				var formData = new FormData();
				formData.append('sportsYouLike',scope.sportsYouLike);

                var interestSubmitUrl = appConfig.API_HOST+appConstants.TEMP_SPORTS;
                formFactory.submit(formData, interestSubmitUrl)
                	.success(function(response) {
                        if(response.status == "success")
                        {
                            scope.messageDisplay = false;
                            scope.form = "false";
                            scope.loader = "true";
                            scope.message = "Congratilations!!! Your Sports Template has been Submitted successfully";
                        }
                        else{

                            scope.loader = "true";
                            scope.messageDisplay = false;
                            scope.form = "false"; 
                            scope.message = "Some issue in backend , please refresh and retry";

                        }
                    }).error(function(){

                        scope.loader = "true";
                        scope.messageDisplay = false;
                        scope.form = "false"; 
                        scope.message = "Some issue in backend , please refresh and retry";

                    });



			};

			$(element[0]).bootstrapValidator({
			    submitButtons: 'button[type="submit"]',
                fields: {
                    sportsList: {
                        validators: {
                            notEmpty: {
                                message: 'Please Provide Sports List'
                            }

                        }
                    }
                }

            })
			.on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            submitCompanyForm();
           
        });


		}
	}

}]);

_revlegApp.directive("resetPasswordFormValidator",["formFactory","blockUI","$timeout" , function( formFactory , blockUI ,$timeout ){
	return {

		link : function( scope, element , attrs) {


			var submitResetPasswordForm = function() {

				var formData = new FormData();
                formData.append('password',$('#resetPassword').val());
                var resetPasswordSubmitUrl =  appConfig.API_HOST + appConstants.RESET_PASSWORD;
                var passionBlockUI = blockUI.instances.get('resetPasswordForm');
                passionBlockUI.start();
                formFactory.submit(formData, resetPasswordSubmitUrl)
                	.success(function(response) {
                        
                        if(response.status == "success")
                        {
                        
                            passionBlockUI.message("Password Changed Successfully");
                            $('#resetPassword').val("");
                            $timeout(function() { 
                              passionBlockUI.stop();
                            }, 1500);      
                            
                        }
                        else{
                            
                            
                            passionBlockUI.message("Some Error");
                            $timeout(function() { 
                              passionBlockUI.stop();
                            }, 1500);      

                        }

                    }).error(function(){

                            passionBlockUI.message("Some Error");
                            $timeout(function() { 
                              passionBlockUI.stop();
                            }, 1500);      
                       

                    });



			};

//            scope.submitResetPassword = function(){
//                
//                submitResetPasswordForm();
//            };
            /** not needed as passion is non mandatory **/
			$(element[0]).bootstrapValidator({
				submitButtons: 'button[type="submit"]',
                fields: {
                    resetPassword: {
                     
                        validators: {
                            notEmpty: {
                                message: 'Please Provide New Password'
                            }

                        }
                    }
                }

            })
			.on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();
            submitResetPasswordForm();
           
        });

		}
	}

}]);


_revlegApp.directive("mySearch",[ "$location", "$analytics" , function(location,$analytics){
	return function (scope, element, attrs) { 
    
        element.bind("keydown", function () {
        if(event.which === 13) {

            event.preventDefault();  
            if(scope.asyncSelected == "")
           {
                alert("Enter something to search !!!!");
           }
           else{

               var searchInput = scope.asyncSelected;
               var searcherUrl = appConfig.API_HOST + appConstants.SEARCHER+ "?q=" + searchInput;

               $analytics.eventTrack('_search', {  category: 'keywords searched', label: searchInput });  

               // re routing it with query params attached;
               location.search('q',searchInput).path("/search");
           }   
        }
        });
        
    } 
}]);


_revlegApp.directive("suggestions",["suggestionFactory", "authService" , "SweetAlert", function(commentFactory,authService,SweetAlert){
	return function (scope, element, attrs) {
        
//       element.bind("keydown", function (event) {
//       if(event.which === 13) {      
//       event.preventDefault(); 
        
         var commentBodyLimit = 250;
         scope.commentAlert = "characters left " + commentBodyLimit ;
         scope.commentChange = function()
         {
           if(scope.myComment.commentEntered.length >= commentBodyLimit )
           {
              scope.commentAlert = "Sorry you have exceeded the limit";
              scope.myComment.commentEntered = scope.myComment.commentEntered.substring(0,scope.myComment.commentEntered.length - 1);
           }
           else {

             scope.commentAlert = "characters left " + (commentBodyLimit-scope.myComment.commentEntered.length);
               
           }
         };
        
         
          
         scope.suggestionReset = function(messageId)
         {
            scope.myComment.commentEntered = "";
            scope.commentAlert = "The total characters left " + commentBodyLimit ;
         };
         
         scope.submitComment = function() {
         
                    // put ajax call here //
                    var suggestion  = scope.myComment.commentEntered;
                    
                    if(suggestion.length != 0)
                    {
                    
                    //start//    
                    // allowed to submit 
                    var messageId = attrs.messageid;
                    var comment_url = appConfig.API_HOST + appConstants.SUGGESTION_CREATE + messageId + "/" + "suggestions" ;
                    var formData = new FormData();
                    formData.append('suggestion',suggestion);
                    
                    
                    // updating before hand if not a success then need to revert back//
                    for( var l = 0; l < scope.$parent.messageList.length ; l++)
                    {
                        if(scope.$parent.messageList[l].messageId == messageId){

                            var suggestionObj = {};
                            
                            suggestionObj.suggestionBody = suggestion;
                            suggestionObj.profilePic = authService.getProfilePic();
                            suggestionObj.createrUserName = authService.getDisplayName();
                            suggestionObj.date = moment().format("MMMM Do");
                            suggestionObj.time = moment().format("h:mm");
                            if(scope.$parent.messageList[l].suggestion == undefined)
                            {
                                scope.$parent.messageList[l].suggestion = [];
                                scope.$parent.messageList[l].suggestionLength = scope.$parent.messageList[l].suggestionLength + 1;   
                                scope.$parent.messageList[l].suggestion.push(suggestionObj);    
                            }
                            else {
                                scope.$parent.messageList[l].suggestionLength = scope.$parent.messageList[l].suggestionLength + 1;    
                                scope.$parent.messageList[l].suggestion.push(suggestionObj);
                            }
                            
                        }
                    }
                    // end //
                    
                    commentFactory.send(formData, comment_url)
                    .success(function(response) {

                        if(response.status == "success")
                        {
                            if(response.status == "success")
                            {
                                  // do nothing //
                                  scope.myComment.commentEntered = "";
                                  scope.commentAlert = "The total characters left " + commentBodyLimit ;
                            }
                            else {
                                
                                    SweetAlert.swal("Oops...", "Some errror occurred", "error"); 
                                    for( var l = 0; l < scope.$parent.messageList.length ; l++)
                                    {
                                        if(scope.$parent.messageList[l].messageId == messageId)
                                        {
                                            scope.$parent.messageList[l].suggestionLength = scope.$parent.messageList[l].suggestionLength - 1; 
                                            scope.$parent.messageList[l].suggestion.pop();

                                        }
                                    }
                                
                            }
                        }

                    }).error(function(){

                                         
                           SweetAlert.swal("Oops...", "Some errror occurred", "error"); 
                            for( var l = 0; l < scope.$parent.messageList.length ; l++)
                            {
                                if(scope.$parent.messageList[l].messageId == messageId)
                                {
                                    scope.$parent.messageList[l].suggestionLength = scope.$parent.messageList[l].suggestionLength - 1; 
                                    scope.$parent.messageList[l].suggestion.pop();

                                }
                            }
                    });
//                    // end //
                    }
                    
                };
        }

}]);


//_revlegApp.directive("comments",["commentFactory", "authService" , function(commentFactory,authService){
//	return function (scope, element, attrs) {
//        
////            element.bind("keydown", function (event) {
////                if(event.which === 13) {
////                    
////                    event.preventDefault(); 
//         var commentBodyLimit = 70;
//         scope.commentAlert = "characters left " + commentBodyLimit ;
//         scope.commentChange = function()
//         {
//           if(scope.myComment.commentEntered.length >= commentBodyLimit )
//           {
//              scope.commentAlert = "Sorry you have exceeded the limit";
//              scope.myComment.commentEntered = scope.myComment.commentEntered.substring(0,scope.myComment.commentEntered.length - 1);
//           }
//           else {
//
//             scope.commentAlert = "characters left " + (commentBodyLimit-scope.myComment.commentEntered.length);
//               
//           }
//         };
//        
//         
//         scope.submitComment = function() {
//         
//                    // put ajax call here //
//                    var comment  = scope.myComment.commentEntered;
//                    
//                    if(comment.length != 0)
//                    {
//                        var comment_url = appConfig.API_HOST + appConstants.COMMENT_CREATE + attrs.messageid + "/" + "comments";
//                        var formData = new FormData();
//				        formData.append('newCommentBody',comment);
//                        
//                        commentFactory.send(formData, comment_url)
//                        .success(function(response) {
//
//                            if(response.status == "success")
//                            {
//
//                                for( var l = 0; l < scope.messageList.length ; l++)
//                                {
//                                    if(scope.messageList[l].messageId == attrs.messageid){
//                                        
//                                        var commentObj = {};
//                                        commentObj.commentBody = comment;
//                                        commentObj.profilePic = authService.getProfilePic();
//                                        scope.messageList[l].comment.push(commentObj);
//                                        scope.myComment.commentEntered = "";
//                                    }
//                                }
//                                
//                                scope.commentAlert = "The total words left " + commentBodyLimit ;
//                                
//                            }
//                            else{
//
//                                alert("failure");                                    
//                            }
//
//                        }).error(function(){
//
//                                 alert("error");
//
//                        });
//                        
//                    }
//                    
//                }
////            });
//        
//        
//        }
//
//}]);


_revlegApp.directive("displayafterloginWidget", ["authService", "$rootScope", "$location",
    function (authService , $rootScope , location) {
        return {
            
           restrict: 'AEC',
           link : function(scope, element, attributes){
                              
               if(!authService.checkLogin())
               {
                   // not logged in //
                   element.addClass('hide');
               }
               else {
               
                    element.removeClass('hide');
               }
               
               scope.$watch(
                function(){ return  authService.checkLogin() },

                function(newVal) {
                  if(newVal == true){
                      
                      element.removeClass('hide');
                  }
                  else {
                     
                      element.addClass('hide');
                  }
                  
                }
               );                 
               
            }
        }   
}]);


_revlegApp.directive("displaybefloginWidget", ["authService", "$rootScope", "$location",
    function (authService , $rootScope , location) {
        return {
            
           restrict: 'AEC',
           link : function(scope, element, attributes){
                              
               if(!authService.checkLogin())
               {
                   // not logged in //
                   element.removeClass('hide');
               }
               else {
                   // logged in //
                   element.addClass('hide');
               }
               
               scope.$watch(
                function(){ return  authService.checkLogin() },

                function(newVal) {
                  if(newVal == true){
                      
                      element.addClass('hide');
                  }
                  else {
                     
                      element.removeClass('hide');
                  }
                  
                }
               );                 
               
            }
        }   
}]);



_revlegApp.directive("locationFormValidator",["formFactory","sessionService","$rootScope","$location", "$timeout" , "blockUI" , "$rootScope" ,     function(formFactory,sessionService,$rootScope,$location,$timeout,blockUI,$rootScope) {
	return {

		link : function(scope , element , attrs){
           
            
            scope.resetView = function()
            {
                /** specefic for every template **/
                var templateId = '1';
                var formData = new FormData();
                
                if(scope.forAdmin == "no") 
                {
                    var resetFormUrl =  appConfig.API_HOST + appConstants.DELETE_VIEW + "/1";
                }
                else {
                    
                    var resetFormUrl =  appConfig.API_HOST + appConstants.ADMIN_DELETE_VIEW + "/1";
                    formData.append('userId',scope.userIdForEdit);   
                }
                
//                var resetFormUrl = appConfig.API_HOST + appConstants.DELETE_VIEW + "/1";
                
                /** specefic for each template **/
                var locationBlockUI = blockUI.instances.get('locationForm');
                locationBlockUI.start();      
                
                formFactory.submit(formData, resetFormUrl)
                	.success(function(response) {
                        
                        if(response.status == "success")
                        {
                            
                            locationBlockUI.message("Successfully Deleted");

                            /** if successfull clean everything **/
                            scope.area = "";
                            scope.state = "";
                            scope.city = "";
                            scope.country = "India";
                
                            /** Reseting the form **/
                            //$(element[0]).bootstrapValidator('resetForm',true);
                            
                            $timeout(function() { 
                              locationBlockUI.stop();
                              scope.location.switchStatus = false;
                              scope.location.isComplete = false; 
                            }, 1500);  
                            
                        }
                        else {
                        
                            locationBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              locationBlockUI.stop();
                            }, 1500);  
                        
                        }
                        
                    })
                    .error(function(){
                    
                            locationBlockUI.message("Some Error ocurred");
                            $timeout(function() { 
                              locationBlockUI.stop();
                            }, 1500); 
                        
                    });
                
                
            }
            
 
			var submitLocationForm = function() {
				
				var formData = new FormData();

				// for location data //
				formData.append('country',scope.country);
				formData.append('state',scope.state);
				formData.append('city',scope.city);
                formData.append('area',scope.area);

                if(scope.forAdmin == "no") 
                {
                    var profileSubmitUrl = appConfig.API_HOST+appConstants.ACCEPT_TEMPLATES + "/1";
                }
                else {
                   var profileSubmitUrl = appConfig.API_HOST+appConstants.ADMIN_ACCEPT_TEMPLATES + "/1";
                   formData.append('userId',scope.userIdForEdit);    
                }
                //scope.location.loader = "false";
                //scope.location.form = "false";

                /** for blocking the div **/
                //$('#locationForm').block({ message: "<p> Processing your view ...</p>", css : {cursor:'not-drop',backgroundColor:'#fff'} }); 
                
                var locationBlockUI = blockUI.instances.get('locationForm');
                
                locationBlockUI.start();
                formFactory.submit(formData, profileSubmitUrl)
                	.success(function(response) {

                        if(response.status == "success")
                        {
                            locationBlockUI.message("Successfully Submitted");
                            
                            $timeout(function() { 
                              locationBlockUI.stop();
                              scope.location.switchStatus = true;
                              scope.location.isComplete = true; 
                              $rootScope.viewIsCompleteCount+=1;    
                            }, 1500); 
                            
                            //$('.blockMsg').html("<p style='color:green;'>View Submittion Successfull</p>");
//                            var somefunc = function() { 
////                                $('#locationForm').unblock(); 
////                                $('#locationForm').block({ message: null, css : {cursor:'not-drop',backgroundColor:'#fff'} });
////                                 $('.blockMsg').html("");
//                            };
//                            $timeout(somefunc,2500);
//                            

                            
                            
                            //scope.location.messageDisplay = false;
                            //scope.location.form = "false";
                            //scope.location.loader = "true";
                            //scope.location.message = "Your location View has been submitted successfully";
                            //scope.message = "Congratilations!!! Your Profile has been Submitted successfully";

                        }
                        else{
                            
                            $('.blockMsg').html("<p style='color:red;'>View Submission UnSuccessful</p>");
                            var somefunc = function() { $('#locationForm').unblock(); }
                            $timeout(somefunc,1500);
                            

                            
                            //scope.location.loader = "true";
                            //scope.location.messageDisplay = false;
                            //scope.location.form = "false"; 
                            //scope.location.message = response.data.message;

                        }

                	}).error(function(){

                        scope.location.loader = "true";
                        scope.location.messageDisplay = false;
                        scope.location.form = "false"; 
                        scope.location.message = "Some issue in backend , please refresh and retry";

                    });

			};

			$(element[0]).bootstrapValidator({
				submitButtons: 'button[type="submit"]',
                fields: {
                    country: {
                     
                        validators: {
                            notEmpty: {
                                message: 'Please Provide country'
                            }

                        }
                    }
                    ,
                    state: {
                     
                        validators: {
                            notEmpty: {
                                message: 'Please Provide state'
                            }

                        }
                    }
                    ,
                    city: {
                     
                        validators: {
                            notEmpty: {
                                message: 'Please Provide city'
                            }

                        }
                    }
                    ,
                    area: {

                        validators: {
                            notEmpty: {
                                message: 'Please Provide area'
                            }

                        }
                    }
                }

            })
			.on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();
            submitLocationForm();
           
        });

		}
	}
}]);


_revlegApp.directive("forgetpasswordFormValidator",["formFactory",function(formFactory){
	return {

		link : function( scope, element , attrs) {


			var submitForgetPasswordForm = function() {

				scope.forgetPassword.loader= true;
				scope.forgetPassword.form = false;

				var formData = new FormData();
				formData.append('emailId',scope.forgetPassword.emailId);
                var url =  appConfig.API_HOST+appConstants.FORGET_PASSWORD;
                formFactory.submit(formData, url)
                	.success(function(response) {
                        
                        if(response.status == "success")
                        {
                            scope.forgetPassword.messageDisplay = true;
                            scope.forgetPassword.form = false;
                            scope.forgetPassword.loader = false;
                            scope.forgetPassword.message = "A new password has been sent to your mail id.";
                        }
                        else{

                            scope.forgetPassword.loader = false;
                            scope.forgetPassword.messageDisplay = true;
                            scope.forgetPassword.form = false;
                            scope.forgetPassword.message = response.data.message;

                        }

                    }).error(function(){

                        scope.forgetPassword.loader = false;
                        scope.forgetPassword.messageDisplay = false;
                        scope.forgetPassword.form = true
                        scope.forgetPassword.message = "Some issue in backend , please refresh and retry";

                    });



			};

			$(element[0]).bootstrapValidator({
			    submitButtons: 'button[type="submit"]',
                fields: {
                    emailId: {
                        validators: {
                            notEmpty: {
                                message: 'Please enter your emailId'
                            }

                        }
                    }
                }

            })
			.on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            submitForgetPasswordForm();
           
        });


		}
	}

}]);


_revlegApp.directive("requestForInvitationForm",["formFactory", function(formFactory){
	return {

		link : function( scope, element , attrs) {


			var submitRequestForInvitiationForm = function() {

//                  var finalData  = new Array();
//      
//
//                  var obj = {};
//                  obj.emailId = scope.singleRequest.email;
//                  obj.name = scope.singleRequest.name;
//
//                  //hardcoding the email//
//                  //obj.emailId = "praveen.menon.88@gmail.com";
//
//                  finalData.push(obj);
//
//
//                  var url  = appConfig.API_HOST + appConstants.INVITATION;
//                  var formData = new FormData();
//
//                  formData.append('referList',JSON.stringify(finalData));
//
//                  formFactory.submit(formData,url)
//                    .success(function(response){
//
//                        //$modalInstance.dismiss('cancel');
//                    })
//                    .error(function(){
//
//                        alert("some error");
//                    });

                scope.submit();


			};

			$(element[0]).bootstrapValidator({
			    submitButtons: 'button[type="submit"]',
                fields: {
                    emailId: {
                        validators: {
                            notEmpty: {
                                message: 'Please enter your emailId'
                            },
                            remote: {
                                url: appConfig.API_HOST + appConstants.CHECK_EMAIL,
                                message: 'This emailId already exist'
                            }

                        }
                    },
                    name : {
                        validators: {
                            notEmpty: {
                                message: 'Please enter your name'
                            }

                        }
                    }
                }

            })
			.on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            submitRequestForInvitiationForm();
           
        });


		}
	}

}]);


_revlegApp.directive("createMessageFormValidator",["formFactory","$location","$rootScope","sessionService", "intendedURLService","ipCookie", "messageFactory", "$timeout" , function(formFactory,$location,$rootScope,sessionService,intendedURLService,ipCookie,messageFactory,$timeout){
    return {

        scope : false,
        transclude : false,
        link : function( scope, element , attrs) {

            scope.loader = "true";
            scope.isForm = "true";
            scope.messageDisplay = "true";
            scope.isCancelClicked = "no"; 

            var submitMessageForm = function() {
            
                  var formData = new FormData();
                  formData.append('messageSubject',scope.message.messageTitle);
                  formData.append('messageBody',scope.message.messageBody);
                  formData.append('audienceBlock',scope.audienceList);
                  formData.append('audienceCount',scope.audienceList.length);
                  var optionArray = []; 
                  var count = 0;
                  $(':visible[name="option[]"]').each(function(){
                      var singleOptionValue = $(this).val();
                      optionArray[count] = singleOptionValue.replace(/,/g, "^");
                      count++;  
                  });
                  formData.append('commentArray',optionArray);
                  // sending the message //
                  var messageCreateUrl = appConfig.API_HOST + appConstants.MESSAGE_CREATE;
                
                  // loading the timer screen //
                  $.blockUI({ css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: 'white', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 1, 
                        color: '#fff' 
                    } }); 
                
                  messageFactory.send(formData, messageCreateUrl)
                  .success(function(response) {
//                      $scope.loader = "true";
        
                       if(response.status == 'success')
                       {
                            $('.blockMsg').html("<h1 style='color:green;'>Message Submission Successful</h1>");
                            //setTimeout($.unblockUI(),2000);
                           
                            $timeout(function(){
                                //$modalInstance.dismiss('cancel');
                                $.unblockUI();
                                scope.$parent.isForm = "true";
                            },1800); 
                            
                            
                       }
                       else{
        
//                            $scope.loader = "true";
                              $('.blockMsg').html("<h1 style='color:red;'>Message Submission Unsuccessful</h1>");
                              $timeout(function(){
                                //$modalInstance.dismiss('cancel');
                                $.unblockUI();
                                
                              },1800); 
                             
                              
                       }
        
                    }).error(function(){
        
                            $('.blockMsg').html("<h1 style='color:red;'>Message Submission Unsuccessful</h1>");
                            $timeout(function(){
                                //$modalInstance.dismiss('cancel');
                                $.unblockUI();
                                
                            },1800); 
                            //setTimeout($.unblockUI(),2000);
                      
//                            $scope.loader = "true";
//                            $scope.form = "true"; 
        
                    });    
                
            };
            
    var MAX_OPTIONS = 4;

     $(element[0]).bootstrapValidator({
            submitButtons: 'button[type="submit"]',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                surveyTitle: {
                    validators: {
                        notEmpty: {
                            message: 'The title is required and cannot be empty'
                        }
                    }
                },
                surveyBody: {
                    validators: {
                        notEmpty: {
                            message: 'The  Description is required and cannot be empty'
                        }
                    }
                },
                'option[]': {
                    validators: {
                        notEmpty: {
                            message: 'The Option is required and cannot be empty'
                        },
                        stringLength: {
                            max: 100,
                            message: 'The option must be less than 100 characters long'
                        }
                    }
                }
            }
        })

        // Add button click handler
        .on('click', '.addButton', function() {
            var $template = $('#optionTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $option   = $clone.find('[name="option[]"]');

            // Add new field
           $(element[0]).bootstrapValidator('addField', $option);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row    = $(this).parents('.form-group'),
                $option = $row.find('[name="option[]"]');

            // Remove element containing the option
            $row.remove();

            // Remove field
            $(element[0]).bootstrapValidator('removeField', $option);
        })
        // Remove button click handler
        .on('click', '.messageCancel', function(e) {
            
            
            //scope.$parent.isForm = "true";
            e.preventDefault();
        })

        // Called after adding new field
        .on('added.field.bv', function(e, data) {
            // data.field   --> The field name
            // data.element --> The new field element
            // data.options --> The new field options

            if (data.field === 'option[]') {
                if ($(element[0]).find(':visible[name="option[]"]').length >= MAX_OPTIONS) {
                    $(element[0]).find('.addButton').attr('disabled', 'disabled');
                }
            }
        })

        // Called after removing the field
        .on('removed.field.bv', function(e, data) {
           if (data.field === 'option[]') {
                if ($(element[0]).find(':visible[name="option[]"]').length < MAX_OPTIONS) {
                    $(element[0]).find('.addButton').removeAttr('disabled');
                }
            }
        })
        .on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            submitMessageForm();
           
        });  

        }
        }

}]);


//_revlegApp.directive("referusFormValidator",["formFactory","$location","$rootScope","sessionService", "intendedURLService","$cookieStore", function(formFactory,$location,$rootScope,sessionService,intendedURLService,$cookieStore){
//    return {
//
//        scope : false,
//        transclude : false,
//        link : function( scope, element , attrs) {
//
//            scope.loader = "true";
//            scope.form = "true";
//            scope.messageDisplay = "true";
//
//            var submitLoginForm = function() {
//            
//            // if client side vaidations
//                
//            var formData = new FormData();
//            formData.append('emailId',scope.emailId);
//            formData.append('password',scope.password);
//            var interestSubmitUrl = appConfig.API_HOST + appConstants.LOGIN;
//            formFactory.submit(formData, interestSubmitUrl)
//                .success(function(response) {
//                    scope.loader = "true";
//
//                   if(response.status == 'success')
//                   {
//                        /** to be changed from username to displayname**/
//                        sessionService.create(response.data.userData.basicInfo);
//                        scope.loader = "true";
//                        scope.$parent.isForm = "true";
//                        $rootScope.$broadcast('event-signin-success');
//                       
//                        /** to remove and move it to services **/
////                        $cookieStore.put('displayName','Amit');
////                        $cookieStore.put('fullName','Amit Aggarwal');
////                        $cookieStore.put('emailId','amit.aggarwal@revleg.com');
////                        $cookieStore.put('isProfileComplete',false);
//                       
//                        if($cookieStore.get('isProfileComplete') == false)
//                        {
//                            $location.path("/profile");
//                        }
//                        
//                        
//                   }
//                   else  {
//
//                        scope.loader = "true";
//                        scope.messageDisplay = false;
//                        scope.form = "true";
//                        scope.loader = "true"; 
//                        scope.message = "Credentials incorrect,please login once again";
//                   }
//                }).error(function(){
//
//                    scope.loader = "true";
//                    scope.messageDisplay = false;
//                    scope.form = "true"; 
//                    scope.message = "Credentials incorrect,please login once again";
//
//                });    
//                
//            };
//
//            $(element[0]).bootstrapValidator({
//                submitButtons: 'button[type="submit"]',
//                fields: {
//                    emailId: {
//                        validators: {
//                            notEmpty: {
//                                message: 'Please Provide EmailId'
//                            }
//
//                        }
//                    },
//                    password: {
//                        validators: {
//                            notEmpty: {
//                                message: 'Please Provide password'
//                            }
//
//                        }
//                    }
//                }
//
//            })
//            .on('success.form.bv', function(e) {
//            // Prevent form submission
//            e.preventDefault();
//
//            submitLoginForm();
//           
//        });
//
//
//        }
//    }
//
//}]);

_revlegApp.directive("referUsFormValidator",["formFactory","$location","$rootScope","sessionService", "intendedURLService","ipCookie", "invitationFactory", "$timeout" , "$analytics" ,  function(formFactory,$location,$rootScope,sessionService,intendedURLService,ipCookie,invitationFactory,$timeout,$analytics){
    return {

        scope : false,
        transclude : false,
        link : function( scope, element , attrs) {

            scope.form = "true";
            scope.isForm = "false";
            var submitReferUsForm = function() {
            
                  var formData = new FormData();
                  var nameArray = []; 
                  var emailArray = [];
                  var count = 0;
                  $(':visible[name="name[]"]').each(function(){
                     nameArray[count] = $(this).val();
                     count++;  
                  });
                  count = 0;
                  $(':visible[name="emailId[]"]').each(function(){
                     emailArray[count] = $(this).val();
                     count++;  
                  });
                
                
                 $analytics.eventTrack('_feedback_submit-click', {  category: 'open feedback model', label: 'null' });
      
                  var finalData  = new Array();

                  for(var i=0; i < count ; i++)
                  {
                      var obj = {};
                      obj.emailId = emailArray[i];
                      obj.name = nameArray[i];
                      finalData.push(obj);
                  }

                  var url  = appConfig.API_HOST + appConstants.INVITATION;
                  var formData = new FormData();

                  formData.append('referList',JSON.stringify(finalData));
                            
                
                  // loading the timer screen //
                  $.blockUI({ css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: 'white', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 1, 
                        color: '#fff' 
                    } }); 
                
                  invitationFactory.send(formData, url)
                  .success(function(response) {
//                      $scope.loader = "true";
        
                       if(response.status == 'success')
                       {
                            $('.blockMsg').html("<h1 style='color:green;'>Referral Submission Successful</h1>");
                            //setTimeout($.unblockUI(),2000);
                           
                            $timeout(function(){
                                //$modalInstance.dismiss('cancel');
                                $.unblockUI();
                                scope.$parent.isForm = "true";
                            },1800); 
                            
                            
                       }
                       else{
        
                           
                            var message =  response.data.message;
                              $('.blockMsg').html("<h1 style='color:red;'>Referral Submission Unsuccessful</h1> <br> <p style='font-size:22px;'>"+message+"</p>");
                              $timeout(function(){
                                //$modalInstance.dismiss('cancel');
                                $.unblockUI();
                                
                              },1800); 
                             
                              
                       }
        
                    }).error(function(){
        
                            $('.blockMsg').html("<h1 style='color:red;'>Referral Submission Unsuccessful</h1>");
                            $timeout(function(){
                                //$modalInstance.dismiss('cancel');
                                $.unblockUI();
                                
                            },1800);         
                    });    
                
            };
            
    var MAX_OPTIONS = 3;

     $(element[0]).bootstrapValidator({
            submitButtons: 'button[type="submit"]',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'name[]': {
                    validators: {
                        notEmpty: {
                            message: 'Persons name is required and cannot be empty'
                        },
                        stringLength: {
                            max: 30,
                            message: 'The Name must be less than 25 characters long'
                        }
                    }
                }
                    ,
                'emailId[]': {
                        validators: {
                            notEmpty: {
                                message: 'EmailId is required and cannot be empty'
                            },
                             emailAddress: {
                                message: 'The value is not a valid email address'
                            }
                        }
                    }
                    
                }
        })

        // Add button click handler
        .on('click', '.addButton', function() {
            var $template = $('#optionTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $name   = $clone.find('[name="name[]"]');
                $emailId  = $clone.find('[name="emailId[]"]');

            // Add new field
           $(element[0]).bootstrapValidator('addField', $name);
           $(element[0]).bootstrapValidator('addField', $emailId);    
            
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row    = $(this).parents('.form-group'),
                $name = $row.find('[name="name[]"]');

            // Remove element containing the option
            $row.remove();
            
            var $row2    = $(this).parents('.form-group'),
                $emailId = $row2.find('[name="emailId[]"]');
            $row2.remove();
            
            // Remove field
            $(element[0]).bootstrapValidator('removeField', $name);
            $(element[0]).bootstrapValidator('removeField', $emailId);
        })

        // Called after adding new field
        .on('added.field.bv', function(e, data) {
            // data.field   --> The field name
            // data.element --> The new field element
            // data.options --> The new field options

            if (data.field === 'name[]') {
                if ($(element[0]).find(':visible[name="name[]"]').length >= MAX_OPTIONS) {
                    $(element[0]).find('.addButton').attr('disabled', 'disabled');
                }
            }
        })
        // Remove button click handler
        .on('click', '.referUsCancel', function(e) {
            
            //scope.$parent.isForm = "true";
            e.preventDefault();
        })
        // Called after removing the field
        .on('removed.field.bv', function(e, data) {
           if (data.field === 'name[]') {
                if ($(element[0]).find(':visible[name="name[]"]').length < MAX_OPTIONS) {
                    $(element[0]).find('.addButton').removeAttr('disabled');
                }
            }
        })
        .on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();

            submitReferUsForm();
           
        });  

        }
        }

}]);

_revlegApp.directive('scrollIf', [ "$rootScope" , "$anchorScroll", "$location" , "$document", "$timeout" , "$interval" , 
function ($rootScope, $anchorScroll, $location, $document , $timeout , $interval ) {
    return function (scope, element, attributes ) {
      
  if($rootScope.lastMessageId == attributes.scrollIf && $rootScope.scrollIntoViewMessageId!= undefined && $rootScope.scrollIntoViewMessageId!= 'none' && $rootScope.lastMessageId!= undefined)  
  {
      
            $timeout(function(){
      
                var top = 400;
                var duration = 0; //milliseconds
                var offset = 90; //pixels; adjust for floating menu, context etc
                //Scroll to #some-id with 30 px "padding"
                //Note: Use this in a directive, not with document.getElementById 
                var someElement = angular.element(document.getElementById("scrollIntoView_"+$rootScope.scrollIntoViewMessageId));
                $document.scrollToElement(someElement, offset, duration);
                $rootScope.scrollIntoViewMessageId = null;
                $rootScope.lastMessageId = null;
                

            });
      
  }      
      
}}]);


_revlegApp.directive('onLastRepeat', function() {
    return function(scope, element, attrs) {
        if (scope.$last) setTimeout(function(){
            scope.$emit('onRepeatLast', element, attrs);
        }, 1);
    };
});

_revlegApp.directive('areaBasedGoogleMap', function () {
    return {
        restrict: "A",
        template: "<div id='areaMap'></div>",
        scope: {           
            area: "=",
            zoom: "="
        },
        controller: function ($scope) {
            var mapOptions;
            var map;           
            var marker;

            var initialize = function () {                                
                mapOptions = {
                    zoom: $scope.zoom,
                    center: new google.maps.LatLng(40.0000, -98.0000),
                    mapTypeId: google.maps.MapTypeId.TERRAIN
                };
                
                map = new google.maps.Map(document.getElementById('areaMap'), mapOptions);
            };
          
            var createMarker = function (area) {
                var position = new google.maps.LatLng(area.Latitude, area.Longitude);
                map.setCenter(position);
                marker = new google.maps.Marker({
                    map: map,
                    position: position,
                    title: area.Name
                });               
            };

            $scope.$watch("area", function (area) {
                if (area != undefined) {
                    createMarker(area);
                }
            });

              initialize();
//            google.maps.event.addDomListener(window, 'resize', initialize);
//            google.maps.event.addDomListener(window, 'load', initialize)
            
        }
    };
});