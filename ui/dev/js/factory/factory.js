_revlegApp.factory("formFactory",["$http",function($http){


	return {

		submit: function(formData,url)
		{
			return $http.post(url, formData, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                });
		}
	}
}]);

_revlegApp.factory("logoutFactory", ["$http",
        function ($http) {
        return {
            logout: function (url) {
                return $http.get(url, {
                    headers: {
                        'Content-Type': undefined,
                        //'session-token': session_token
                    }
                });
            }
        };
}]);

_revlegApp.factory("searcherFactory", ["$http",
        function ($http) {
        return {
            search: function (url) {
                return $http.get(url, {
                    headers: {
                        'Content-Type': undefined,
                        //'session-token': session_token
                    }
                });
            }
        };
}]);

_revlegApp.factory("messageFactory",["$http",function($http){
	return {

		send: function(formData,url)
		{
			return $http.post(url, formData, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                });
		},
        recv: function (url) {
                return $http.get(url, {
                    headers: {
                        'Content-Type': undefined,
                        //'session-token': session_token
                    }
                });
        }
        
        
	}
}]);


_revlegApp.factory("commentFactory",["$http",function($http){
	return {

		send: function(formData,url)
		{
			return $http.post(url, formData, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                });
		},
        recv: function (url) {
                return $http.get(url, {
                    headers: {
                        'Content-Type': undefined,
                        //'session-token': session_token
                    }
                });
        }
        
        
	}
}]);


_revlegApp.factory("invitationFactory",["$http",function($http){
	return {

		send: function(formData,url)
		{
			return $http.post(url, formData, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                });
		}
        
        
	}
}]);

_revlegApp.factory("referralFactory",["$http",function($http){
	return {
        
        checkReferCode: function (url) {
                return $http.get(url, {
                    headers: {
                        'Content-Type': undefined,
                        //'session-token': session_token
                    }
                });
        }
        
        
	}
}]);

_revlegApp.factory("emailverificationFactory",["$http",function($http){
	return {
        
        verifyEmailId: function (url) {
                return $http.get(url, {
                    headers: {
                        'Content-Type': undefined,
                        //'session-token': session_token
                    }
                });
        }
        
        
	}
}]);


_revlegApp.factory("templatesFactory",["$http",function($http){
	return {
        
        getAll: function (url) {
                return $http.get(url, {
                    headers: {
                        'Content-Type': undefined,
                        //'session-token': session_token
                    }
                });
        }
        
        
	}
}]);


_revlegApp.factory("locationFactory",["$http",function($http){
	return {
        
        getFromLatLong: function (lat,long) {
                
                var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + long;
                return $http.get(url, {
                    headers: {
                        'Content-Type': undefined,
                        //'session-token': session_token
                    }
                });
        }
        
        
	}
}]);


_revlegApp.factory("forgetPasswordFactory",["$http",function($http){
	return {

		send: function(formData,url)
		{
			return $http.post(url, formData, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                });
		}
        
        
	}
}]);


_revlegApp.factory("suggestionFactory",["$http",function($http){
	return {

		send: function(formData,url)
		{
			return $http.post(url, formData, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                });
		}
        
        
	}
}]);


_revlegApp.factory("getAllProfileFactory",["$http",function($http){
	return {
        
        get: function (url) {
                return $http.get(url, {
                    headers: {
                        'Content-Type': undefined,
                        //'session-token': session_token
                    }
                });
        }
        
        
	}
}]);

_revlegApp.factory("logoutFactory",["$http",function($http){
	return {
        
        logout: function (url) {
                return $http.get(url, {
                    headers: {
                        'Content-Type': undefined,
                        //'session-token': session_token
                    }
                });
        }
        
        
	}
}]);

_revlegApp.factory("getLocationList",["$http",function($http){
	return {
        
        get: function (url) {
                return $http.get(url, {
                    headers: {
                        'Content-Type': undefined,
                        //'session-token': session_token
                    }
                });
        }
        
        
	}
}]);

_revlegApp.factory('socket', ["socketFactory",function (socketFactory) {
    
    return {
        
          startSocketConnection: function()
          {
                var myIoSocket = io.connect(appConfig.NOTIFICATION_SERVER,{'force new connection':true});
                var mySocket = socketFactory({
                    ioSocket: myIoSocket
                });
                return mySocket;
          }
    
    }
    
}]);

_revlegApp.factory("commonFunctions", ["$http",function($http){
	return {
        
        getNotificationIndex: function (array,messageId) {
            
            for(var i = 0 ; i < array.length  ; i++)
            {
                if(array[i].messageObj != undefined)
                {
                if(array[i].messageObj.messageId == messageId)
                {
                    return i;
                }
                }
            }
            
        }
        
        
	}
}]);

_revlegApp.factory("APIFactory",["$http",function($http){
	return {
        
        get: function (url) {
                return $http.get(url, {
                    headers: {
                        'Content-Type': undefined,
                        //'session-token': session_token
                    }
                });
        }
        
        
	}
}]);