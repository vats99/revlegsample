// Declare app level module which depends on views, and components
var _revlegApp = angular.module('revlegApp', [
  'ngRoute','ui.bootstrap','ipCookie','angulartics', 'angulartics.google.analytics','angularFileUpload','oitozero.ngSweetAlert','toggle-switch','blockUI','angular-growl','ngSanitize','facebook','btford.socket-io' , 'duScroll' , 'infinite-scroll' , 'monospaced.elastic'
]);
    
_revlegApp.config(['$routeProvider', '$analyticsProvider' , 'blockUIConfig' , 'growlProvider' , 'FacebookProvider', '$locationProvider' , function($routeProvider,$analyticsProvider,blockUIConfig,growlProvider, FacebookProvider , $locationProvider ) {
    
  growlProvider.globalTimeToLive(3000);
  growlProvider.globalEnableHtml(true); 

  //Facebook APP Id    
  FacebookProvider.init('1474170819522482');
    
  blockUIConfig.autoBlock = false;  
    
  $locationProvider.html5Mode(false);
  $locationProvider.hashPrefix('!');
    
  $analyticsProvider.virtualPageviews (true);    
  $routeProvider
   .when('/outbox', {
                templateUrl: '/views/outboxnew.html',
                pageTitle: 'message-sent',
                requireLogin: true,
                needToBeRoutedAfterLogin:false
   })
   .when('/howItWorks', {
                templateUrl: '/views/howItWorks_iframe.html',
                pageTitle: 'howItWorks',
                requireLogin: false,
                needToBeRoutedAfterLogin:false
   })
   .when('/aboutUs', {
                templateUrl: '/views/aboutUs.html',
                pageTitle: 'aboutUs',
                requireLogin: false,
                needToBeRoutedAfterLogin:false
   })     
  .when('/search', {
                templateUrl: '/views/search.html',
                pageTitle: 'Search',
                requireLogin: false,
                needToBeRoutedAfterLogin:false
   })
  .when('/profile', {
                templateUrl: '/views/profileNew.html',
                pageTitle: 'profile',
                requireLogin: true,
                needToBeRoutedAfterLogin:false
   })
  .when('/signupflow', {
                templateUrl: '/views/signupflow.html',
                pageTitle: 'signupflow',
                requireLogin: true,
                needToBeRoutedAfterLogin:false
   })
   .when('/inbox/:messageId', {
                templateUrl: '/views/inboxnew.html',
                pageTitle: 'message-singlemessage',
                requireLogin: true,
                needToBeRoutedAfterLogin:false
   })
  .when('/pool/:messageId', {
                templateUrl: '/views/pool.html',
                pageTitle: 'message-singlemessage',
                requireLogin: false,
                needToBeRoutedAfterLogin:false
   })
   .when('/outbox/:messageId', {
                templateUrl: '/views/outboxnew.html',
                pageTitle: 'message-singlemessage-outbox',
                requireLogin: true,
                needToBeRoutedAfterLogin:false
   })
   .when('/forgetPassword', {
                templateUrl: '/views/forgetPassword.html',
                pageTitle: 'forget Password',
                requireLogin: false,
                needToBeRoutedAfterLogin:true
   })
   .when('/referUs', {
                templateUrl: '/views/referUs.html',
                pageTitle: 'Refer Us',
                requireLogin: true,
                needToBeRoutedAfterLogin:false
   })
  .when('/inbox', {
                templateUrl: '/views/inboxnew.html',
                pageTitle: 'message-received',
                requireLogin: true,
                needToBeRoutedAfterLogin:false
   })
  .when('/openquestions', {
                templateUrl: '/views/openQuestions.html',
                pageTitle: 'open-questions',
                requireLogin: true,
                needToBeRoutedAfterLogin:false
   })
  .when('/openquestions/:messageId', {
                templateUrl: '/views/openQuestions.html',
                pageTitle: 'open-questionsSingleMessage',
                requireLogin: true,
                needToBeRoutedAfterLogin:false
   })
   .when('/pointssystem', {
                templateUrl: '/views/pointssystem.html',
                pageTitle: 'Points System',
                requireLogin: true,
                needToBeRoutedAfterLogin:false
   })
   .when('/getall/:authCode', {
                templateUrl: '/views/getall.html',
                pageTitle: 'profileList',
                requireLogin: false,
                needToBeRoutedAfterLogin:false
   })
   .when('/adminprofileedit/:authCode/:userId', {
                templateUrl: '/views/profileAdminEdit.html',
                pageTitle: 'adminProfileEdit',
                requireLogin: false,
                needToBeRoutedAfterLogin:false
   })
   .when('/referer/:userId/:refCode', {
                templateUrl: '/views/referLoading.html',
                pageTitle: 'refer',
                requireLogin: false,
                needToBeRoutedAfterLogin:false
   })
  .when('/emailverify/:userId/:authCode', {
                templateUrl: '/views/emailVerification.html',
                pageTitle: 'email verification',
                requireLogin: false,
                needToBeRoutedAfterLogin:false
   })
   .when('/adminprimary/:type', {
                templateUrl: '/views/adminphase1sanatize.html',
                pageTitle: 'adminProfileEdit',
                requireLogin: false,
                needToBeRoutedAfterLogin:false
   })
   .when('/adminsymbols/:type/:min/:max', {
                templateUrl: '/views/adminphase2sanatize.html',
                pageTitle: 'adminProfileEdit',
                requireLogin: false,
                needToBeRoutedAfterLogin:false
   })
  .otherwise({redirectTo: '/search'});
       
    
}]);


_revlegApp.run(['$location', '$rootScope', '$route' , 'authService' , "intendedURLService" ,

    function($location, $rootScope , $route , authService , intendedURLService) {
                
        $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {

            if (current !== undefined) {
                if (current.$$route !== undefined) {
                    
                    if(current.$$route.templateUrl == "/views/search.html") {
                        
                          $rootScope.$emit('addImage');
                    }
                    else {
                    
                        $rootScope.$emit('removeImage');
                    }
                    
                    
                    
                }
            }
            
           

        });
        
        
        
        
        // using $locationChangeStart tp check the access control
        $rootScope.$on("$locationChangeStart", function(event, next, current) {
            
            
            intendedURLService.saveURL(null);
            
            if($location.path().indexOf("/inbox") >=0 )
            {
                // here login is required //                
                if(!authService.checkLogin())
                {
                     event.preventDefault();
                     intendedURLService.saveURL($location.path());
                     $rootScope.$emit('event-signIn');
                }
                
            }
            
            if( ($route.routes[$location.path()] != undefined) && ($location.path().indexOf("/inbox") < 0) && ($route.routes[$location.path()].requireLogin) && (!authService.checkLogin()))
            {
                //checking the current path require login 
                     //when not loggedin
                     //alert("You need to be authenticated to see this page!");
                     event.preventDefault();
                     //$location.path("/search");
                     intendedURLService.saveURL($location.path());
                     $rootScope.$emit('event-signIn');
            }
            
            if(authService.checkLogin() && (!authService.checkProfileComplete()))
            {
                $location.path("/signupflow");
            }




        });        
        
    }
                
                
    
]);


_revlegApp.run(['$rootScope', '$route' , '$anchorScroll' , function($rootScope,$route, $anchorScroll ){
    
    $rootScope.$on("$routeChangeSuccess", function(currentRoute, previousRoute){
    //Change page title, based on Route information
    $rootScope.pageTitle = $route.current.pageTitle;
    $anchorScroll.yOffset = 50;       
  });
    
}]);


/** adding filters **/
_revlegApp.filter("nl2br", function($filter) {
 return function(data) {
   if (!data) return data;
   return data.replace(/\n\r?/g, '<br/>');
 };
});
